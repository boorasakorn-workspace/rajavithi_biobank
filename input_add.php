<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: login.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }
    include("connect.php"); 
?> 
<?php 
    $sqlfz = "SELECT * FROM `ms_rack_floor` LEFT JOIN ms_freezer ON ms_freezer.freezer_id = ms_rack_floor.freezer_id LEFT JOIN ms_freezer_floor ON ms_freezer_floor.freezer_floor_id = ms_rack_floor.freezer_floor_id LEFT JOIN ms_rack ON ms_rack.rack_id = ms_rack_floor.rack_id where ms_freezer_floor.freezer_id = '".$_GET['fz']."' AND ms_freezer_floor.freezer_floor_id = '".$_GET['fz_f']."' AND ms_rack.rack_id = '".$_GET['r']."' AND ms_rack_floor.rack_floor_id = '".$_GET['r_f']."' ";
    $objQueryfz = $db_connection->query($sqlfz);
    while(($row = $objQueryfz->fetch_assoc()) != null){
        $fz_id = $row['freezer_id'];
        $fz_name = $row['freezer_name'];
        $fz_f_id = $row['freezer_floor_id'];
        $fz_f_name = $row['freezer_floor_name'];
        $fz_f_edit = $row['freezer_floor_edit'];
        $r_id = $row['rack_id'];
        $r_name = $row['rack_name'];
        $r_edit = $row['rack_edit'];
        $r_f_id = $row['rack_floor_id'];
        $r_f_name = $row['rack_floor_name'];
        $r_f_edit = $row['rack_floor_edit'];
    }
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

    <?php include("_css.php"); ?>
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Rack</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-10"> <h3 class="m-t-10"><?php echo @$fz_name." <i class='fa fa-chevron-right'></i> ".@$fz_f_edit." <i class='fa fa-chevron-right'></i> ".@$r_edit." <i class='fa fa-chevron-right'></i> ".@$r_f_edit; ?></h3> </div>
                                            <div class="col-md-2 text-right"> <button type="button" class="btn btn-secondary mb-1" onclick="window.history.back();"> Back </button> </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-success mb-1 m-t-10" data-toggle="modal" data-target="#inputboxmodel" style="width: 100%; font-size: 50px;"> <img src="images/box_in.png" width="70px" alt="CoolAdmin" id="iconimg"> Input Box </button>
                                            </div> 
                                            <div class="col-md-6">
                                               <button type="button" class="btn btn-success mb-1 m-t-10" data-toggle="modal" data-target="#inputtubemodel" style="width: 100%; font-size: 50px;"> <img src="images/tube_in.png" width="70px" alt="CoolAdmin" id="iconimg"> Input Tube </button>
                                            </div>
                                        </div>
                                        <hr>
                                        <table id="example" class="table table-responsive table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th width="1%" style="white-space: nowrap;">N</th>
                                                    <th width="1%" style="white-space: nowrap;">Box Barcode</th>
                                                    <th width="1%" style="white-space: nowrap;">Box Name</th>
                                                    <th width="1%" style="white-space: nowrap;">Manage</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                                $sql = "SELECT * FROM `tr_freezer_add` LEFT JOIN `ms_box` on ms_box.box_barcode = tr_freezer_add.box_barcode  WHERE `freezer_id` = '".$fz_id."' AND `freezer_floor_id` = '".$fz_f_id."' AND `rack_id` = '".$r_id."' AND `rack_floor_id` = '".$r_f_id."' AND freezeradd_status !=1 ";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    echo "<tr>
                                                        <td>".$i."</td>
                                                        <td>".$row['box_barcode']."</td>
                                                        <td>".$row['box_name']."</td>
                                                        <td><a href='input_box_add.php?fz=".$_GET['fz']."&box_bar=".$row['box_barcode']."'>
                                                        <button type='button' class='btn btn-sm btn-success'> Input Tube </button></a> </td>
                                                    </tr>";
                                                }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTAINER-->
        </div>

        <!-- modal medium -->
        <div class="modal fade" id="inputboxmodel" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="margin-top: 20%;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Input Box</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="tablet_add_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="input_barcode_box" name="input_barcode_box" placeholder="Barcode box">
                                        <input type="hidden" class="form-control" id="freezer_id" name="freezer_id" value="<?php echo $fz_id; ?>">
                                        <input type="hidden" class="form-control" id="freezer_floor_id" name="freezer_floor_id" value="<?php echo $fz_f_id; ?>">
                                        <input type="hidden" class="form-control" id="rack_id" name="rack_id" value="<?php echo $r_id; ?>">
                                        <input type="hidden" class="form-control" id="rack_floor_id" name="rack_floor_id" value="<?php echo $r_f_id; ?>">
                                        <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer"> 
                            <button type="submit" class="btn btn-primary" name="save_inputbox" value="save_inputbox">Confirm</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end modal medium -->

        <!-- modal medium -->
        <div class="modal fade" id="inputtubemodel" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="margin-top: 20%;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Input Tube</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="input_box_add.php?fz=<?php echo $_GET['fz']; ?>" method="post">
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="input_barcode_tube" name="input_barcode_tube" placeholder="Barcode box">
                                        <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" name="q" value="q">Confirm</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end modal medium -->

    <?php include("_footer.php"); ?>
    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        }

        $('#inputboxmodel').on('shown.bs.modal', function () {
            document.getElementById("input_barcode_box").value = "";
            $('#input_barcode_box').focus();
        });

        $('#inputtubemodel').on('shown.bs.modal', function () {
            document.getElementById("input_barcode_tube").value = "";
            $('#input_barcode_tube').focus();
        });

        var link = '';
        function move_fz(id,bar) {
            link = '';
            var n = <?php echo $i; ?>;
            var i;
            document.getElementById("bar_fz").innerHTML = bar;
            link = 'tablet_move_freezer_floor.php?id='+id+'&s=b';
            for(i=1;i<=n;i++){
                document.getElementById('move_link'+i).href = link;
            }
        }

        function move_fz_l(i,fz) {
            document.getElementById('move_link'+i).href = link+'&fz='+fz;
        }
    </script>
</body>
</html>
<!-- end document-->