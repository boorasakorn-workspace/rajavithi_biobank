<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: login.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }

    include("connect.php"); 
?> 
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

	<title>Biobank</title>
    <link rel="icon" type="image/ico" href="images/icon/logo_bio.ico" sizes="16x16">
	<?php include("_css.php"); ?>
	<?php include("./vendor/datatables/_css_datatable.php"); ?>
    <link href="vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">   
        #printable { display: block; }  
        @media print{    
             #non-printable { display: none; }   
             #printable { display: block; }   
        }
    </style>
</head>
<body class="animsition">
	<div class="page-wrapper">
		<!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- END HEADER MOBILE-->

		<!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- END MENU SIDEBAR-->
    <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
    <form action="sample_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
		<!-- PAGE CONTAINER-->
		<div class="page-container">
			<!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- HEADER DESKTOP-->

			<!-- MAIN CONTENT-->
			<div class="main-content">
				<div class="section__content section__content--p30">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-header">
										<h4>Tube</h4>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-md-6"><h4>Tube</h4></div>
											<div class="col-md-6 text-right"><a href="tube_barcode.php"><button type="button" class="btn btn-info mb-1" data-toggle="modal" data-target="#addmodal"> Print </button> </a></div>
										</div>
										<br>
										<table id="example" class="table table-responsive table-striped table-bordered" style="width:100%">
											<thead>
												<tr>
												    <th width="1%" style="white-space: nowrap;">N</th>
												    <th width="1%" style="white-space: nowrap; min-width: 100px;">LN</th>  
                                                    <th width="1%" style="white-space: nowrap; min-width: 100px;">Tube ID</th>
                                                    <th width="1%" style="white-space: nowrap; min-width: 100px;">Tube Barcode </th>
                                                    <th width="1%" style="white-space: nowrap; min-width: 100px;">Tube Name </th>
                                                    <th width="1%" style="white-space: nowrap; min-width: 100px;">Box Name</th>
                                                    <th width="1%" style="white-space: nowrap; min-width: 30px;">Well</th>
												    <th width="1%" style="white-space: nowrap; text-align: left;">Manage</th>
												</tr>
											</thead>
											<tbody>
											<?php 
                                                $sql = "SELECT * ,tr_sample.sample_sid as sam_id  FROM `tr_sample_tube` LEFT JOIN tr_sample on tr_sample.sample_id=tr_sample_tube.sample_sid LEFT JOIN tr_box_add on tr_box_add.sample_sid=tr_sample_tube.tube_barcode AND boxadd_status !=1 LEFT JOIN ms_box on ms_box.box_id=tr_box_add.box_id WHERE tube_status != 1 ORDER BY tr_sample_tube.tube_id DESC" ;/*   */
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    echo "<tr>
                                                        <td>".$i."</td>
                                                        <td>".$row['sam_id']."</td>
                                                        <td>".$row['sample_sid_tube']."</td>
                                                        <td>".$row['tube_barcode']."</td>
                                                        <td>".$row['tube_name']."</td>
                                                        <td>".$row['box_name']."</td>
                                                        <td>".$row['boxadd_well']."</td>
                                                        <td>";
                                                        if($row['box_name']==''){
                                                            echo "<button type='button' class='btn btn-danger btn-sm' data-toggle='modal' data-target='#delModal' data-toggle='tooltip' title='Delete' onclick='del(\"".$row['tube_id']."\",\"".$row['tube_barcode']."\")'>x
                                                            </button>
                                                            </td>";
                                                        }
                                                    echo "</tr>";
                                                }
                                            ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!-- /# column -->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->
		</div>

        <!-- modal static -->
        <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
             data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticModalLabel">Delete</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <label>Want to delete </label>
                        <label class="form-control-label" id="del_tube"></label> 
                        <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                        <input type="hidden" class="form-control" name="del_id" id="del_id">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="del_tube" value="del_tube">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>    
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal static -->
    </form>

	<?php include("_footer.php"); ?>
	<?php include("_js.php"); ?>
	<?php include("./vendor/datatables/_js_datatable.php"); ?>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>
    <!--  <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.th.min.js"></script> -->

	<script type="text/javascript">
		$(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        }

        function pn(id,ln) {
            document.getElementById("pn_tube").innerHTML = ln+"?";
            document.getElementById("pn_bar").innerHTML = "<img alt=\"Coding Sips\" src=\"barcode.php?size=20&text="+ln+"&print=true\" />";
            document.getElementById("pn_id").value = id;
        }

        function del(id,ln) {
            document.getElementById("del_tube").innerHTML = ln+"?";
            document.getElementById("del_id").value = id;
        }

        $(document).ready(function(){
            $("#sample_date").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', <?php echo date("d-m-Y"); ?>);
            });
        });

        $(document).ready(function(){
            $("#sample_hbd").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', '');
            });
        });

        $(document).ready(function(){
            $("#sample_date_edit").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', '');
            });
        });

        $(document).ready(function(){
            $("#sample_hbd_edit").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', '');
            });
        });

	</script>
</body>
</html>
<!-- end document-->