<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>404</title>

    <?php include("_css.php"); ?>
    
    <style type="text/css">
        h1 {
            font-weight: 700;
            color: #555;
            margin-top: 32px;
            margin-bottom: 32px;
            text-align: center;
            font-size: 80px;
        }
        #example1 {
            background: url(images/BioBank_Background.png) no-repeat left top; 
            background-size: cover;
        }
        h2 {
            font-weight: 400;
            color: #555;
            margin-top: 32px;
         
            text-align: center;
        }
        .btn-lg{
            padding: .4rem 2rem;
        }
        .login-wrap {
            padding-top: 20vh;
        }
    </style>
</head>

<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge4" id="example1">
           <!-- <img src="images/1.jpg" alt="CoolAdmin"><img src="images/icon/logo.png" alt="CoolAdmin"><img src="images/icon/logo.png" alt="CoolAdmin"> -->
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">
                                <h1>404</h1>
                                <h1>ERROR</h1>
                                <h2>PAGE NOT FOUND</h2>
                                <!-- <img src="images/icon/logo.png" alt="CoolAdmin"> -->
                            </a>
                        </div>
                        <div class="login-form">
                            <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
                            <form action="tablet_check.php" method="post" enctype="multipart/form-data" target="iframe_target">
                                <div class="row form-group">
                                    <div class="col col-md-12">
                                        <div class="input-group">
                                            
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include("_js.php"); ?>
</body>
</html>
<!-- end document-->