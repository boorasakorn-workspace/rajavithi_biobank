<!-- MENU SIDEBAR-->
<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="index.php">
            <img src="images/icon/RJBioBank.png" alt="Biobank" style="margin-left: 10%; width: 50%;"  />
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <div class="menu">
                <ul class="list-unstyled navbar__list">
                    <li id="index">
                        <a href="index.php"><i class="fas fa-desktop"></i>Dashboard</a> 
                    </li>
                    <?php
                        if($_SESSION["user_type"]!=2){
                            echo " <li id='sample'>
                                <a href='sample.php'><i class='fas fa-barcode'></i>Sample</a> 
                            </li>
                            <li id='input'>
                                <a href='input_freezer.php'><img src='images/arrow_r.png' alt='Smiley face' height='19' width='19' style='margin-right: 7%; box-sizing: border-box;'>Input</a> 
                            </li> ";
                        }
                    ?>
                    <li id="search">
                        <a href="search.php"><i class="fas fa-search"></i>Search</a> 
                    </li>
                    <li id="report">
                        <a href="report.php"><i class="fa fa-folder-open"></i>Report</a> 
                    </li>
                    <?php
                        if($_SESSION["user_type"]!=2){
                            echo "<li class='has-sub'>
                                <a class='js-arrow' href='#''><i class='fa fa-wrench'></i>Master</a>    
                                <ul class='list-unstyled navbar__sub-list js-sub-list' id='manage1'>
                                    <li id='tube'>
                                        <a href='tube.php'>Tube</a> 
                                    </li>
                                    <li id='box'>
                                        <a href='box.php'>Box</a> 
                                    </li>
                                    <li id='freezer'>
                                        <a href='freezer.php'>Freezer</a> 
                                    </li>
                                </ul>
                            </li>";
                        }
                    ?>
                    <li class="has-sub">
                        <a class="js-arrow" href="#"><i class="fa fa-cog"></i>Manage</a>    
                        <ul class="list-unstyled navbar__sub-list js-sub-list" id="manage">
                            <li id="account">
                                <a href="account.php">Account</a>
                            </li>
                            <li id="change_password">
                                <a href="change_password.php">Change Password</a>
                            </li>
                            <?php 
                                if($_SESSION["user_type"]==0){
                                    echo "<li id='user'>
                                        <a href='user.php'>User</a>
                                    </li>";
                                }
                            ?>            
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</aside>
<!-- END MENU SIDEBAR