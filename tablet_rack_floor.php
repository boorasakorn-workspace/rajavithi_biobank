<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: tablet_404.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }
    include("connect.php"); 
?> 
<?php 
    $sqlfz = "SELECT * FROM `ms_rack` LEFT JOIN ms_freezer ON ms_freezer.freezer_id = ms_rack.freezer_id LEFT JOIN ms_freezer_floor ON ms_freezer_floor.freezer_floor_id = ms_rack.freezer_floor_id where ms_freezer_floor.freezer_id = '".$_GET['fz']."' AND ms_freezer_floor.freezer_floor_id = '".$_GET['fz_f']."' AND ms_rack.rack_id = '".$_GET['r']."'";
    $objQueryfz = $db_connection->query($sqlfz);
    while(($row = $objQueryfz->fetch_assoc()) != null){
        $fz_id = $row['freezer_id'];
        $fz_name = $row['freezer_name'];
        $fz_f_id = $row['freezer_floor_id'];
        $fz_f_name = $row['freezer_floor_name'];
        $fz_f_edit = $row['freezer_floor_edit'];
        $r_id = $row['rack_id'];
        $r_name = $row['rack_name'];
        $r_edit = $row['rack_edit'];
    }
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

    <?php include("_css.php"); ?>
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
    <link href="vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #iconimg{
            width: 15%;
        }

        @media (max-width: 991px) {
            #iconimg{
                width: 35%;
            }

            div.dataTables_wrapper div.dataTables_filter input {
                margin-left: 0.5em;
                display: inline-block;
                width: 65%;
            }
        }
    </style>
</head>
<body class="animsition">
    <div class="page-wrapper">
            <div class="container">
                    <div class="login-content">
                <form action="<?php echo $_SERVER['SCRIPT_NAME'];?>" method="post" name="frmSearch">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Rack floor</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6"> 
                                                <img src="images/icon/logo-bio.png" alt="CoolAdmin" id="iconimg"> <h3 class="m-t-10"><?php echo @$fz_name." <i class='fa fa-chevron-right'></i> ".@$fz_f_edit." <i class='fa fa-chevron-right'></i> ".@$r_edit; ?></h3>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <button type="button" class="btn btn-secondary mb-1" onclick="window.history.back();"> Back </button>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row form-group">
                                        <?php 
                                            $sql = "SELECT * ,ms_rack_floor.rack_floor_id as r_f_id FROM `ms_rack_floor` where freezer_floor_id = ".$_GET['fz_f']." and freezer_id = ".$_GET['fz']." and rack_id = ".$_GET['r']." AND rack_floor_status !=1 ";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                $sqlbox = "SELECT * FROM `tr_freezer_add` where freezeradd_status=0 AND rack_floor_id=".$row['rack_floor_id']."";
                                                $objQuerybox = $db_connection->query($sqlbox);
                                                $sum_num = $objQuerybox->num_rows;
                                                /*echo $sum_num;
*/                                                $i++;
                                                echo "<div class='col-md-12 text-center p-b-20'>
                                                        <a href='tablet_add.php?fz=".$fz_id."&fz_f=".$fz_f_id."&r=".$r_id."&r_f=".$row['r_f_id']."'>
                                                            <img src='images/rack_floor_n.png' alt='Biobank' width='55%'/>  
                                                        </a>
                                                    <label style='color:black;font-size:22px;'>"; 
                                                    if($sum_num<4){
                                                        echo " <span class='badge badge-success' style='color:#28a745;' >0</span>";
                                                    }
                                                    else{
                                                        echo " <span class='badge badge-danger' style='color:#dc3545;' >0</span>";
                                                    }
                                                    echo " <b> ".$row['rack_floor_edit']." </b></label>
                                                </div>";
                                            }
                                        ?> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->
                        </div>
                     <!--    <div class="row">
                            <div class="col-md-6">
                                <button type="button" class="btn btn-success mb-1 m-t-10" style="width: 100%; font-size: 50px;" onclick="window.location.href='tablet_search.php?fz=<?php // echo $_GET['fz']; ?>'"> <img src="images/search.png" alt="CoolAdmin" id="iconimg" > Search</button>
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-danger mb-1 m-t-10" style="width: 100%; font-size: 50px;" onclick="window.location.href='tablet_freezer_floor.php?fz=<?php // echo $_GET['fz']; ?>'"> <img src="images/in_out.png" alt="CoolAdmin" id="iconimg" > Input/Output</button>
                            </div>
                        </div>
                        <br> -->
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <input type="button" class="btn btn-success m-t-10" value="Menu" onclick="window.location.href='tablet_menu.php?fz=<?php echo $_GET['fz']; ?>'" />
                                <input type="button" class="btn btn-info m-t-10 m-l-10" value="Logout" onclick="window.location.href='tablet_logout.php?fz=<?php echo $_GET['fz']; ?>'" />
                            </div>
                        </div>
            </div>
        </div>
    </div>
    <!-- modal medium -->
        <div class="modal fade" id="mediumModalinput" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="margin-top: 20%;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Input</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="input-group">
                                        <input type="email" id="input2-group2" name="input2-group2" placeholder="Barcode" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer"> 
                        <button type="button" class="btn btn-primary">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal medium -->
    </form>

    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>
    <!--  <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.th.min.js"></script> -->

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        }
    </script>
</body>
</html>
<!-- end document-->