<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: login.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }

    include("connect.php"); 
?> 
<?php 
    $sqlfz = "SELECT * FROM `ms_freezer` where freezer_id = ".$_GET['fzm']."";
    $objQueryfz = $db_connection->query($sqlfz);
    while(($row = $objQueryfz->fetch_assoc()) != null){
        $fz_id = $row['freezer_id'];
        $fz_name = $row['freezer_name'];
    }
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

    <?php include("_css.php"); ?>
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- END MENU SIDEBAR-->
        
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- HEADER DESKTOP-->
        <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Move <?php if($_GET['s']=='t'){ echo "Tube"; }elseif($_GET['s']=='b'){ echo "Box"; } ?> | Freezer floor</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6"><h3><?php echo @$fz_name; ?></h3></div>
                                            <div class="col-md-6 text-right p-t-10">
                                               <!--  <button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#add_modal"> Add Floor </button> -->
                                                <button type="button" class="btn btn-secondary mb-1" onclick="window.history.back();"> Back </button>
                                            </div>
                                        </div>   
                                        <br><!-- <br> -->
                                        <div class="row form-group">
                                        <?php 
                                            $sql = "SELECT * FROM `ms_freezer_floor` where freezer_id = ".$_GET['fzm']." AND freezer_floor_status != 1";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                $sqlrack = "SELECT * FROM `ms_rack_floor` LEFT JOIN ms_rack ON ms_rack.rack_id = ms_rack_floor.rack_id LEFT JOIN ms_freezer_floor ON ms_freezer_floor.freezer_floor_id = ms_rack.freezer_floor_id WHERE ms_rack_floor.freezer_floor_id=".$row['freezer_floor_id']." AND ms_rack.rack_status != 1 ";
                                                $objQueryrack = $db_connection->query($sqlrack);
                                                $max_rack = $objQueryrack->num_rows;
                                                if($max_rack==0){
                                                    $max_rack = 10;
                                                }
                                                else{
                                                    $max_rack = $max_rack*4;
                                                }
                                                $sqlbox = "SELECT * FROM `tr_freezer_add` LEFT JOIN ms_rack_floor ON ms_rack_floor.rack_floor_id = tr_freezer_add.rack_floor_id LEFT JOIN ms_rack ON ms_rack.rack_id = ms_rack_floor.rack_id where freezeradd_status=0 AND ms_rack_floor.freezer_floor_id=".$row['freezer_floor_id']."";
                                                $objQuerybox = $db_connection->query($sqlbox);
                                                $sum_num = $objQuerybox->num_rows;
                                                $i++;
                                                echo "<div class='col-md-12 text-center'>
                                                    <a href='input_move_rack.php?id=".$_GET['id']."&s=".$_GET['s']."&fz=".$_GET['fz']."&fzm=".$_GET['fzm']."&fz_f=".$row['freezer_floor_id']."'>
                                                        <img src='images/freezer_floor_n.png' alt='Biobank'/>
                                                    </a>
                                                    <h3 class='text-center p-t-10 p-b-20'>".$row['freezer_floor_edit']." ";
                                                        if($sum_num<$max_rack){
                                                            echo " <span class='badge badge-success' style='color:#28a745;' >0</span>";
                                                        }
                                                        else{
                                                            echo " <span class='badge badge-danger' style='color:#dc3545;' >0</span>";
                                                        }
                                                echo "</h3>
                                                </div>";
                                            }
                                        ?>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTAINER-->
        </div>

        <!-- Add modal -->
        <div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Add Freezer floor</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="freezer_floor_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                    <div class="modal-body">
                        <div class="row form-group">
                            <div class="col col-md-12">
                                <label class=" form-control-label">Add Freezer floor</label>
                            </div>
                            <?php
                                $numfz = '0';
                                $numfz = substr("000".$fz_id, -3);
                            ?>
                            <div class="col col-md-12 m-l-30">
                                <div class="form-check">
                                    <div class="checkbox">
                                        <label for="checkbox1" class="form-check-label ">
                                            <input type="hidden" class="form-control" name="freezer_id" value="<?php echo $fz_id; ?>">
                                            <input type="checkbox" class="form-check-input" id="name_a" name="name_a" value="A<?php echo $numfz; ?>" 
                                            <?php  
                                                $name_a = 'A'.$numfz;
                                                $sql = "SELECT * FROM `ms_freezer_floor` where freezer_id = ".$_GET['fz']." AND  freezer_floor_name = 'A".$numfz."'";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    if($row['freezer_floor_status']==0){
                                                        echo "checked";
                                                    }
                                                    $name_a = $row['freezer_floor_edit'];
                                                }
                                            ?> 
                                            ><?php echo $name_a; ?>
                                            <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label for="checkbox2" class="form-check-label ">
                                            <input type="checkbox" class="form-check-input" id="name_b" name="name_b" value="B<?php echo $numfz; ?>" 
                                            <?php  
                                                $name_b = 'B'.$numfz;
                                                $sql = "SELECT * FROM `ms_freezer_floor` where freezer_id = ".$_GET['fz']." AND  freezer_floor_name = 'B".$numfz."'";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    if($row['freezer_floor_status']==0){
                                                        echo "checked";
                                                    }
                                                    $name_b = $row['freezer_floor_edit'];
                                                }
                                            ?>
                                            ><?php echo $name_b; ?>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label for="checkbox3" class="form-check-label ">
                                            <input type="checkbox" class="form-check-input" id="name_c" name="name_c" value="C<?php echo $numfz; ?>"
                                            <?php  
                                                $name_c = 'C'.$numfz;
                                                $sql = "SELECT * FROM `ms_freezer_floor` where freezer_id = ".$_GET['fz']." AND  freezer_floor_name = 'C".$numfz."'";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    if($row['freezer_floor_status']==0){
                                                        echo "checked";
                                                    }
                                                    $name_c = $row['freezer_floor_edit'];
                                                }
                                            ?>
                                            ><?php echo $name_c; ?>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label for="checkbox3" class="form-check-label ">
                                            <input type="checkbox" class="form-check-input" id="name_d" name="name_d" value="D<?php echo $numfz; ?>"
                                            <?php  
                                                $name_d = 'D'.$numfz;
                                                $sql = "SELECT * FROM `ms_freezer_floor` where freezer_id = ".$_GET['fz']." AND  freezer_floor_name = 'D".$numfz."'";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    if($row['freezer_floor_status']==0){
                                                        echo "checked";
                                                    }
                                                    $name_d = $row['freezer_floor_edit'];
                                                }
                                            ?>
                                            ><?php echo $name_d; ?>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label for="checkbox3" class="form-check-label ">
                                            <input type="checkbox" class="form-check-input" id="name_e" name="name_e" value="E<?php echo $numfz; ?>"
                                            <?php  
                                                $name_e = 'E'.$numfz;
                                                $sql = "SELECT * FROM `ms_freezer_floor` where freezer_id = ".$_GET['fz']." AND  freezer_floor_name = 'E".$numfz."'";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    if($row['freezer_floor_status']==0){
                                                        echo "checked";
                                                    }
                                                    $name_e = $row['freezer_floor_edit'];
                                                }
                                            ?>
                                            ><?php echo $name_e; ?>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label for="checkbox3" class="form-check-label ">
                                            <input type="checkbox" class="form-check-input" id="name_f" name="name_f" value="F<?php echo $numfz; ?>"
                                            <?php  
                                                $name_f = 'F'.$numfz;
                                                $sql = "SELECT * FROM `ms_freezer_floor` where freezer_id = ".$_GET['fz']." AND  freezer_floor_name = 'F".$numfz."'";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    if($row['freezer_floor_status']==0){
                                                        echo "checked";
                                                    }
                                                    $name_f = $row['freezer_floor_edit'];
                                                }
                                            ?>
                                            ><?php echo $name_f; ?>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label for="checkbox3" class="form-check-label ">
                                            <input type="checkbox" class="form-check-input" id="name_g" name="name_g" value="G<?php echo $numfz; ?>"
                                            <?php  
                                                $name_g = 'G'.$numfz;
                                                $sql = "SELECT * FROM `ms_freezer_floor` where freezer_id = ".$_GET['fz']." AND  freezer_floor_name = 'G".$numfz."'";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    if($row['freezer_floor_status']==0){
                                                        echo "checked";
                                                    }
                                                    $name_g = $row['freezer_floor_edit'];
                                                }
                                            ?>
                                            ><?php echo $name_g; ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>       
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="save_freezer_floor" value="save_freezer_floor">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- Add modal -->
        <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Edit Name</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="freezer_floor_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                    <div class="modal-body">
                        <div class="row">            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" class="form-control" name="name_edit" id="name_edit" >
                                    <input type="hidden" class="form-control" name="id_edit" id="id_edit">
                                    <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="edit_save" value="edit_save">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- modal static -->
        <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
             data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticModalLabel">Delete</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                     <form action="freezer_floor_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                    <div class="modal-body">
                        <label>Want to delete </label>
                        <label class="control-label" id="name_del"></label>
                        <input type="hidden" class="form-control" name="del_id" id="del_id">
                        <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="del_save" value="del_save">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>    
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- end modal static -->

    <?php include("_footer.php"); ?>
    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>

    <script type="text/javascript">
        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        } 

        function edit(id,name){
            document.getElementById("id_edit").value = id;
            document.getElementById("name_edit").value = name;
        }

        function del(id,name){
            document.getElementById("del_id").value = id;
            document.getElementById("name_del").innerHTML = name+"?";
        }
    </script>
</body>
</html>
<!-- end document-->