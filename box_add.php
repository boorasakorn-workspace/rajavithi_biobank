<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: login.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }
    include("connect.php"); 
?> 
<?php
    $sql = "SELECT * FROM `ms_box` where box_id = ".$_GET['idb']." ";
    $objQuery = $db_connection->query($sql);
    $i=0;
    while(($row = $objQuery->fetch_assoc()) != null){
        $box_name = $row['box_name'];
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

    <?php include("_css.php"); ?>
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">    
                            <div class="col-md-12">
                            <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
                            <form action="box_add_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Box Add</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6"><h4><?php echo $box_name; ?></h4></div>
                                            <div class="col-md-6 text-right">
                                                <!-- <button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#add_modal"> Add Box </button>  -->
                                                <button type="button" class="btn btn-secondary mb-1" onclick="window.history.back();"> Back </button>
                                            </div>
                                        </div>   
                                        <br><br>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <table id="example" class="table table-responsive table-striped table-bordered" style="width:100%">
                                                    <tr>
                                                        <th width="1%" style="white-space: nowrap; text-align: center;">N</th> 
                                                        <th width="1%" style="white-space: nowrap; text-align: left;">Barcode</th>
                                                        <th width="1%" style="white-space: nowrap; text-align: left;">Well</th>
                                                        <th width="1%" style="white-space: nowrap; text-align: left;">M</th>
                                                    </tr>
                                                    <?php
                                                        $sql = "SELECT * FROM `tr_box_add` where box_id = '".$_GET['idb']."'";
                                                        $objQuery = $db_connection->query($sql);
                                                        $i=0;
                                                        while(($row = $objQuery->fetch_assoc()) != null){
                                                            if($row['boxadd_status']!=1){
                                                                $i++;
                                                                echo "<tr>
                                                                    <td class='text-center'>".$i."</td>
                                                                    <td>".$row['sample_sid']."</td>
                                                                    <td>".$row['boxadd_well']."</td>
                                                                    <td>
                                                                        <button type='button' class='btn btn-danger btn-sm' data-toggle='modal' data-target='#del_model' data-toggle='tooltip' title='Delete' onclick='delbox(\"".$row['boxadd_id']."\",\"".$row['sample_sid']."\")'>x
                                                                        </button>
                                                                    </td>
                                                                </tr>";
                                                            }
                                                        }  /*<button type='button' data-toggle='modal' data-target='#del_model' data-toggle='tooltip' title='Edit' style='color:#666;' onclick='delbox(\"".$row['boxadd_id']."\",\"".$row['sample_sid']."\")'>
                                                                        <i class='zmdi zmdi-delete'></i></button>*/
                                                    ?>
                                                </table>
                                            </div>
                                            <div class="col-md-7">
                                                <table id="example" class="table table-responsive table-striped table-bordered text-center" style="width:100%">
                                                <?php
                                                    $r = $_GET['r'];
                                                    $r = $r+65;
                                                    $c = $_GET['c'];
                                                    echo "<tr>
                                                        <td></td>";
                                                        for($i=1;$i<=$c;$i++){
                                                            echo "<th width='1%'>".$i."</th>";
                                                        }
                                                    echo"</tr>";
                                                    for($i=65;$i<$r;$i++){                                                    
                                                        echo "<tr>";
                                                            echo "<th width='1%'>".chr($i)."</th>";
                                                            for($j=1;$j<=$c;$j++){  
                                                                $well = chr($i).$j;
                                                                $sql = "SELECT * FROM `tr_box_add` where box_id = '".$_GET['idb']."' AND boxadd_well = '".$well."' AND boxadd_status != 1";
                                                                $objQuery = $db_connection->query($sql);
                                                                $row = $objQuery->fetch_assoc();
                                                                /* while(($row = $objQuery->fetch_assoc()) != null){
                                                                    if($row['boxadd_well']!=''){
                                                                        $dd = $row['boxadd_well'];
                                                                        echo $dd."<br>";
                                                                    }
                                                                    else{
                                                                        $dd = '';
                                                                        echo $dd."<br>";
                                                                        /*echo "<td>
                                                                            <button type='button' data-toggle='modal' data-target='#add_model'  style='' onclick='addtube(\"".$_GET['idb']."\",\"".$well."\",)'> ".$well." </button>";
                                                                        echo "</td>";*/
                                                                /*     }
                                                                }*/
                                                                if($row['boxadd_well']!=''){
                                                                    echo "<td style='background-color: #ffc107; color: #212529'>";
                                                                    echo $row['boxadd_well'];
                                                                    echo "</td>";
                                                                }
                                                                else{
                                                                    echo "<td>
                                                                        <button type='button' data-toggle='modal' data-target='#add_model'  style='' onclick='addtube(\"".$_GET['idb']."\",\"".$well."\",)'> ".$well." </button>";
                                                                    echo "</td>";
                                                                }                                       
                                                            }
                                                        echo "</tr>";
                                                    }
                                                   /* $sql = "SELECT * FROM `tr_box_add` where box_id = '".$_GET['idb']."'";
                                                    $objQuery = $db_connection->query($sql);
                                                    $i=0;
                                                    while(($row = $objQuery->fetch_assoc()) != null){
                                                        $i++;
                                                        $asd[$row['boxadd_well']] = $row['boxadd_well'];
                                                        $st[$row['boxadd_well']] = $row['boxadd_status']; 
                                                    }*/

                                                    /*for($i=0;$i<$n;$i++){
                                                        echo "<tr>";        
                                                        for($j=0; $j<$m;$j++){ 

                                                            echo "<td width='1%' ";
                                                            if($num == @$asd[$num]&&@$st[$num]!=1){ echo "style='background-color: #ffc107;'";}
                                                            echo ">
                                                                <button type='button' data-toggle='modal' data-target='#add_model'  style='' onclick='addtube(\"".$_GET['idb']."\",\"".$num."\",)'>
                                                                    ".$num."
                                                                </button></td> ";
                                                            $num++;
                                                        }
                                                        echo "</tr>";     
                                                    }*/
                                                ?>
                                                </table>
                                                <center>Open</center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTAINER-->
        </div>

        <!-- Add modal -->
        <div class="modal fade" id="add_model" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
             data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticModalLabel">Add Tube in box</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label" id="well_box"></label><br>
                                    <label class="control-label">Barcode Tube</label>
                                    <input type="text" class="form-control" name="sample_id" id="sample_id">
                                    <input type="hidden" class="form-control" name="well" id="well">
                                    <input type="hidden" class="form-control" name="box_id" id="box_id">
                                    <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                </div>
                            </div>               
                        </div>
                    </div>
                    <div class="modal-footer"> 
                        <button type="submit" class="btn btn-primary" name="save_boxadd" value="save_boxadd">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div> 
                </div>
            </div>
        </div>
       <!--  <div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Add Box</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">ID Sample</label>
                                    <input type="text" class="form-control" name="sample_id">
                                    <input type="hidden" class="form-control" name="box_id" value="<?php // echo $_GET['idb']; ?>">
                                </div>
                            </div>               
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Well</label>
                                    <input type="text" class="form-control" name="well">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">    
                        <button type="submit" class="btn btn-primary" name="save_boxadd" value="save_boxadd">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- end modal scroll -->

        <!-- modal static -->
        <div class="modal fade" id="del_model" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
             data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticModalLabel">Delect</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Want to delete </label>
                                    <label class="form-control-label" id="del_bar"></label>  
                                    <input type="hidden" class="form-control" id="del_id" name="del_id">
                                    <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer"> 
                        <button type="submit" class="btn btn-primary" name="del_boxadd" value="del_boxadd">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div> 
                </div>
            </div>
        </div>
        <!-- end modal static -->
    </form>

    <?php include("_footer.php"); ?>
    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        }
            
        function delbox(id,bar) {
            document.getElementById("del_id").value = id;
            document.getElementById("del_bar").innerHTML = bar+'?';
        }

        function addtube(id,num) {
            document.getElementById("box_id").value = id;
            document.getElementById("well").value = num;
            document.getElementById("well_box").innerHTML = num;
        }

        $('#add_model').on('shown.bs.modal', function () {
            $('#sample_id').focus()
        });
    </script>
</body>
</html>
<!-- end document-->