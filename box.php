<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: login.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }
    include("connect.php"); 
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

    <?php include("_css.php"); ?>
    <?php include("connect.php"); ?> 
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
    <link href="vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        h2{
            color: #555;
            margin-top: 15px;
            text-align: center;
        }
        .btn-lg{
            margin-top: 15px;
            padding: .3rem 2rem;
        }
        .lead{
             margin-top: 15px;
        }
        .sweet-alert{
            background-color: #f2f2f2;
        }
        #red{
            color: red;
        }
    </style>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                            <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
                            <form action="box_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Box</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6"><h4>Box</h4></div>
                                            <div class="col-md-6 text-right">
                                                <button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#add_modal"> Add Box </button>
                                                <a href="box_barcode.php"><button type="button" class="btn btn-info mb-1" data-toggle="modal" data-target="#addmodal"> Print </button> </a>
                                            </div>
                                        </div>
                                        <br>    
                                        <table id="example" class="table table-responsive table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th width="1%" style="white-space: nowrap;">N</th>
                                                    <th width="1%" style="white-space: nowrap; text-align: left; min-width: 100px;">Barcode</th>
                                                    <th width="1%" style="white-space: nowrap; text-align: left; min-width: 100px;">Name</th>
                                                    <th width="1%" style="white-space: nowrap; text-align: left; min-width: 40px;">Size</th>
                                                    <th width="1%" style="white-space: nowrap; text-align: left; min-width: 200px;">Location</th>
                                                    <th width="1%" style="white-space: nowrap; text-align: left; min-width: 120px;">Manage</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                                $sql = "SELECT * , ms_box.box_barcode as box_bar ,tr_freezer_add.freezer_id as fz_id ,tr_freezer_add.freezer_floor_id as fz_f_id FROM `ms_box` LEFT JOIN `tr_freezer_add` ON tr_freezer_add.box_barcode=ms_box.box_barcode AND tr_freezer_add.freezeradd_status!=1 LEFT JOIN `ms_freezer` ON ms_freezer.freezer_id=tr_freezer_add.freezer_id AND ms_freezer.freezer_status!=1 LEFT JOIN `ms_freezer_floor` ON ms_freezer_floor.freezer_floor_id=tr_freezer_add.freezer_floor_id AND ms_freezer_floor.freezer_floor_status!=1 LEFT JOIN `ms_rack` ON ms_rack.rack_id=tr_freezer_add.rack_id AND ms_rack.rack_status!=1 LEFT JOIN `ms_rack_floor` ON ms_rack_floor.rack_floor_id=tr_freezer_add.rack_floor_id AND ms_rack_floor.rack_floor_status!=1 WHERE `box_status`!=1 ORDER BY ms_box.box_id DESC";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    echo "<tr>
                                                        <td>".$i."</td>
                                                        <td>".$row['box_bar']."</td>
                                                        <td>".$row['box_name']."</td>
                                                        <td>".$row['box_row']*$row['box_colum']."</td>";
                                                    echo "<td>";
                                                    if($row['fz_id']!=''){
                                                        echo $row['freezer_name'];
                                                    }

                                                    if($row['fz_f_id']!=''){
                                                        echo " > ".$row['freezer_floor_edit'];
                                                    }

                                                    if($row['rack_id']!=''){
                                                        echo " > ".$row['rack_edit'];
                                                    }

                                                    if($row['rack_floor_id']!=''){
                                                        echo " > ".$row['rack_floor_edit'];
                                                    }
                                                    echo "</td>";
                                                    echo "<td>
                                                        <button type='button' class='btn btn-warning btn-sm' data-toggle='modal' data-target='#edit_modal' data-toggle='tooltip' title='Edit' onclick='editbox(\"".$row['box_id']."\",\"".$row['box_bar']."\",\"".$row['box_name']."\",\"".$row['box_date']."\",\"".$row['box_time']."\",\"".$row['box_row']."\",\"".$row['box_colum']."\")'>
                                                                Edit
                                                        </button> 
                                                        <a href='box_add.php?idb=".$row['box_id']."&r=".$row['box_row']."&c=".$row['box_colum']."'>
                                                            <button type='button' class='btn btn-primary btn-sm' data-toggle='tooltip' title='Tube'> Tube</button>
                                                        </a> ";
                                                        if($row['fz_id']==''){
                                                            echo "<button type='button' class='btn btn-danger btn-sm' data-toggle='modal' data-target='#delModal' data-toggle='tooltip' title='Delete' onclick='del(\"".$row['box_id']."\",\"".$row['box_bar']."\")'>x
                                                            </button>";
                                                        }
                                                    echo "</td>
                                                    </tr>";
                                                }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTAINER-->
        </div>

        <!-- Add modal -->
        <div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Add Box</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <?php
                        $sqlbarcode = "SELECT MAX(`box_barcode`) AS last_ba FROM ms_box";
                        $objbarcode = $db_connection->query($sqlbarcode);
                        $rowba = $objbarcode->fetch_assoc();
                        $maxba = substr($rowba['last_ba'], -4);  //ข้อมูลนี้จะติดรหัสตัวอักษรด้วย ตัดเอาเฉพาะตัวเลขท้ายนะครับ
                        $maxba = ($maxba + 1);
                        $maxba = substr("0000".$maxba, -4);
                        $num_ba = "2".$maxba;
                    ?>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Barcode <a id="red">*</a></label>
                                    <input type="text" class="form-control" name="barcode_add" value="<?php echo $num_ba; ?>" readonly>
                                    <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                </div>
                            </div>               
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" class="form-control" name="name_add">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Date</label>
                                    <input type="text" class="form-control" id="date_add" name="date_add" value="<?php echo date("d-m-Y"); ?>" maxlength="10">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Time</label>
                                    <input type="time" class="form-control" name="time_add" value="<?php echo date("H:i"); ?>">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Row Box</label>
                                    <input type="number" class="form-control" id='row_box' name="row_box" value="1" min="1" >
                                </div>
                            </div>  
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Colum Box</label>
                                    <input type="number" class="form-control" id='col_box' name="col_box" value="1" min="1">
                                </div>
                            </div>               
                        </div>
                    </div>
                    <div class="modal-footer">    
                        <button type="submit" class="btn btn-primary" name="save_box" value="save_box">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- modal scroll -->
        <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Edit Box</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <input type="hidden" class="form-control" id="edit_id" name="id_edit">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Barcode <a id="red">*</a></label>
                                    <input type="text" class="form-control" id="edit_bar" name="barcode_edit" readonly>
                                </div>
                            </div>               
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" class="form-control" id="edit_name" name="name_edit">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Date</label>
                                    <input type="text" class="form-control" id="edit_date" name="date_edit" maxlength="10">
                                </div>
                            </div>    
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Time</label>
                                    <input type="time" class="form-control" id="edit_time" name="time_edit">
                                </div>
                            </div>    
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Row Box</label>
                                    <input type="number" class="form-control" id="edit_row" name="edit_row" value="" min="1">
                                </div>
                            </div>  
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Colum Box</label>
                                    <input type="number" class="form-control" id="edit_col" name="edit_col" value="" min="1">
                                </div>
                            </div>                
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="edit_box" value="edit_box">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- modal static -->
        <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
             data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticModalLabel">Delete</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <form action="box_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                    <div class="modal-body">
                        <label>Want to delete </label>
                        <label class="form-control-label" id="del_ln"></label>
                        <input type="hidden" class="form-control" name="del_id" id="del_id">
                        <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="del_box" value="del_box">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>    
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- end modal static -->
    </form>

    <?php include("_footer.php"); ?>
    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
            else if(result==0){
                swal({
                    title: "Fail",
                    text: "Input Barcode box",
                });
            }
            else if(result==4){
                swal({
                    title: "Fail",
                    text: "Duplicate Barcode",
                });
            }
        }
            
        function editbox(id,barcode,name,date,time,row,col) {
            document.getElementById("edit_id").value = id;
            document.getElementById("edit_bar").value = barcode;
            document.getElementById("edit_name").value = name; 
            document.getElementById("edit_date").value = date; 
            document.getElementById("edit_time").value = time;
            document.getElementById("edit_row").value = row;
            document.getElementById("edit_col").value = col;
        }

        $(document).ready(function(){
            $("#date_add").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', <?php echo date("d-m-Y"); ?>);
            });
        });

        $(document).ready(function(){
            $("#edit_date").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', );
            });
        });

        var row_box = document.getElementById('row_box');
        row_box.onkeydown = function(e) {
            if(!((e.keyCode > 95 && e.keyCode < 106)
              || (e.keyCode > 47 && e.keyCode < 58) 
              || e.keyCode == 8)) {
                return false;
            }
        }

        var col_box = document.getElementById('col_box');
        col_box.onkeydown = function(e) {
            if(!((e.keyCode > 95 && e.keyCode < 106)
              || (e.keyCode > 47 && e.keyCode < 58) 
              || e.keyCode == 8)) {
                return false;
            }
        }

        var edit_row = document.getElementById('edit_row');
        edit_row.onkeydown = function(e) {
            if(!((e.keyCode > 95 && e.keyCode < 106)
              || (e.keyCode > 47 && e.keyCode < 58) 
              || e.keyCode == 8)) {
                return false;
            }
        }

        var edit_col = document.getElementById('edit_col');
        edit_col.onkeydown = function(e) {
            if(!((e.keyCode > 95 && e.keyCode < 106)
              || (e.keyCode > 47 && e.keyCode < 58) 
              || e.keyCode == 8)) {
                return false;
            }
        }

        function del(id,ln) {
            document.getElementById("del_ln").innerHTML = ln+"?";
            document.getElementById("del_id").value = id;
        }
    </script>
</body>
</html>
<!-- end document-->