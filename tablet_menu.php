<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: tablet_404.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }
    include("connect.php"); 
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

    <?php include("_css.php"); ?>
    <style type="text/css">
        #example1 {
            background: url(images/BioBank_Background.png) no-repeat left top; 
            background-size: cover;
            min-height:730px;
        }

        #iconimg{
            width: 15%;
        }

        .container {
            padding-top: 70px;
            padding-right: 0px;
            padding-left: 0px;
            padding-bottom: 0px;
            margin-right: auto;
            margin-left: auto;
        }

        @media (max-width: 991px) {
            #iconimg{
                width: 35%;
            }

            div.dataTables_wrapper div.dataTables_filter input {
                margin-left: 0.5em;
                display: inline-block;
                width: 65%;
            }
        }
    </style>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <div class="container">
            <div class="login-content" id="example1">
                <div class="login-logo" style="margin-top: 5%; width: 150px; height: 150px;">
                    <!-- <img src="images/icon/logo-bio.png" alt="CoolAdmin" style="width: 150px; height: 150px;"> -->
                </div>
                <div class="login-form" style="margin-top: 5%;">
                    <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
                    <form action="" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="button" class="btn btn-success mb-1 m-t-10" style="width: 100%; font-size: 50px;" onclick="window.location.href='tablet_search.php?fz=<?php echo $_GET['fz']; ?>'"> <img src="images/search.png" alt="CoolAdmin" id="iconimg" width="10%"> Search</button>
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-success mb-1 m-t-10" style="width: 100%; font-size: 50px;" onclick="window.location.href='tablet_freezer_floor.php?fz=<?php echo $_GET['fz']; ?>'"> <img src="images/in.png" alt="CoolAdmin" id="iconimg" width="10%"> Input</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <button type="button" class="btn btn-danger mb-1 m-t-10" data-toggle="modal" data-target="#outputtubemodel" style="width: 100%; font-size: 50px;"> <img src="images/tube_out.png" alt="CoolAdmin" id="iconimg"> Output Tube </button>
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-danger mb-1 m-t-10" data-toggle="modal" data-target="#outputboxmodel" style="width: 100%; font-size: 57px;"> <img src="images/box_out.png" alt="CoolAdmin" id="iconimg"> Output Box </button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12 text-center" style="margin-top: 7px;">
                                <input type="button" class="btn btn-info mb-1 m-t-10" value="Logout" onclick="window.location.href='tablet_logout.php?fz=<?php echo $_GET['fz']; ?>'" />
                            </div>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>

        <!-- modal medium -->
        <div class="modal fade" id="outputtubemodel" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="margin-top: 20%;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Output Tube</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="tablet_add_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="output_barcode_tube" name="output_barcode_tube" placeholder="Barcode tube">
                                        <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" id="save_outputtube" name="save_outputtube" value="save_outputtube">Confirm</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end modal medium -->

        <!-- modal medium -->
        <div class="modal fade" id="outputboxmodel" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="margin-top: 20%;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Output Box</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="tablet_add_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="output_barcode_box" name="output_barcode_box" placeholder="Barcode box">
                                        <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" id="save_outputbox" name="save_outputbox" value="save_outputbox">Confirm</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end modal medium -->
    </div>
    <?php include("_js.php"); ?>
    <script type="text/javascript">
        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        }

        $('#outputboxmodel').on('shown.bs.modal', function () {
            document.getElementById("output_barcode_box").value = "";
            $('#output_barcode_box').focus();
        });

        $('#outputtubemodel').on('shown.bs.modal', function () {
            document.getElementById("output_barcode_tube").value = "";
            $('#output_barcode_tube').focus();
        });
        //////////////////
        $('#output_barcode_tube').keyup(function(){
            if(this.value.length !=''){
                $('#save_outputtube').click();
            }
        });

        $('#output_barcode_box').keyup(function(){
            if(this.value.length !=''){
                $('#save_outputbox').click();
            }
        });

    </script>
</body>
</html>
<!-- end document-->