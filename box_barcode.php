<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: login.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }
    include("connect.php"); 
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>
    <!-- CSS -->
    <?php include("_css.php"); ?>
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">   
                               <!--  <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe> -->
                                <form action="box_print.php" method="post" >
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Print Barcode Box</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6"><h4><?php echo $box_name; ?></h4></div>
                                            <div class="col-md-6 text-right">
                                                <button type="submit" class="btn btn-primary mb-1" name="save_boxadd" value="save_boxadd">Confirm</button>
                                                <button type="button" class="btn btn-secondary mb-1" onclick="window.history.back();"> Back </button>
                                            </div>
                                        </div>   
                                        <br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table id="example" class="table table-responsive table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="1%" style="text-align: center;"><input type="checkbox" class="form-check-input" id="checkall" > All</th> 
                                                            <th width="1%" style="white-space: nowrap; text-align: left;">Barcode Box</th>
                                                            <th width="1%" style="white-space: nowrap; text-align: left;">Name Box</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                        $sql = "SELECT * FROM `ms_box` where box_status != 1";
                                                        $objQuery = $db_connection->query($sql);
                                                        $i=0;
                                                        while(($row = $objQuery->fetch_assoc()) != null){
                                                            if($row['boxadd_status']!=1){
                                                                $i++;
                                                                echo "<tr>
                                                                    <td class='text-center'><input type='checkbox' class='form-check-input checkcheck' name='box_check[]' value='".$row['box_barcode']."'></td>
                                                                    <td>".$row['box_barcode']."</td>
                                                                    <td>".$row['box_name']."</td>    
                                                                </tr>";
                                                            }
                                                        }  
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                </form>
                            </div>
                            <!-- /# column -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
    
    <?php include("_footer.php"); ?>
    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        }
            
        function delbox(id,bar) {
            document.getElementById("del_id").value = id;
            document.getElementById("del_bar").innerHTML = bar;
        }

        function addtube(id,num) {
            document.getElementById("box_id").value = id;
            document.getElementById("well").value = num;
            document.getElementById("well_box").innerHTML = num;
        }

        $('#add_model').on('shown.bs.modal', function () {
            $('#sample_id').focus()
        });

        $('#checkall').change(function () {
            $('.checkcheck').prop('checked',this.checked);
        });

        $('.checkcheck').change(function () {
            if($('.checkcheck:checked').length == $('.checkcheck').length){
                $('#checkall').prop('checked',true);
            }
            else{
                $('#checkall').prop('checked',false);   
            }
        });
    </script>
</body>
</html>
<!-- end document-->