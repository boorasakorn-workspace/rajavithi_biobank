<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: login.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }

    include("connect.php"); 
?> 
<?php 
    $sqlfz = "SELECT * FROM `ms_freezer_floor` LEFT JOIN ms_freezer ON ms_freezer.freezer_id = ms_freezer_floor.freezer_id where ms_freezer_floor.freezer_id = '".$_GET['fz']."' AND ms_freezer_floor.freezer_floor_id = '".$_GET['fz_f']."'";
    $objQueryfz = $db_connection->query($sqlfz);
    while(($row = $objQueryfz->fetch_assoc()) != null){
        $fz_id = $row['freezer_id'];
        $fz_name = $row['freezer_name'];
        $fz_f_id = $row['freezer_floor_id'];
        $fz_f_name = $row['freezer_floor_name'];
        $fz_f_edit = $row['freezer_floor_edit'];
    }
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

    <?php include("_css.php"); ?>
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
                       
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Rack</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6"><h3><?php echo @$fz_name." <i class='fa fa-chevron-right'></i> ".@$fz_f_edit; ?></h3></div>
                                            <div class="col-md-6 text-right">
                                                <button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#add_modal"> Add Rack </button>
                                                <button type="button" class="btn btn-secondary mb-1" onclick="window.history.back();"> Back </button>
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="row">
                                            <?php 
                                                $sql = "SELECT * FROM `ms_rack` where freezer_floor_id = '".$_GET['fz_f']."' and freezer_id = '".$_GET['fz']."' AND rack_status != 1";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    echo "<div class='col-md-4 text-center'>
                                                        <a href='rack_floor.php?fz=".$fz_id."&fz_f=".$fz_f_id."&r=".$row['rack_id']."'>
                                                            <img src='images/rack_n.png' alt='Biobank'/>
                                                        </a>
                                                        <h3 class='text-center p-t-10 p-b-20'>".$row['rack_edit']." <button type='button' class='btn btn-warning btn-sm' data-toggle='modal' data-target='#edit_modal' data-toggle='tooltip' title='Edit' onclick='edit(\"".$row['rack_id']."\",\"".$row['rack_edit']."\")'>Edit</button> <button type='button' class='btn btn-danger btn-sm' data-toggle='modal' data-target='#delModal' data-toggle='tooltip' title='Delete' onclick='del(\"".$row['rack_id']."\",\"".$row['rack_edit']."\")'>x</button>
                                                        </h3>
                                                    </div>";
                                                }
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTAINER-->
        </div>

        <!-- Add modal -->
        <div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Add Rack</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="rack_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                    <div class="modal-body">
                        <div class="col-md-12 m-l-30">
                            <div class="form-check">
                                <div class="checkbox">
                                    <label for="checkbox1" class="form-check-label ">
                                        <input type="hidden" class="form-control" name="fz_id" value="<?php echo $fz_id; ?>">
                                        <input type="hidden" class="form-control" name="fz_f_id" value="<?php echo $fz_f_id; ?>">
                                        <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                        <input type="checkbox" class="form-check-input" id="name_r1" name="name_r1" value="<?php echo $fz_f_name; ?>1" 
                                        <?php  
                                            $name_1 = $fz_f_name.'1';
                                            $sql = "SELECT * FROM `ms_rack` where freezer_floor_id = '".$_GET['fz_f']."' and freezer_id = '".$_GET['fz']."' AND rack_name = '".$fz_f_name."1'";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                if($row['rack_status']==0){
                                                    echo "checked";
                                                }
                                                $name_1 = $row['rack_edit'];
                                            }
                                        ?>
                                        > <?php echo $name_1; ?>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label for="checkbox2" class="form-check-label ">
                                        <input type="checkbox" class="form-check-input" id="name_r2" name="name_r2" value="<?php echo $fz_f_name; ?>2" 
                                        <?php  
                                            $name_2 = $fz_f_name.'2';
                                            $sql = "SELECT * FROM `ms_rack` where freezer_floor_id = '".$_GET['fz_f']."' and freezer_id = '".$_GET['fz']."' AND rack_name = '".$fz_f_name."2'";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                if($row['rack_status']==0){
                                                    echo "checked";
                                                }
                                                $name_2 = $row['rack_edit'];
                                            }
                                        ?>
                                        > <?php echo $name_2; ?>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label for="checkbox3" class="form-check-label ">
                                        <input type="checkbox" class="form-check-input" id="name_r3" name="name_r3" value="<?php echo $fz_f_name; ?>3" 
                                        <?php  
                                            $name_3 = $fz_f_name.'3';
                                            $sql = "SELECT * FROM `ms_rack` where freezer_floor_id = '".$_GET['fz_f']."' and freezer_id = '".$_GET['fz']."' AND rack_name = '".$fz_f_name."3'";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                if($row['rack_status']==0){
                                                    echo "checked";
                                                }
                                                $name_3 = $row['rack_edit'];
                                            }
                                        ?>
                                        > <?php echo $name_3; ?>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label for="checkbox3" class="form-check-label ">
                                        <input type="checkbox" class="form-check-input" id="name_r4" name="name_r4" value="<?php echo $fz_f_name; ?>4" 
                                        <?php  
                                            $name_4 = $fz_f_name.'4';
                                            $sql = "SELECT * FROM `ms_rack` where freezer_floor_id = '".$_GET['fz_f']."' and freezer_id = '".$_GET['fz']."' AND rack_name = '".$fz_f_name."4'";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                if($row['rack_status']==0){
                                                    echo "checked";
                                                }
                                                $name_4 = $row['rack_edit'];
                                            }
                                        ?>
                                        > <?php echo $name_4; ?>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label for="checkbox3" class="form-check-label ">
                                        <input type="checkbox" class="form-check-input" id="name_r5" name="name_r5" value="<?php echo $fz_f_name; ?>5" 
                                        <?php  
                                            $name_5 = $fz_f_name.'5';
                                            $sql = "SELECT * FROM `ms_rack` where freezer_floor_id = '".$_GET['fz_f']."' and freezer_id = '".$_GET['fz']."' AND rack_name = '".$fz_f_name."5'";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                if($row['rack_status']==0){
                                                    echo "checked";
                                                }
                                                $name_5 = $row['rack_edit'];
                                            }
                                        ?>
                                        > <?php echo $name_5; ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="save_rack" value="save_rack">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- Add modal -->
        <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Edit Name</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="rack_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                    <div class="modal-body">
                        <div class="row">            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" class="form-control" name="name_edit" id="name_edit" >
                                    <input type="hidden" class="form-control" name="id_edit" id="id_edit">
                                    <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="edit_save" value="edit_save">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- modal static -->
        <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
             data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticModalLabel">Delete</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="rack_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                    <div class="modal-body">
                        <label>Want to delete </label>
                        <label class="control-label" id="name_del"></label>
                        <input type="hidden" class="form-control" name="del_id" id="del_id">
                        <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="del_save" value="del_save">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>    
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- end modal static -->

    <?php include("_footer.php"); ?>
    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>

    <script type="text/javascript">
        function showResult(result,id){
            if(result==1){
                location.reload();
            }
            else{
                location.reload();
            }
        }

        function edit(id,name){
            document.getElementById("id_edit").value = id;
            document.getElementById("name_edit").value = name;
        }

        function del(id,name){
            document.getElementById("del_id").value = id;
            document.getElementById("name_del").innerHTML = name+"?";
        }
    </script>
</body>
</html>
<!-- end document-->