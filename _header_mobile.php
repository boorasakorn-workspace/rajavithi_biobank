<header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
        <div class="container-fluid">
            <div class="header-mobile-inner">
                <a class="logo" href="index.php">
                    <img src="images/icon/RJBioBankmo.png" alt="Biobank" style="width: 80%;"/>
                </a>
                <button class="hamburger hamburger--slider" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <nav class="navbar-mobile">
        <div class="container-fluid">
            <ul class="navbar-mobile__list list-unstyled">
                <li>
                    <a href="index.php"><i class="fas fa-desktop"></i>Dashboard</a> 
                </li>
                <li>
                    <a href="sample.php"><i class="fas fa-barcode"></i>Sample</a> 
                </li>
                <li>
                    <a href="tube.php"><img src="images/tube.png" alt="Smiley face" height="19" width="19" style="margin-right: 18px; box-sizing: border-box;">Tube</a> 
                </li>
                <li>
                    <a href="box.php"><i class="fas fa-inbox"></i>Box</a> 
                </li>
                <li>
                    <a href="search.php"><i class="fas fa-search"></i>Search</a> 
                </li>
                <li>
                    <a href="input_freezer.php"><img src="images/arrow_r.png" alt="Smiley face" height="19" width="19" style="margin-right: 20px; box-sizing: border-box;">Input</a> 
                </li>
                <li>
                    <a href="report.php"><i class="fa fa-folder-open"></i>Report</a> 
                </li>
                <!-- <li class="has-sub">
                    <a class="js-arrow" href="#"><i class="fas fa-check"></i>Check Bio</a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        <li>
                            <a href="check_dna.php">DNA</a>
                        </li>
                        <li>
                            <a href="check_rna.php">RNA</a>
                        </li>
                        <li>
                            <a href="check_serum.php">Serum/Plasma</a>
                        </li>
                        <li>
                            <a href="check_tissue.php">Tissue</a>
                        </li>
                    </ul>
                </li> -->
               <!--  <li>
                    <a href="inventory.php"><i class="fas fa-table"></i>Inventory</a> 
                </li> -->
                <!-- <li>
                    <a href="patient.php"><i class="fas fa-user"></i>Patient</a> 
                </li> --> 
                <li>
                    <a href="freezer.php"><img src="images/iconfreezer.png" alt="Smiley face" height="17" width="17" style="margin-right: 18px; box-sizing: border-box;"></i>Freezer</a> 
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#"><i class="fa fa-cog"></i>Manage</a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        <li>
                            <a href="account.php">Account</a>
                        </li>
                        <li>
                            <a href="change_password.php">Change Password</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- END HEADER MOBILE