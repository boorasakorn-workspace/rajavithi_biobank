<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>Register</title>

    <?php include("_css.php"); ?>
    <?php include("connect.php"); ?> 
    <link href="vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #example1 {
            background: url(images/33.jpg) no-repeat left top fixed; 
            background-size: cover;
        }
        h2 {
            color: #555;
            margin-top: 15px;
            text-align: center;
        }
        .btn-lg{
            margin-top: 15px;
            padding: .3rem 2rem;
        }
        .lead {
             margin-top: 15px;
        }
        #red{
            color: red;
        }
    </style>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5" id="example1">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
                        <div class="text-center">
                            <a href="#">
                                <h3>Register</h3>
                               <!-- <img src="images/icon/logo-bio.png" alt="Biobank" style="width: 90px; height: 90px;" /> -->
                               <!--  <img src="images/icon/logo.png" alt="CoolAdmin"> -->
                            </a>
                        </div>
                        <hr>
                    <form action="register_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                        <div class="login-form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label"> Staff ID <a id="red">*</a></label>
                                            <input type="text" class="form-control" name="user_idcard"> 
                                        </div>
                                    </div>   
                                </div>
                        <!-- ///////////////// -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Birthday</label>
                                            <input type="text" class="form-control" name="user_hbd" id="user_hbd" maxlength="10">
                                        </div>
                                    </div>    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Age</label>
                                            <input type="number" class="form-control" name="user_age">
                                        </div>
                                    </div>    
                                </div>    
                       <!--  //////////////// -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Prefix</label>
                                            <select class="form-control" name="user_prefix">
                                                <option value="">Please select</option>
                                                <option value="Mr.">Mr.</option>
                                                <option value="Mrs.">Mrs.</option>
                                                <option value="Miss.">Miss.</option>
                                            </select>
                                        </div>
                                    </div>  
                                    <div class="col-md-6">
                                        <div class="form-group"> 
                                            <label class="control-label">Gender</label>
                                            <div class="form-check-inline form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="radio" name="user_gender"  value="Male" checked> Male
                                                </label>
                                                <label class="form-check-label p-l-30">
                                                    <input class="form-check-input" type="radio" name="user_gender" value="Female"> Female
                                                </label>
                                            </div>
                                        </div>
                                    </div>   
                                </div>    
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Firstname <a id="red">*</a></label>
                                            <input type="text" class="form-control" name="user_fname">
                                        </div>
                                    </div>  
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Lastname <a id="red">*</a></label>
                                            <input type="text" class="form-control" name="user_lname">
                                        </div>
                                    </div>     
                                </div> 
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Username <a id="red">*</a></label>
                                            <input type="text" class="form-control" name="user_username">
                                        </div>
                                    </div>  
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Password <a id="red">*</a></label>
                                            <input type="password" class="form-control" name="user_password">
                                        </div>
                                    </div>     
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Email <a id="red">*</a></label>
                                            <input type="email" class="form-control" name="user_email">
                                        </div>
                                    </div>  
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Phone</label>
                                            <input type="text" class="form-control" name="user_phone" maxlength="10" onKeyPress="CheckNum()">
                                        </div>
                                    </div>     
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-md-4">
                                        <label class="control-label">Profile Image</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="file" class="form-control-file" id="user_image" name="user_image">
                                    </div>
                                </div>
                                <hr>
                                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="user_save" value="user_save">register</button>
                            <div class="register-link">
                                <p>Already have account?
                                    <a href="login.php"><b> Sign In </b></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
    <?php include("_js.php"); ?>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>
</body>
<script type="text/javascript">
    function showResult(result,id){
        if(result==1){
            swal({
                title: "Success",
            },
            function(){
                window.location.href = 'login.php';
            });
        }
        else if(result==0){
            swal({
                title: "Fail",
            });
        }
        else if(result==3){
            swal({
                title: "Fail",
                text: "Upload File Images",
            });
        }
        else if(result==4){
            swal({
                title: "Fail",
                text: "Duplicate Staff ID",
            });
        }
    }

    $(document).ready(function(){
        $("#user_hbd").each(function() {    
            $(this).datepicker({
                format: 'dd-mm-yyyy',
                todayBtn: false,
                language: 'eng',             
                thaiyear: false,
                autoclose:true,   
            }).datepicker('setDate', '');
        });
    });

    function CheckNum(){
        if(event.keyCode < 48 || event.keyCode > 57){
            event.returnValue = false;
        }
    }
</script>
</html>
<!-- end document-->