<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: login.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }

    include("connect.php"); 
?> 
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

	<?php include("_css.php"); ?>
	<?php include("./vendor/datatables/_css_datatable.php"); ?>
    <link href="vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        h2{
            color: #555;
            margin-top: 15px;
            text-align: center;
        }
        .btn-lg{
            margin-top: 15px;
            padding: .3rem 2rem;
        }
        .lead{
             margin-top: 15px;
        }
        .sweet-alert{
            background-color: #f2f2f2;
        }
        #red{
            color: red;
        }
    </style>
</head>
<body class="animsition">
	<div class="page-wrapper">
		<!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- END HEADER MOBILE-->

		<!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- END MENU SIDEBAR-->

    <!-- <form action="sample_model.php" method="post" enctype="multipart/form-data" target="iframe_target"> -->
		<!-- PAGE CONTAINER-->
		<div class="page-container">
			<!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- HEADER DESKTOP-->

			<!-- MAIN CONTENT-->
			<div class="main-content">
				<div class="section__content section__content--p30">
					<div class="container-fluid">
						<div class="row">
                            <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
							<div class="col-md-12">
								<div class="card">
									<div class="card-header">
										<h4>User</h4>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-md-6"><h4>User</h4></div>
											<div class="col-md-6 text-right"><button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#addmodal"> New User </button></div>
										</div>
										<br>
										<table id="example" class="table table-responsive table-striped table-bordered" style="width:100%">
											<thead>
												<tr>
												    <th width="1%" style="white-space: nowrap;">N</th>
												    <th width="1%" style="white-space: nowrap; min-width: 150px;">Name</th>  
                                                    <th width="1%" style="white-space: nowrap;">Gender</th>
                                                    <th width="1%" style="white-space: nowrap;">Birthday</th>
												    <th width="1%" style="white-space: nowrap;">Age</th>
												    <th width="1%" style="white-space: nowrap; min-width: 100px;">Email</th>
                                                    <th width="1%" style="white-space: nowrap;">Phone</th>
                                                    <th width="1%" style="white-space: nowrap; min-width: 80px;">Position</th>
												    <th width="1%" style="white-space: nowrap; text-align: left;">Manage</th>
												</tr>
											</thead>
											<tbody>
											<?php 
                                                $sql = "SELECT * FROM `user` WHERE user_id != 1 AND user_status != 2";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    echo "<tr>";
                                                    echo "<td>".$i."</td>";
                                                    echo "<td>".$row['user_prefix']."".$row['user_fname']." ".$row['user_lname']."</td>";
                                                    echo "<td>".$row['user_gender']."</td>";
                                                    echo "<td>".$row['user_hbd']."</td>";
                                                    echo "<td>".$row['user_age']."</td>";
                                                    echo "<td>".$row['user_email']."</td>";
                                                    echo "<td>".$row['user_phone']."</td>";

                                                    if($row['user_type']==1){
                                                        echo "<td>Staff</td>"; 
                                                    }
                                                    elseif($row['user_type']==2){
                                                        echo "<td>Head staff</td>"; 
                                                    }
                                                    
                                                    echo "<td><button type='button' class='btn btn-sm btn-warning' data-toggle='modal' data-target='#edit_modal' onclick='edituser(\"".$row['user_id']."\",\"".$row['user_prefix']."\",\"".$row['user_fname']."\",\"".$row['user_lname']."\",\"".$row['user_hbd']."\",\"".$row['user_age']."\",\"".$row['user_gender']."\",\"".$row['user_idcard']."\",\"".$row['user_email']."\",\"".$row['user_phone']."\",\"".$row['user_username']."\",\"".$row['user_image']."\",\"".$row['user_imagename']."\",\"".$row['user_type']."\")'> Edit </button> <button type='button' class='btn btn-sm btn-danger' data-toggle='modal' data-target='#del_modal' onclick='del(\"".$row['user_id']."\",\"".$row['user_prefix']."\",\"".$row['user_fname']."\",\"".$row['user_lname']."\")'> x </button></td>";
                                                    echo "</tr>";
                                                }
                                            ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!-- /# column -->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->
		</div>

        <!-- modal scroll -->
        <div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">New Sample</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="cancel_sam()">
                            <span aria-hidden="true">&times;</span>
                        </button>                                
                    </div>
                <form action="register_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Prefix</label>
                                    <select class="form-control" name="user_prefix">
                                        <option value="">Please select</option>
                                        <option value="Mr.">Mr.</option>
                                        <option value="Mrs.">Mrs.</option>
                                        <option value="Miss.">Miss.</option>
                                    </select>
                                </div>
                            </div>  
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">Firstname <a id="red">*</a></label>
                                    <input type="text" class="form-control" name="user_fname">
                                </div>
                            </div>  
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">Lastname <a id="red">*</a></label>
                                    <input type="text" class="form-control" name="user_lname">
                                </div>
                            </div>     
                        </div> 
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Birthday</label>
                                    <input type="text" class="form-control" id="user_hbd" name="user_hbd" maxlength="10"> 
                                </div>
                            </div>  
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Age</label>
                                    <input type="number" class="form-control" name="user_age" min="0">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Gender</label>
                                <div class="form-group">
                                    <div class="form-check-inline form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="user_gender" value="Male" checked> Male
                                        </label>
                                        <label class="form-check-label p-l-30">
                                            <input class="form-check-input" type="radio" name="user_gender" value="Female"> Female
                                        </label>
                                    </div>
                                </div>
                            </div>        
                        </div>     
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Staff ID <a id="red">*</a></label>
                                    <input type="text" class="form-control" name="user_idcard">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input type="email" class="form-control" name="user_email">
                                </div>
                            </div>  
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Phone</label>
                                    <input type="text" class="form-control" name="user_phone" maxlength="10" onKeyPress="CheckNum()">
                                </div>
                            </div>     
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Username <a id="red">*</a></label>
                                    <input type="text" class="form-control" name="user_username">
                                </div>
                            </div>  
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Password <a id="red">*</a></label>
                                    <input type="password" class="form-control" name="user_password">
                                </div>
                            </div>     
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-4"> 
                                <label class="control-label">Profile Image</label>
                                <input type="file" class="form-control-file" id="user_image" name="user_image">
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Position</label>
                                <div class="form-group">
                                    <div class="form-check-inline form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="user_type" value="1" checked> Staff
                                        </label>
                                        <label class="form-check-label p-l-30">
                                            <input class="form-check-input" type="radio" name="user_type" value="2"> Head staff
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>       
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="user_save" value="user_save">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- modal scroll -->
        <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Edit user</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="cancel_sam()">
                            <span aria-hidden="true">&times;</span>
                        </button>                                
                    </div>
                <form action="register_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                    <div class="modal-body">

                        <input type="hidden" class="form-control" name="user_id_edit" id="user_id_edit">

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Prefix</label>
                                    <select class="form-control" name="user_prefix_edit" id="user_prefix_edit">
                                        <option value="">Please select</option>
                                        <option value="Mr.">Mr.</option>
                                        <option value="Mrs.">Mrs.</option>
                                        <option value="Miss.">Miss.</option>
                                    </select>
                                </div>
                            </div>  
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">Firstname <a id="red">*</a></label>
                                    <input type="text" class="form-control" name="user_fname_edit" id="user_fname_edit">
                                </div>
                            </div>  
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">Lastname <a id="red">*</a></label>
                                    <input type="text" class="form-control" name="user_lname_edit" id="user_lname_edit">
                                </div>
                            </div>     
                        </div> 
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Birthday</label>
                                    <input type="text" class="form-control" name="user_hbd_edit" id="user_hbd_edit" maxlength="10"> 
                                </div>
                            </div>  
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Age</label>
                                    <input type="number" class="form-control" name="user_age_edit" id="user_age_edit" min="0">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Gender</label>
                                <div class="form-group">
                                    <div class="form-check-inline form-check" >
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" id="user_gender_edit1" name="user_gender_edit" value="Male"> Male
                                        </label>
                                        <label class="form-check-label p-l-30">
                                            <input class="form-check-input" type="radio" id="user_gender_edit2" name="user_gender_edit" value="Female"> Female
                                        </label>
                                    </div>
                                </div>
                            </div>        
                        </div>     
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Staff ID <a id="red">*</a></label>
                                    <input type="text" class="form-control" name="user_idcard_edit" id="user_idcard_edit">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input type="email" class="form-control" name="user_email_edit" id="user_email_edit">
                                </div>
                            </div>  
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Phone</label>
                                    <input type="text" class="form-control" name="user_phone_edit" id="user_phone_edit" maxlength="10" onKeyPress="CheckNum()">
                                </div>
                            </div>     
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Username <a id="red">*</a></label>
                                    <input type="text" class="form-control" name="user_username_edit" id="user_username_edit">
                                </div>
                            </div>  
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Password </label>
                                    <input type="password" class="form-control" readonly>
                                </div>
                            </div>     
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-4"> 
                                <label class="control-label">Profile Image</label>
                                <input type="file" class="form-control-file" name="image_edit">
                                <input type="hidden" class="form-control" id="user_image_edit" name="user_image_edit">
                                <input type="hidden" class="form-control" id="user_imagename_edit" name="user_imagename_edit">
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Position</label>
                                <div class="form-group">
                                    <div class="form-check-inline form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="user_type_edit" id="user_type_edit1" value="1"> Staff
                                        </label>
                                        <label class="form-check-label p-l-30">
                                            <input class="form-check-input" type="radio" name="user_type_edit" id="user_type_edit2" value="2"> Head staff
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>       
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="user_edit" value="user_edit">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- modal static -->
        <div class="modal fade" id="del_modal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
             data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticModalLabel">Delete</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <form action="register_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                    <div class="modal-body">
                        <label>Want to delete user </label>
                        <label class="form-control-label" id="del_ln"></label>
                        <input type="hidden" class="form-control" name="del_id" id="del_id">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="del_save" value="del_save">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>    
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- end modal static -->
  <!--   </form> -->

	<?php include("_footer.php"); ?>
	<?php include("_js.php"); ?>
	<?php include("./vendor/datatables/_js_datatable.php"); ?>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>
    <!--  <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.th.min.js"></script> -->

	<script type="text/javascript">
		$(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
            else if(result==0){
                swal({
                    title: "Error",
                });
            }
            else if(result==2){
                swal({
                    title: "Fail",
                    text: "Input LN",
                });
            }
            else if(result==4){
                swal({
                    title: "Fail",
                    text: "Duplicate LN",
                });
            }
            else if(result==5){
                swal({
                    title: "Fail",
                    text: "Duplicate Tube Barcode",
                });
            }
            else if(result==3){
                swal({
                    title: "Fail",
                    text: "Check input barcode tube",
                });
            }
        }

        function edituser(id,prefix,fname,lname,hbd,age,gender,idcard,email,phone,username,img,imgname,type) {
            document.getElementById("user_id_edit").value = id;  
            document.getElementById("user_prefix_edit").value = prefix; 
            document.getElementById("user_fname_edit").value = fname;
            document.getElementById("user_lname_edit").value = lname; 
            document.getElementById("user_hbd_edit").value = hbd; 
            document.getElementById("user_age_edit").value = age; 
            var gender1 = document.getElementById("user_gender_edit1").value;
            var gender2 = document.getElementById("user_gender_edit2").value;
            if(gender1==gender){
                document.getElementById("user_gender_edit1").checked = gender;
            }
            else if(gender2==gender){
                document.getElementById("user_gender_edit2").checked = gender;
            }
            document.getElementById("user_idcard_edit").value = idcard; 
            document.getElementById("user_email_edit").value = email; 
            document.getElementById("user_phone_edit").value = phone; 
            document.getElementById("user_username_edit").value = username; 
            document.getElementById("user_image_edit").value = img; 
            document.getElementById("user_imagename_edit").value = imgname; 

            var type1 = document.getElementById("user_type_edit1").value;
            var type2 = document.getElementById("user_type_edit2").value;
            if(type1==type){
                document.getElementById("user_type_edit1").checked = type;
            }
            else if(type2==type){
                document.getElementById("user_type_edit2").checked = type;
            }
        }

        function del(id,prefix,fname,lname) {
            document.getElementById("del_ln").innerHTML = prefix+fname+" "+lname+"?";
            document.getElementById("del_id").value = id;
        }

        $(document).ready(function(){
            $("#user_hbd").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', '');
            });
        });

        $(document).ready(function(){
            $("#user_hbd_edit").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', '');
            });
        });

        $(document).ready(function(){
            $("#sample_date").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', <?php echo date("d-m-Y"); ?>);
            });
        });

        $(document).ready(function(){
            $("#sample_hbd").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', '');
            });
        });

        $(document).ready(function(){
            $("#sample_date_edit").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', '');
            });
        });

        $(document).ready(function(){
            $("#sample_hbd_edit").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', '');
            });
        });

        $('#addmodal').on('shown.bs.modal', function () {
            $('#sample_id').focus()
        });

        function cancel_sam() {
            document.getElementById("sample_id").value = '';
            document.getElementById("sample_prefix").value = '';
            document.getElementById("sample_fname").value = '';
            document.getElementById("sample_lname").value = '';
            document.getElementById("sample_hn").value = '';
            document.getElementById("ssaample_hbd").value = '';
            document.getElementById("sample_age").value = '';
            document.getElementById("sample_gender1").checked = 'Male';
            document.getElementById("sample_type").value = '';
            document.getElementById("sample_pjowner").value = '';
            document.getElementById("sample_consent").value = '';
            document.getElementById("sample_pjname").value = '';
            document.getElementById("sample_receive1").checked = 'Yes';
        }

        var sample_age = document.getElementById('sample_age');
        sample_age.onkeydown = function(e) {
            if(!((e.keyCode > 95 && e.keyCode < 106)
              || (e.keyCode > 47 && e.keyCode < 58) 
              || e.keyCode == 8)) {
                return false;
            }
        }

        var sample_age_edit = document.getElementById('sample_age_edit');
        sample_age_edit.onkeydown = function(e) {
            if(!((e.keyCode > 95 && e.keyCode < 106)
              || (e.keyCode > 47 && e.keyCode < 58) 
              || e.keyCode == 8)) {
                return false;
            }
        }

        function check_1() {
            var checkBox = document.getElementById("tube_check1");
            var text = document.getElementById("tube_barcode1");
            if(checkBox.checked == true){
                text.readOnly = false;
            }
            else{
               text.readOnly = true;
               text.value = '';
            }
        }

        function check_2() {
            var checkBox = document.getElementById("tube_check2");
            var text = document.getElementById("tube_barcode2");
            if(checkBox.checked == true){
                text.readOnly = false;
            }
            else{
               text.readOnly = true;
               text.value = '';
            }
        }

        function check_3() {
            var checkBox = document.getElementById("tube_check3");
            var text = document.getElementById("tube_barcode3");
            if(checkBox.checked == true){
                text.readOnly = false;
            }
            else{
               text.readOnly = true;
               text.value = '';
            }
        }

        function check_4() {
            var checkBox = document.getElementById("tube_check4");
            var text = document.getElementById("tube_barcode4");
            if(checkBox.checked == true){
                text.readOnly = false;
            }
            else{
               text.readOnly = true;
               text.value = '';
            }
        }

        function check_5() {
            var checkBox = document.getElementById("tube_check5");
            var text = document.getElementById("tube_barcode5");
            if(checkBox.checked == true){
                text.readOnly = false;
            }
            else{
               text.readOnly = true;
               text.value = '';
            }
        }

        $('#checkall').change(function () {
            $('.checkcheck').prop('checked',this.checked);
        });

        $('.checkcheck').change(function () {
            if($('.checkcheck:checked').length == $('.checkcheck').length){
                $('#checkall').prop('checked',true);
            }
            else{
                $('#checkall').prop('checked',false);
            }
        });

        function CheckNum(){
            if(event.keyCode < 48 || event.keyCode > 57){
                event.returnValue = false;
            }
        }

	</script>
</body>
</html>
<!-- end document-->