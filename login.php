<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>Login</title>

    <?php include("_css.php"); ?>
    
    <style type="text/css">
        #example1 {
            background: url(images/BioBank_Background.png) no-repeat left top; 
            background-size: cover;
        }
        h2 {
            font-weight: 200;
            color: #555;
            margin-top: 32px;
            margin-bottom: 32px;
            text-align: center;
        }
        .btn-lg{
            padding: .4rem 2rem;
        }
    </style>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge4" id="example1">
           <!-- <img src="images/1.jpg" alt="CoolAdmin"><img src="images/icon/logo.png" alt="CoolAdmin"><img src="images/icon/logo.png" alt="CoolAdmin"> -->
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">
                                <img src="images/icon/logo-bio.png" alt="Biobank" style="width: 150px; height: 150px;" />
                                <!-- <img src="images/icon/logo.png" alt="CoolAdmin"> -->
                            </a>
                        </div> 
                        <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>
                <form action="login_check.php" method="post" enctype="multipart/form-data" target="iframe_target">
                        <div class="login-form">
                                <div class="form-group">
                                    <label class="control-label">Username</label>
                                    <input class="form-control" type="text" name="username">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Password</label>
                                    <input class="form-control" type="password" name="password">
                                </div>
                                <!-- <div class="login-checkbox">
                                    <label>
                                        <input type="checkbox" name="remember">Remember Me
                                    </label>
                                    <label>
                                        <a href="#">Forgotten Password?</a>
                                    </label>
                                </div> -->
                                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="login" value="login">sign in</button>
                                <!-- <div class="social-login-content">
                                    <div class="social-button">
                                        <button class="au-btn au-btn--block au-btn--blue m-b-20">sign in with facebook</button>
                                        <button class="au-btn au-btn--block au-btn--blue2">sign in with twitter</button>
                                    </div>
                                </div> -->
                           <!--  <div class="register-link">
                                <p>
                                    Don't you have account?
                                    <a href="register.php"><b> Register Here </b></a>
                                </p>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </form>
    </div>
    <?php include("_js.php"); ?>
</body>
<script type="text/javascript">
    function showResult(result,id){
        if(result==1){
            window.location.href = "index.php";
        }
        else if(result==0){
            swal({
                title: "Incorrect Username/Password",
            },
            function(){
                
            });
        }
    }
</script>
</html>
<!-- end document-->