<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: login.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }

    include("connect.php"); 
?> 
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

	<?php include("_css.php"); ?>
    <?php include("connect.php"); ?> 
	<?php include("./vendor/datatables/_css_datatable.php"); ?>
    <link href="vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="page-wrapper">
		<!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- END HEADER MOBILE-->

		<!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- END MENU SIDEBAR-->
    
   
		<!-- PAGE CONTAINER-->
		<div class="page-container">
			<!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- HEADER DESKTOP-->

			<!-- MAIN CONTENT-->
			<div class="main-content">
				<div class="section__content section__content--p30">
					<div class="container-fluid">
						<div class="row"> 

							<div class="col-md-12">
								<div class="card">
									<div class="card-header">
										<h4>Report</h4>
									</div>
									<div class="card-body">
                                        <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff;display: none;"></iframe>
                                        <form action="report_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
										<div class="row">
											<div class="col-md-6"><h4>Report</h4></div>
											<div class="col-md-6 text-right"><button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#addmodal"> New Report </button></div>
										</div>
										<br>
										<table id="example" class="table table-responsive table-striped table-bordered" style="width:100%">
											<thead>
												<tr>
												    <th width="1%" style="white-space: nowrap;">N</th>
                                                    <th width="1%" style="white-space: nowrap; text-align: left; min-width: 100px;">Manage</th> 
                                                    <th width="1%" style="white-space: nowrap; min-width: 200px;">Report File</th>
												    <th width="1%" style="white-space: nowrap;">LN</th>  
                                                    <th width="1%" style="white-space: nowrap; min-width: 100px;">IN Date</th>
                                                    <th width="1%" style="white-space: nowrap;">IN Time</th>
												    <th width="1%" style="white-space: nowrap; min-width: 100px;">Name</th>
												    <th width="1%" style="white-space: nowrap;">HN</th>
                                                    <th width="1%" style="white-space: nowrap;">Age</th>
												    <th width="1%" style="white-space: nowrap;">Gender</th>
                                                    <th width="1%" style="white-space: nowrap;">Type of speciman</th>
                                                    <th width="1%" style="white-space: nowrap;">Project owner</th>
                                                    <th width="1%" style="white-space: nowrap;">Consent form</th>
                                                    <th width="1%" style="white-space: nowrap;">Project name</th>
                                                    
												</tr>
											</thead>
											<tbody>
											<?php 
                                                $sql = "SELECT * , tr_report.sample_sid as report_ln FROM `tr_report` LEFT JOIN tr_sample on tr_sample.sample_sid = tr_report.sample_sid ORDER BY tr_report.report_id DESC";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    echo "<tr>
                                                        <td>".$i."</td>
                                                        <td>
                                                            <a href='report_model.php?file=".$row['report_file']."&&filename=".$row['report_filename']."&&dow=report'>
                                                                <button type='button' class='btn btn-primary btn-sm' name='dowload_file' value='report'>Download</button>
                                                            </a>
                                                        </td>
                                                        <td>".$row['report_filename']."</td>
                                                        <td>".$row['report_ln']."</td>
                                                        <td>".$row['sample_date']."</td>
                                                        <td>".$row['sample_time']."</td>
                                                        <td>".$row['sample_prefix']."".$row['sample_fname']." ".$row['sample_lname']."</td>
                                                        <td>".$row['sample_hn']."</td>
                                                        <td>".$row['sample_age']."</td>
                                                        <td>".$row['sample_gender']."</td>
                                                        <td>".$row['sample_type']."</td>
                                                        <td>".$row['sample_pjowner']."</td>
                                                        <td>".$row['sample_consent']."</td>
                                                        <td>".$row['sample_pjname']."</td>";
                                                    echo " </tr>"; 
                                                }
                                            ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!-- /# column -->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->
		</div>

        <!-- modal scroll -->
        <div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Add Report</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>                                
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">LN</label>
                                    <input type="text" class="form-control" name="report_ln" value="">
                                    <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                </div>
                            </div>               
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Upload file</label>
                                    <input type="file" class="form-control-file" id="report_file" name="report_file">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="save_report" value="save_report">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

		<!-- modal scroll -->
        <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Patient Edit</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>                                
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">LN</label>
                                    <input type="text" class="form-control" name="sample_id" value="">
                                </div>
                            </div>               
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Upload file</label>
                                    <input type="file" class="form-control-file" id="file-input" name="file-input" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="edit_sample" value="edit_sample">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->
    </form>

	<?php include("_footer.php"); ?>
	<?php include("_js.php"); ?>
	<?php include("./vendor/datatables/_js_datatable.php"); ?>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>
    <!--  <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.th.min.js"></script> -->

	<script type="text/javascript">
		$(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        }

        function editpatient(id,sam_id,sam_date,sam_time,prefix,fname,lname,hn,age,gender,type,pjowner,consent,pjname) {
            document.getElementById("sample_id_edit").value = id;
            document.getElementById("sample_sid_edit").value = sam_id;
            document.getElementById("sample_date_edit").value = sam_date;
            document.getElementById("sample_time_edit").value = sam_time;
            document.getElementById("sample_prefix_edit").value = prefix;
            document.getElementById("sample_fname_edit").value = fname;
            document.getElementById("sample_lname_edit").value = lname;  
            document.getElementById("sample_hn_edit").value = hn;
            document.getElementById("sample_age_edit").value = age;
            document.getElementById("sample_gender_edit").value = gender;
            document.getElementById("sample_type_edit").value = type;
            document.getElementById("sample_pjowner_edit").value = pjowner;
            document.getElementById("sample_consent_edit").value = consent;
            document.getElementById("sample_pjname_edit").value = pjname;
        }

        function dow(file,filename,dowload) {
            document.getElementById("dowload_report_file").value = file;
            document.getElementById("dowload_report_filename").value = filename;
            document.getElementById("dowload_file").value = dowload;
        }

        $(document).ready(function(){
            $("#sample_date").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', <?php echo date("d-m-Y"); ?>);
            });
        });

        $(document).ready(function(){
            $("#sample_date_edit").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', '');
            });
        });

	</script>
</body>
</html>
<!-- end document-->