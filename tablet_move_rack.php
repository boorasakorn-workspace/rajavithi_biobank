<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: tablet_404.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }
    include("connect.php"); 
?> 
<?php 
    $sqlfz = "SELECT * FROM `ms_freezer_floor` LEFT JOIN ms_freezer ON ms_freezer.freezer_id = ms_freezer_floor.freezer_id where ms_freezer_floor.freezer_id = '".$_GET['fzm']."' AND ms_freezer_floor.freezer_floor_id = '".$_GET['fz_f']."'";
    $objQueryfz = $db_connection->query($sqlfz);
    while(($row = $objQueryfz->fetch_assoc()) != null){
        $fz_id = $row['freezer_id'];
        $fz_name = $row['freezer_name'];
        $fz_f_id = $row['freezer_floor_id'];
        $fz_f_name = $row['freezer_floor_name'];
        $fz_f_edit = $row['freezer_floor_edit'];
    }
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

    <?php include("_css.php"); ?>
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
    <link href="vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        #iconimg{
            width: 15%;
        }

        @media (max-width: 991px) {
            #iconimg{
                width: 35%;
            }

            div.dataTables_wrapper div.dataTables_filter input {
                margin-left: 0.5em;
                display: inline-block;
                width: 65%;
            }
        }
    </style>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <div class="container">
            <div class="login-content">
                <div class="row">
                    <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Move <?php if($_GET['s']=='t'){ echo "Tube"; }elseif($_GET['s']=='b'){ echo "Box"; } ?> | Rack</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <img src="images/icon/logo-bio.png" alt="CoolAdmin" id="iconimg">  
                                        <h3 class="m-t-10"><?php echo @$fz_name." <i class='fa fa-chevron-right'></i> ".@$fz_f_edit; ?></h3>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <button type="button" class="btn btn-secondary mb-1" onclick="window.history.back();"> Back </button>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                <?php 
                                    $sql = "SELECT * FROM `ms_rack` where freezer_floor_id = '".$_GET['fz_f']."' and freezer_id = '".$_GET['fzm']."' AND rack_status != 1";
                                    $objQuery = $db_connection->query($sql);
                                    $num = $objQuery->num_rows;
                                    $i=0;
                                    if($num==0){
                                ?>
                                    </div>
                                        <table id="example" class="table table-responsive table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th width="1%" style="white-space: nowrap;">N</th>
                                                    <th width="1%" style="white-space: nowrap;">Box Barcode</th>
                                                    <th width="1%" style="white-space: nowrap;">Box Name</th>
                                                    <th width="1%" style="white-space: nowrap;">Manage</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    $sql = "SELECT * FROM `tr_freezer_add` LEFT JOIN `ms_box` on ms_box.box_barcode = tr_freezer_add.box_barcode  WHERE `freezer_id` = '".$fz_id."' AND `freezer_floor_id` = '".$fz_f_id."'";
                                                    $objQuery = $db_connection->query($sql);
                                                    $i=0;
                                                    while(($row = $objQuery->fetch_assoc()) != null){
                                                        $i++;
                                                        echo "<tr>
                                                            <td>".$i."</td>
                                                            <td>".$row['box_barcode']."</td>
                                                            <td>".$row['box_name']."</td>
                                                            <td><a href='tablet_move_box_add.php?id=".$_GET['id']."&s=".$_GET['s']."&fz=".$_GET['fz']."&fzm=".$_GET['fzm']."&box_bar=".$row['box_barcode']."'>
                                                            <button type='button' class='btn btn-sm btn-success'> Input Tube </button></a></td>
                                                        </tr>";
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                <?php
                                    }
                                    else{
                                        while(($row = $objQuery->fetch_assoc()) != null){
                                            $sqlrack = "SELECT * FROM `ms_rack_floor` LEFT JOIN ms_rack ON ms_rack.rack_id = ms_rack_floor.rack_id WHERE ms_rack_floor.rack_id=".$row['rack_id']." AND rack_floor_status !=1";
                                            $objQueryrack = $db_connection->query($sqlrack);
                                            $max_rack = $objQueryrack->num_rows;
                                            $max_rack = $max_rack*4;
                                            $sqlbox = "SELECT * FROM `tr_freezer_add` LEFT JOIN ms_rack_floor ON ms_rack_floor.rack_floor_id = tr_freezer_add.rack_floor_id LEFT JOIN ms_rack ON ms_rack.rack_id = ms_rack_floor.rack_id where freezeradd_status=0 AND ms_rack.rack_id = ".$row['rack_id']."";
                                            $objQuerybox = $db_connection->query($sqlbox);
                                            $sum_num = $objQuerybox->num_rows;
                                            $i++;
                                            echo "<div class='col-md-3 text-center'>
                                                <a href='tablet_move_rack_floor.php?id=".$_GET['id']."&s=".$_GET['s']."&fz=".$_GET['fz']."&fzm=".$_GET['fzm']."&fz_f=".$fz_f_id."&r=".$row['rack_id']."'>
                                                    <img src='images/rack_n.png' alt='Biobank'/>
                                                </a>
                                                <h3 class='text-center p-t-10 p-b-20'>".$row['rack_edit']." ";
                                                    if($sum_num<$max_rack){
                                                        echo "<span class='badge badge-success' style='color:#28a745;' >0</span>";
                                                    } 
                                                    else{
                                                        echo "<span class='badge badge-danger' style='color:#dc3545;' >0</span>";
                                                    }
                                                echo "</h3>
                                                </div>";
                                        }
                                        echo "</div>";
                                    }

                                ?> 
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>      
                <div class="row">
                    <div class="col-md-12 text-center">
                        <input type="button" class="btn btn-success m-t-10" value="Menu" onclick="window.location.href='tablet_menu.php?fz=<?php echo $_GET['fz']; ?>'" />
                        <input type="button" class="btn btn-info m-t-10 m-l-10" value="Logout" onclick="window.location.href='tablet_logout.php?fz=<?php echo $_GET['fz']; ?>'" />
                    </div>
                </div>
            </div>
        </div>
    </div>

     <!-- modal medium -->
        <div class="modal fade" id="inputboxmodel" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="margin-top: 20%;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Input Box</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="tablet_add_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="input_barcode_box" name="input_barcode_box" placeholder="Barcode">
                                        <input type="hidden" class="form-control" id="freezer_id" name="freezer_id" value="<?php echo $fz_id; ?>">
                                        <input type="hidden" class="form-control" id="freezer_floor_id" name="freezer_floor_id" value="<?php echo $fz_f_id; ?>">
                                        <input type="hidden" class="form-control" id="rack_id" name="rack_id" value="0">
                                        <input type="hidden" class="form-control" id="rack_floor_id" name="rack_floor_id" value="0">
                                        <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer"> 
                            <button type="submit" class="btn btn-primary" name="save_inputbox" value="save_inputbox">Confirm</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end modal medium -->

    <!-- modal medium -->
        <div class="modal fade" id="inputtubemodel" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="margin-top: 20%;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Input Tube</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="tablet_box_add.php?fz=<?php echo $_GET['fz']; ?>" method="post">
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="input_barcode_tube" name="input_barcode_tube" placeholder="Barcode">
                                        <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" name="q" value="q">Confirm</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <!-- end modal medium -->

    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        }

        $('#inputboxmodel').on('shown.bs.modal', function () {
            document.getElementById("input_barcode_box").value = "";
            $('#input_barcode_box').focus();
        });

        $('#inputtubemodel').on('shown.bs.modal', function () {
            document.getElementById("input_barcode_tube").value = "";
            $('#input_barcode_tube').focus();
        });

    </script>
    </script>
</body>
</html>
<!-- end document-->