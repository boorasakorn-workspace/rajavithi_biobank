<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: login.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }
    include("connect.php"); 
?> 
<?php 
    $sqlfz = "SELECT * FROM `ms_freezer` where freezer_id = ".$_GET['fz']."";
    $objQueryfz = $db_connection->query($sqlfz);
    while(($row = $objQueryfz->fetch_assoc()) != null){
        $fz_id = $row['freezer_id'];
        $fz_name = $row['freezer_name'];
    }
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

    <?php include("_css.php"); ?>
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- PAGE CONTAINER-->
        <div class="page-container">
        <!-- HEADER DESKTOP-->
        <?php  include("_header_desktop.php"); ?>
        <!-- HEADER DESKTOP-->
        <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Freezer floor</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6"><h3><?php echo @$fz_name; ?></h3></div>
                                            <div class="col-md-6 text-right p-t-10">
                                               <!--  <button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#add_modal"> Add Floor </button> -->
                                                <button type="button" class="btn btn-secondary mb-1" onclick="window.history.back();"> Back </button>
                                            </div>
                                        </div>   
                                        <br>
                                        <div class="row form-group">
                                        <?php 
                                            $sql = "SELECT * FROM `ms_freezer_floor` where freezer_id = ".$_GET['fz']." AND freezer_floor_status != 1";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                $sqlrack = "SELECT * FROM `ms_rack_floor` LEFT JOIN ms_rack ON ms_rack.rack_id = ms_rack_floor.rack_id LEFT JOIN ms_freezer_floor ON ms_freezer_floor.freezer_floor_id = ms_rack.freezer_floor_id WHERE ms_rack_floor.freezer_floor_id=".$row['freezer_floor_id']." AND ms_rack.rack_status != 1 ";
                                                $objQueryrack = $db_connection->query($sqlrack);
                                                $max_rack = $objQueryrack->num_rows;
                                                if($max_rack==0){
                                                    $max_rack = 10;
                                                }
                                                else{
                                                    $max_rack = $max_rack*4;
                                                }
                                                $sqlbox = "SELECT * FROM `tr_freezer_add` LEFT JOIN ms_rack_floor ON ms_rack_floor.rack_floor_id = tr_freezer_add.rack_floor_id LEFT JOIN ms_rack ON ms_rack.rack_id = ms_rack_floor.rack_id where freezeradd_status=0 AND ms_rack_floor.freezer_floor_id=".$row['freezer_floor_id']."";
                                                $objQuerybox = $db_connection->query($sqlbox);
                                                $sum_num = $objQuerybox->num_rows;
                                                $i++;
                                                echo "<div class='col-md-12 text-center p-b-20'>
                                                    <a class='p-r-20' href='input_rack.php?fz=".$fz_id."&fz_f=".$row['freezer_floor_id']."'>
                                                        <img src='images/freezer_floor_n.png' alt='Biobank'/>
                                                    </a>
                                                    <label style='color:black;font-size:22px;'>
                                                     ";
                                                    if($sum_num<$max_rack){
                                                        echo " <span class='badge badge-success' style='color:#28a745;' >0</span>";
                                                    }
                                                    else{
                                                        echo " <span class='badge badge-danger' style='color:#dc3545;' >0</span>";
                                                    }
                                                echo " <b> ".$row['freezer_floor_edit']." </b></label>
                                                </div>";
                                            }
                                        ?>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTAINER-->
        </div>

    <?php include("_footer.php"); ?>
    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>

    <script type="text/javascript">
        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        } 

        function edit(id,name){
            document.getElementById("id_edit").value = id;
            document.getElementById("name_edit").value = name;
        }

        function del(id,name){
            document.getElementById("del_id").value = id;
            document.getElementById("name_del").innerHTML = name+"?";
        }
    </script>
</body>
</html>
<!-- end document-->