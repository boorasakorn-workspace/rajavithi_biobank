<iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags-->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- <meta name="description" content="au theme template">
	<meta name="author" content="Hau Nguyen">
	<meta name="keywords" content="au theme template"> -->

	<!-- Title Page-->
	<title>Biobank</title>

	<?php include("_css.php"); ?>
    <?php include("connect.php"); ?> 
	<?php include("./vendor/datatables/_css_datatable.php"); ?>
    <link href="vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
</head>
<body class="animsition">
	<div class="page-wrapper">
		<!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- END HEADER MOBILE-->

		<!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- END MENU SIDEBAR-->

    <form action="patient_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
		<!-- PAGE CONTAINER-->
		<div class="page-container">
			<!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- HEADER DESKTOP-->

			<!-- MAIN CONTENT-->
			<div class="main-content">
				<div class="section__content section__content--p30">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-header">
										<h4>Patient</h4>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-md-6"><h4>Patient</h4></div>
											<div class="col-md-6 text-right"><button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#addmodal"> Add </button></div>
										</div>
										<br>
										<table id="example" class="table table-responsive table-striped table-bordered" style="width:100%">
											<thead>
												<tr>
												    <th width="1%" style="white-space: nowrap;">Order</th>
												    <th width="1%" style="white-space: nowrap;">HN Number</th>  
                                                    <th width="1%" style="white-space: nowrap;">Date of collection</th>
                                                    <th width="1%" style="white-space: nowrap;">Time of collection</th>
												    <th width="1%" style="white-space: nowrap;">Name</th>
												    <th width="1%" style="white-space: nowrap;">Age</th>
												    <th width="1%" style="white-space: nowrap;">Gender</th>
                                                    <th width="1%" style="white-space: nowrap;">Type of speciman</th>
                                                    <th width="1%" style="white-space: nowrap;">Project owner</th>
                                                    <th width="1%" style="white-space: nowrap;">Consent form</th>
                                                    <th width="1%" style="white-space: nowrap;">Project name</th>
                                                    <th width="1%" style="white-space: nowrap;">Date delete</th>
                                                    <th width="1%" style="white-space: nowrap;">Cabinet</th>
                                                    <th width="1%" style="white-space: nowrap;">Shelver</th>
                                                    <th width="1%" style="white-space: nowrap;">Rack</th>
												    <th width="1%" style="white-space: nowrap; text-align: left;">Manage</th>
												</tr>
											</thead>
											<tbody>
											<?php 
                                                $sql = "SELECT * FROM `tr_patient`";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    echo "<tr>
                                                        <td>".$i."</td>
                                                        <td>".$row['patient_hnnum']."</td>
                                                        <td>".$row['patient_hndate']."</td>
                                                        <td>".$row['patient_hntime']."</td>
                                                        <td>".$row['patient_prefix']."".$row['patient_fname']." ".$row['patient_lname']."</td>
                                                        <td>".$row['patient_age']."</td>
                                                        <td>".$row['patient_gender']."</td>
                                                        <td>".$row['patient_type']."</td>
                                                        <td>".$row['patient_pjowner']."</td>
                                                        <td>".$row['patient_consent']."</td>
                                                        <td>".$row['patient_pjname']."</td>
                                                        <td>".$row['patient_datedel']."</td> 
                                                        <td>".$row['patient_cabinet']."</td>
                                                        <td>".$row['patient_shelves']."</td>
                                                        <td>".$row['patient_rack']."</td>
                                                        <td>
                                                            <button type='button' data-toggle='modal' data-target='#edit_modal' data-toggle='tooltip' title='Edit' onclick='editpatient(\"".$row['patient_id']."\",\"".$row['patient_hnnum']."\",\"".$row['patient_hndate']."\",\"".$row['patient_hntime']."\",\"".$row['patient_prefix']."\",\"".$row['patient_age']."\",\"".$row['patient_gender']."\",\"".$row['patient_fname']."\",\"".$row['patient_lname']."\",\"".$row['patient_type']."\",\"".$row['patient_pjowner']."\",\"".$row['patient_consent']."\",\"".$row['patient_pjname']."\",\"".$row['patient_datedel']."\",\"".$row['patient_cabinet']."\",\"".$row['patient_shelves']."\",\"".$row['patient_rack']."\")' style='color:#666;'>
                                                                <i class='zmdi zmdi-edit'></i>
                                                            </button>
                                                        </td>
                                                    </tr>";
                                                }
                                                /*<button class='item p-l-10' data-toggle='modal' data-target='#staticModal' data-toggle='tooltip' title='Delete'  style='color:#666;'>
                                                                <i class='zmdi zmdi-delete'></i>
                                                            </button>*/
                                            ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!-- /# column -->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->
		</div>

        <!-- modal scroll -->
        <div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Patient Add</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>                                
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">HN Number</label>
                                    <input type="text" class="form-control" name="hn_num" value="">
                                </div>
                            </div>               
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Date of Collection</label>
                                    <input type="text" class="form-control" id="hn_date" name="hn_date" value="<?php echo date("d-m-Y"); ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Time of Collection</label>
                                    <input type="text" class="form-control" name="hn_time" value="<?php echo date("H:s"); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Prefix</label>
                                    <select class="form-control" name="prefix" id="prefix">
                                        <option value="0">Please select</option>
                                        <option value="1">นาย</option>
                                        <option value="2">นาง</option>
                                        <option value="2">นางสาว</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group"> 
                                    <label class="control-label">Age</label>
                                    <input type="text" class="form-control" name="age" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group"> 
                                    <label class="control-label">Gender</label>
                                    <select class="form-control" name="gender" id="gender">
                                        <option value="0">Please select</option>
                                        <option value="1">ชาย</option>
                                        <option value="2">หญิง</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group"> 
                                    <label class="control-label">Fristname</label>
                                    <input type="text" class="form-control" name="fname" value="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group"> 
                                    <label class="control-label">Lastname</label>
                                    <input type="text" class="form-control" name="lname" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Type of specimen</label>
                                    <select class="form-control" name="type_specimen" id="type_specimen">
                                        <option value="0">Please select</option>
                                        <?php 
                                           /* $sql = "SELECT * FROM `ms_bio`";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                $i++;
                                                echo "<option value='".$row['bio_barcode']."'>".$row['bio_barcode']." ".$row['bio_name']."</option>";
                                            }*/
                                        ?>
                                    </select>
                                </div>
                            </div>               
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Project owner</label>
                                    <input type="text" class="form-control" name="pj_owner" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Consent form</label>
                                    <input type="text" class="form-control" name="pj_consent" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label">Project name</label>
                                    <input type="text" class="form-control" name="pj_name" value="">
                                </div>
                            </div>               
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Date delect</label>
                                    <input type="text" class="form-control" id="date_del" name="date_del" value="">
                                </div>
                            </div>
                        </div>
                        <label for="x_card_code" class="control-label mb-1">Room</label>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Cabinet</label>
                                    <select class="form-control" name="cabinet" id="cabinet">
                                        <option value="0">Please select</option>
                                        <?php 
                                            /*$sql = "SELECT * FROM `ms_cabinet`";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                $i++;
                                                echo "<option value='".$row['cabinet_name']."'>".$row['cabinet_name']."</option>";
                                            }*/
                                        ?>
                                    </select>
                                </div>
                            </div>               
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Shelves</label>
                                    <select class="form-control" name="shelves" id="shelves">
                                        <option value="0">Please select</option>
                                        <?php 
                                            /*$sql = "SELECT * FROM `ms_shelves`";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                $i++;
                                                echo "<option value='".$row['shelves_name']."'>".$row['shelves_name']."</option>";
                                            }*/
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Rack</label>
                                    <select class="form-control" name="rack" id="rack">
                                        <option value="0">Please select</option>
                                        <?php 
                                            /*$sql = "SELECT * FROM `ms_rack`";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                $i++;
                                                echo "<option value='".$row['rack_barcode']."'>".$row['rack_barcode']." ".$row['rack_name']."</option>";
                                            }*/
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" name="save_patient" value="save_patient">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

		<!-- modal scroll -->
        <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Patient Edit</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>                                
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <input type="hidden" class="form-control" name="id_edit" id="id_edit" value="">
                                <div class="form-group">
                                    <label class="control-label">HN Number</label>
                                    <input type="text" class="form-control" name="hn_num_edit" id="hn_num_edit" value="">
                                </div>
                            </div>               
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Date of Collection</label>
                                    <input type="text" class="form-control" name="hn_date_edit" id="hn_date_edit" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Time of Collection</label>
                                    <input type="text" class="form-control" name="hn_time_edit" id="hn_time_edit" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Prefix</label>
                                    <select class="form-control" name="prefix_edit" id="prefix_edit">
                                        <option value="0">Please select</option>
                                        <option value="1">นาย</option>
                                        <option value="2">นาง</option>
                                        <option value="3">นางสาว</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group"> 
                                    <label class="control-label">Age</label>
                                    <input type="text" class="form-control" name="age_edit" id="age_edit" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group"> 
                                    <label class="control-label">Gender</label>
                                    <select class="form-control" name="gender_edit" id="gender_edit">
                                        <option value="0">Please select</option>
                                        <option value="1">ชาย</option>
                                        <option value="2">หญิง</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group"> 
                                    <label class="control-label">Fristname</label>
                                    <input type="text" class="form-control" name="fname_edit" id="fname_edit" value="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group"> 
                                    <label class="control-label">Lastname</label>
                                    <input type="text" class="form-control" name="lname_edit" id="lname_edit" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Type of specimen</label>
                                    <select class="form-control" name="type_edit" id="type_edit">
                                        <option value="0">Please select</option>
                                        <?php 
                                            /*$sql = "SELECT * FROM `ms_bio`";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                $i++;
                                                echo "<option value='".$row['bio_barcode']."'>".$row['bio_barcode']." ".$row['bio_name']."</option>";
                                            }*/
                                        ?>
                                    </select>
                                </div>
                            </div>               
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Project owner</label>
                                    <input type="text" class="form-control" name="pj_owner_edit" id="pj_owner_edit" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Consent form</label>
                                    <input type="text" class="form-control" name="pj_consent_edit" id="pj_consent_edit" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label">Project name</label>
                                    <input type="text" class="form-control" name="pj_name_edit" id="pj_name_edit" value="">
                                </div>
                            </div>               
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Date delect</label>
                                    <input type="text" class="form-control" name="date_del_edit" id="date_del_edit" value="">
                                </div>
                            </div>
                        </div>
                        <label for="x_card_code" class="control-label mb-1">Room</label>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Cabinet</label>
                                    <select class="form-control" name="cabinet_edit" id="cabinet_edit">
                                        <option value="0">Please select</option>
                                        <?php 
                                            /*$sql = "SELECT * FROM `ms_cabinet`";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                $i++;
                                                echo "<option value='".$row['cabinet_name']."'>".$row['cabinet_name']."</option>";
                                            }*/
                                        ?>
                                    </select>
                                </div>
                            </div>               
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Shelves</label>
                                    <select class="form-control" name="shelves_edit" id="shelves_edit">
                                        <option value="0">Please select</option>
                                        <?php 
                                            /*$sql = "SELECT * FROM `ms_shelves`";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                $i++;
                                                echo "<option value='".$row['shelves_name']."'>".$row['shelves_name']."</option>";
                                            }*/
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Rack</label>
                                    <select class="form-control" name="rack_edit" id="rack_edit">
                                        <option value="0">Please select</option>
                                        <?php 
                                            /*$sql = "SELECT * FROM `ms_rack`";
                                            $objQuery = $db_connection->query($sql);
                                            $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                $i++;
                                                echo "<option value='".$row['rack_barcode']."'>".$row['rack_barcode']." ".$row['rack_name']."</option>";
                                            }*/
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" name="edit_patient" value="edit_patient">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- modal static -->
		<!-- <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
			 data-backdrop="static">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="staticModalLabel">Static Modal</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="" method="post">
                            <div class="row">
                            	<div class="col-md-12">
									<p>Delect ???</p>	
								</div>
                            </div>
                        </form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary">Confirm</button>
					</div>
				</div>
			</div>
		</div> -->
		<!-- end modal static -->
    </form>

	<?php include("_footer.php"); ?>
	<?php include("_js.php"); ?>
	<?php include("./vendor/datatables/_js_datatable.php"); ?>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>
    <!--  <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.th.min.js"></script> -->

	<script type="text/javascript">
		$(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        }

        function editpatient(id,hnnum,hndate,hntime,prefix,age,gender,fname,lname,type,pjowner,consent,pjname,datedel,cabinet,shelves,rack) {
            document.getElementById("id_edit").value = id;
            document.getElementById("hn_num_edit").value = hnnum;
            document.getElementById("hn_date_edit").value = hndate;
            document.getElementById("hn_time_edit").value = hntime;
            document.getElementById("prefix_edit").selectedIndex = prefix;
            document.getElementById("age_edit").value = age;
            document.getElementById("gender_edit").selectedIndex = gender;
            document.getElementById("fname_edit").value = fname;
            document.getElementById("lname_edit").value = lname;
            document.getElementById("type_edit").value = type;
            document.getElementById("pj_owner_edit").value = pjowner;
            document.getElementById("pj_consent_edit").value = consent;
            document.getElementById("pj_name_edit").value = pjname;
            document.getElementById("date_del_edit").value = datedel;
            document.getElementById("cabinet_edit").value = cabinet;
            document.getElementById("shelves_edit").value = shelves;
            document.getElementById("rack_edit").value = rack;
        }

        $(document).ready(function(){
            $("#hn_date").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', <?php echo date("d-m-Y"); ?>);
            });
        });

        $(document).ready(function(){
            $("#date_del").each(function() {    
                $(this).datepicker({
                    format: 'dd/mm/yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', '');
            });
        });

        $(document).ready(function(){
            $("#hn_date_edit").each(function() {    
                $(this).datepicker({
                    format: 'dd/mm/yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', '');
            });
        });

        $(document).ready(function(){
            $("#date_del_edit").each(function() {    
                $(this).datepicker({
                    format: 'dd/mm/yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', '');
            });
        });
	</script>
</body>
</html>
<!-- end document-->