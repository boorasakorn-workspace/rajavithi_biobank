<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <!--  <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template"> -->

    <!-- Title Page-->
    <title>Biobank</title>

    <?php include("_css.php"); ?>

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">Check Serum/Plasma</div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">Check Serum/Plasma</h3>
                                        </div>
                                        <hr>
                                        <form action="" method="post" novalidate="novalidate">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#inputmodel" style="width: 100%; font-size: 70px;"> < Input </button>
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="button" class="btn btn-danger mb-1" data-toggle="modal" data-target="#outputmodel" style="width: 100%; font-size: 70px;"> Output > </button>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive table--no-card m-b-30">
                                                        <table class="table table-borderless table-striped table-earning">
                                                            <thead>
                                                                <tr>
                                                                    <th>Barcode</th>
                                                                    <th>Name</th>
                                                                    <th class="text-right">Num</th>
                                                                    <th class="text-right">Rack</th>
                                                                    <th class="text-right">Floor</th>
                                                                    <th class="text-right">Cabinet</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>100000</td>
                                                                    <td>XXXXXXXXXXXXX</td>
                                                                    <td class="text-right">10</td>
                                                                    <td class="text-right">14853</td>                                        
                                                                    <td class="text-right">ชั้น 3</td>
                                                                    <td class="text-right">ตู้ 1</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>100001</td>
                                                                    <td>XXXXXXXXXXXXX</td>
                                                                    <td class="text-right">20</td>
                                                                    <td class="text-right">56731</td>
                                                                    <td class="text-right">ชั้น 3</td>
                                                                    <td class="text-right">ตู้ 2</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>100396</td>
                                                                    <td>XXXXXXXXXXXXX</td>
                                                                    <td class="text-right">22</td>
                                                                    <td class="text-right">05634</td>
                                                                    <td class="text-right">ชั้น 3</td>
                                                                    <td class="text-right">ตู้ 2</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>100395</td>
                                                                    <td>XXXXXXXXXXXXX</td>
                                                                    <td class="text-right">15</td>
                                                                    <td class="text-right">97456</td>
                                                                    <td class="text-right">ชั้น 2</td>
                                                                    <td class="text-right">ตู้ 1</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>100393</td>
                                                                    <td>XXXXXXXXXXXXX</td>
                                                                    <td class="text-right">37</td>
                                                                    <td class="text-right">56754</td>
                                                                    <td class="text-right">ชั้น 1</td>
                                                                    <td class="text-right">ตู้ 1</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>100398</td>
                                                                    <td>XXXXXXXXXXXXX</td>
                                                                    <td class="text-right">18</td>
                                                                    <td class="text-right">21347</td>
                                                                    <td class="text-right">ชั้น 1</td>
                                                                    <td class="text-right">ตู้ 1</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>100397</td>
                                                                    <td>XXXXXXXXXXXXX</td>
                                                                    <td class="text-right">61</td>
                                                                    <td class="text-right">12321</td>
                                                                    <td class="text-right">ชั้น 1</td>
                                                                    <td class="text-right">ตู้ 2</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>100396</td>
                                                                    <td>XXXXXXXXXXXXX</td>
                                                                    <td class="text-right">27</td>
                                                                    <td class="text-right">11232</td>
                                                                    <td class="text-right">ชั้น 3</td>
                                                                    <td class="text-right">ตู้ 1</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>100395</td>
                                                                    <td>XXXXXXXXXXXXX</td>
                                                                    <td class="text-right">1</td>
                                                                    <td class="text-right">43321</td>
                                                                    <td class="text-right">ชั้น 2</td>
                                                                    <td class="text-right">ตู้ 2</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>100393</td>
                                                                    <td>XXXXXXXXXXXXX</td>
                                                                    <td class="text-right">3</td>
                                                                    <td class="text-right">5123</td>
                                                                    <td class="text-right">ชั้น 1</td>
                                                                    <td class="text-right">ตู้ 2</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal medium -->
        <div class="modal fade" id="inputmodel" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="margin-top: 20%;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Input</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="input-group">
                                        <input type="email" id="input_serum" name="input2-group2" placeholder="Barcode" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal medium -->

        <!-- modal medium -->
        <div class="modal fade" id="outputmodel" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="margin-top: 20%;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Output</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="input-group">
                                        <input type="email" id="output_serum" name="input2-group2" placeholder="Barcode" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal medium -->
        <?php include("_footer.php"); ?>
    </div>

    <?php include("_js.php"); ?>

    <script type="text/javascript">
        $('#inputmodel').on('shown.bs.modal', function () {
            $('#input_serum').focus()
        });

        $('#outputmodel').on('shown.bs.modal', function () {
            $('#output_serum').focus()
        });
    </script>
</body>

</html>
<!-- end document-->
