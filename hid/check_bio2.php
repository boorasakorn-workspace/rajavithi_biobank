<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Forms</title>

    <?php include("_css.php"); ?>

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="images/icon/logo.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="has-sub">
                            <a class="js-arrow" href="#"><i class="fas fa-tachometer-alt"></i>Dashboard</a>    
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="index.html">Dashboard 1</a>
                                </li>
                                <li>
                                    <a href="index2.html">Dashboard 2</a>
                                </li>
                                <li>
                                    <a href="index3.html">Dashboard 3</a>
                                </li>
                                <li>
                                    <a href="index4.html">Dashboard 4</a>
                                </li>
                            </ul>
                        </li>
                        <li class="active">
                            <a href="check_bio2.php"><i class="fas fa-chart-bar"></i>Check Bio</a> 
                        </li>
                        <li>
                            <a href="inventory.php"><i class="fas fa-chart-bar"></i>Inventory</a> 
                        </li>
                        <li>
                            <a href="patient.php"><i class="fas fa-chart-bar"></i>Patient</a> 
                        </li>

                        <li>
                            <a href="chart.html"><i class="fas fa-chart-bar"></i>Charts</a> 
                        </li>
                        <li>
                            <a href="table.html"><i class="fas fa-table"></i>Tables</a>       
                        </li>
                        <li>
                            <a href="form.html"><i class="far fa-check-square"></i>Forms</a>
                        </li>
                        <li>
                            <a href="#"><i class="fas fa-calendar-alt"></i>Calendar</a>
                        </li>
                        <li>
                            <a href="map.html"><i class="fas fa-map-marker-alt"></i>Maps</a>  
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#"><i class="fas fa-copy"></i>Pages</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="login.html">Login</a>
                                </li>
                                <li>
                                    <a href="register.html">Register</a>
                                </li>
                                <li>
                                    <a href="forget-pass.html">Forget Password</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#"><i class="fas fa-desktop"></i>UI Elements</a>   
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="button.html">Button</a>
                                </li>
                                <li>
                                    <a href="badge.html">Badges</a>
                                </li>
                                <li>
                                    <a href="tab.html">Tabs</a>
                                </li>
                                <li>
                                    <a href="card.html">Cards</a>
                                </li>
                                <li>
                                    <a href="alert.html">Alerts</a>
                                </li>
                                <li>
                                    <a href="progress-bar.html">Progress Bars</a>
                                </li>
                                <li>
                                    <a href="modal.html">Modals</a>
                                </li>
                                <li>
                                    <a href="switch.html">Switchs</a>
                                </li>
                                <li>
                                    <a href="grid.html">Grids</a>
                                </li>
                                <li>
                                    <a href="fontawesome.html">Fontawesome Icon</a>
                                </li>
                                <li>
                                    <a href="typo.html">Typography</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">Check Bio</div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">Check Bio</h3>
                                        </div>
                                        <hr>
                                        <form action="" method="post" novalidate="novalidate">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#mediumModal" style="width: 100%; font-size: 70px;"> < Input </button>
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="button" class="btn btn-danger mb-1" data-toggle="modal" data-target="#mediumModal" style="width: 100%; font-size: 70px;"> Output > </button>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive table--no-card m-b-30">
                                                        <table class="table table-borderless table-striped table-earning">
                                                            <thead>
                                                                <tr>
                                                                    <th>ลำดับ</th>
                                                                    <th>Barcode</th>
                                                                    <th>Name</th>
                                                                    <th class="text-right">Rack</th>
                                                                    <th class="text-right">จำนวน</th>
                                                                    <th class="text-right">ตู้</th>
                                                                    <th class="text-right">ชั้น</th>
                                                                    <th class="text-right">Patient</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>1</td>
                                                                    <td>100398</td>
                                                                    <td>iPhone X 64Gb Grey</td>
                                                                    <td class="text-right">$999.00</td>
                                                                    <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                     <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2</td>
                                                                    <td>100397</td>
                                                                    <td>Samsung S8 Black</td>
                                                                    <td class="text-right">$756.00</td>
                                                                    <td class="text-right">1</td>
                                                                    <td class="text-right">$756.00</td>
                                                                     <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>3</td>
                                                                    <td>100396</td>
                                                                    <td>Game Console Controller</td>
                                                                    <td class="text-right">$22.00</td>
                                                                    <td class="text-right">2</td>
                                                                    <td class="text-right">$44.00</td>
                                                                     <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>4</td>
                                                                    <td>100395</td>
                                                                    <td>iPhone X 256Gb Black</td>
                                                                    <td class="text-right">$1199.00</td>
                                                                    <td class="text-right">1</td>
                                                                    <td class="text-right">$1199.00</td>
                                                                     <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>5</td>
                                                                    <td>100393</td>
                                                                    <td>USB 3.0 Cable</td>
                                                                    <td class="text-right">$10.00</td>
                                                                    <td class="text-right">3</td>
                                                                    <td class="text-right">$30.00</td>
                                                                     <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal medium -->
        <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="margin-top: 20%;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Barcode</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="input-group">
                                        <input type="email" id="input2-group2" name="input2-group2" placeholder="Barcode" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal medium -->
        <?php include("_footer.php"); ?>
    </div>

   <?php include("_js.php"); ?>

   <script type="text/javascript">
       $(document).ready(function(){
            $("#check_bio2").click(function(){
                Console.log();
                $("#li_check_bio2").addClass("active");
            });
        });
   </script>

</body>

</html>
<!-- end document-->
