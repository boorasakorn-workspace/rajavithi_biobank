<iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">-->
 
    <!-- Title Page-->
    <title>Biobank</title>

    <?php include("_css.php"); ?>
    <?php include("connect.php"); ?> 
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- END MENU SIDEBAR-->
    <form action="bio_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Bio</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6"><h4>Bio</h4></div>
                                            <div class="col-md-6 text-right"><button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#add_modal"> Add </button></div>
                                        </div>
                                        <br>    
                                        <table id="example" class="table table-responsive table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th width="1%" style="white-space: nowrap; text-align: center;">Order</th>
                                                    <th width="1%" style="white-space: nowrap; text-align: left; min-width: 100px;">Barcode</th>
                                                    <th width="1%" style="white-space: nowrap; text-align: left; min-width: 300px;">Name</th>
                                                    <th width="1%" style="white-space: nowrap; text-align: left; min-width: 300px;">Detail</th>
                                                    <th width="1%" style="white-space: nowrap; text-align: left;">Manage</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                                $sql = "SELECT * FROM `ms_bio`";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    echo "<tr>
                                                        <td style='text-align: center;'>".$i."</td>
                                                        <td>".$row['bio_barcode']."</td>
                                                        <td>".$row['bio_name']."</td>
                                                        <td>".urldecode($row['bio_detail'])."</td>
                                                        <td>
                                                            <button type='button' data-toggle='modal' data-target='#edit_modal' data-toggle='tooltip' title='Edit' onclick='editbio(\"".$row['bio_id']."\",\"".$row['bio_barcode']."\",\"".$row['bio_name']."\",\"".$row['bio_detail']."\")' style='color:#666;'>
                                                                <i class='zmdi zmdi-edit'></i>
                                                            </button>
                                                        </td>
                                                    </tr>";
                                                }/*<button class='item p-l-10' data-toggle='modal' data-target='#staticModal' data-toggle='tooltip' title='Delete'  style='color:#666;'>
                                                                <i class='zmdi zmdi-delete'></i>
                                                            </button>*/
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTAINER-->
        </div>

        <!-- Add modal -->
        <div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Add Bio</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Barcode</label>
                                    <input type="text" class="form-control" name="barcode_add">
                                </div>
                            </div>               
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" class="form-control" name="name_add">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Detail</label>
                                    <textarea class="form-control" name="detail_add" rows="5"></textarea>
                                </div>
                            </div>               
                        </div>
                    </div>
                    <div class="modal-footer">    
                        <button type="submit" class="btn btn-primary" name="save_bio" value="save_bio">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- modal scroll -->
        <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Edit Bio</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <input type="hidden" class="form-control" id="edit_id" name="id_edit">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Barcode</label>
                                    <input type="text" class="form-control" id="edit_bar" name="barcode_edit">
                                </div>
                            </div>               
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" class="form-control" id="edit_name" name="name_edit">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Detail</label>
                                    <textarea class="form-control" id="edit_detail" name="detail_edit" rows="5"></textarea>
                                </div>
                            </div>               
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="edit_bio" value="edit_bio">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- modal static -->
        <!-- <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
             data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticModalLabel">Static Modal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Delect ???</p>   
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- end modal static -->
    </form>

    <?php include("_footer.php"); ?>
    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        }
            
        function editbio(id,bar,name,dt) {
            console.log(id);
            document.getElementById("edit_id").value = id;
            document.getElementById("edit_bar").value = bar;
            document.getElementById("edit_name").value = name;
            document.getElementById("edit_detail").value =  decodeURIComponent(dt);
        }
    </script>
    
</body>
</html>
<!-- end document-->