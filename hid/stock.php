<?php include("connect.php"); ?> 
<?php 
    $sqlfz = "SELECT * FROM `ms_rack_floor` LEFT JOIN ms_freezer ON ms_freezer.freezer_id = ms_rack_floor.freezer_id LEFT JOIN ms_freezer_floor ON ms_freezer_floor.freezer_floor_id = ms_rack_floor.freezer_floor_id LEFT JOIN ms_rack ON ms_rack.rack_id = ms_rack_floor.rack_id where ms_freezer_floor.freezer_id = '".$_GET['fz']."' AND ms_freezer_floor.freezer_floor_id = '".$_GET['fz_f']."' AND ms_rack.rack_id = '".$_GET['r']."' AND ms_rack_floor.rack_floor_id = '".$_GET['r_f']."'";
    $objQueryfz = $db_connection->query($sqlfz);
    while(($row = $objQueryfz->fetch_assoc()) != null){
        $fz_id = $row['freezer_id'];
        $fz_name = $row['freezer_name'];
        $fz_f_id = $row['freezer_floor_id'];
        $fz_f_name = $row['freezer_floor_name'];
        $r_id = $row['rack_id'];
        $r_name = $row['rack_name'];
        $r_f_id = $row['rack_floor_id'];
        $r_f_name = $row['rack_floor_name'];
    }
?> 
<iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">-->
 
    <!-- Title Page-->
    <title>Biobank</title>

    <?php include("_css.php"); ?>
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- END MENU SIDEBAR-->

    <form action="stock_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Stock <?php echo @$fz_name." ".@$fz_f_name." ".@$r_name." ".@$r_f_name; ?></h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6"><h4>Stock <?php echo @$fz_name." ".@$fz_f_name." ".@$r_name." ".@$r_f_name; ?></h4></div>
                                            <div class="col-md-6 text-right">
                                                <button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#add_modal"> Add Box </button>
                                                <button type="button" class="btn btn-secondary mb-1" onclick="window.history.back();"" > back </button>
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="row">
                                            <?php 
                                                $sql = "SELECT * FROM `tr_stock` where rack_floor_id = '".$_GET['r_f']."'";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    echo "<div class='col-md-4 text-center'>
                                                        <img src='images/box.png' alt='Biobank'/> 
                                                        <h3 class='text-center p-t-10 p-b-20'>".$row['box_barcode']."</h3>
                                                    </div>";
                                                }
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTAINER-->
        </div>

        <!-- Add modal -->
        <div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Add Box</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Barcode Box</label>
                                    <input type="text" class="form-control" name="bar_add">
                                    <input type="text" class="form-control" name="rack_floor_id" value="<?php echo $r_f_id; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"> 
                        <button type="submit" class="btn btn-primary" name="save_stock" value="save_stock">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- modal scroll -->
        <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Edit Rack</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <input type="hidden" class="form-control" id="edit_id" name="id_edit">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Barcode</label>
                                    <input type="text" class="form-control" id="edit_bar" name="barcode_edit">
                                </div>
                            </div>               
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" class="form-control" id="edit_name" name="name_edit">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" name="edit_rack" value="edit_rack">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- modal static -->
        <!-- <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
             data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticModalLabel">Static Modal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Delect ???</p>   
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- end modal static -->
    </form>

    <?php include("_footer.php"); ?>
    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        }
            
        function editbio(id,bar,name) {
            document.getElementById("edit_id").value = id;
            document.getElementById("edit_bar").value = bar;
            document.getElementById("edit_name").value = name;
        } 
    </script>

</body>
</html>
<!-- end document-->