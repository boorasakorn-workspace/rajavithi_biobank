<iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags-->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- <meta name="description" content="au theme template">
	<meta name="author" content="Hau Nguyen">
	<meta name="keywords" content="au theme template">
 -->
	<!-- Title Page-->
	<title>Biobank</title>

	<?php include("_css.php"); ?>
	<?php include("./vendor/datatables/_css_datatable.php"); ?>

	<!-- <link href="vendor/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="all"> -->
	<link href="vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		
	</style>
</head>

<body class="animsition">
	<div class="page-wrapper">
		<!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- END HEADER MOBILE-->

		<!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- END MENU SIDEBAR-->
	<form action="inventory_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
		<!-- PAGE CONTAINER-->
		<div class="page-container">
			<!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- HEADER DESKTOP-->

			<!-- MAIN CONTENT-->
			<div class="main-content">
				<div class="section__content section__content--p30">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-header">
										<h4>Inventory</h4>
									</div>
									<div class="card-body">
										<div class="default-tab">
											<nav>
												<div class="nav nav-tabs" id="nav-tab" role="tablist">
													<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home"
													 aria-selected="true">Home</a>
													<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile"
													 aria-selected="false">DNA</a>
													<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact"
													 aria-selected="false">RNA Sample</a>
													<a class="nav-item nav-link" id="nav-serum-tab" data-toggle="tab" href="#nav-serum" role="tab" aria-controls="nav-contact"
													 aria-selected="false">Serum / plasma sample</a>
													<a class="nav-item nav-link" id="nav-tissue-tab" data-toggle="tab" href="#nav-tissue" role="tab" aria-controls="nav-contact"
													 aria-selected="false">Tissue sample</a>
												</div>
											</nav>
											<div class="tab-content pl-3 pt-2" id="nav-tabContent">
												<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
													<br>
													<div class="row">
														<div class="col-md-6"><h4>Home</h4></div>
														<div class="col-md-6 text-right"><button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#mediumModal"> Add </button></div>
													</div>
													<br>
													<table id="example" class="table table-responsive table-striped table-bordered" style="width:100%">
												        <thead>
												            <tr>
												                <th width="1%" style="white-space: nowrap; text-align: left; min-width: 300px;">Name</th>
												                <th width="1%">QTY</th>
												                <th width="1%">Floor</th>
												                <th width="1%">Rack</th>
												                <th width="1%">date</th>
												                <th width="1%" style="white-space: nowrap; text-align: left;">Modify</th>
												            </tr>
												        </thead>
												        <tbody>
												        	<?php 
												        		for($i=0;$i<=20;$i++){
												        			echo "<tr>
															            <td>Tiger Nixon</td>
															            <td>System Architect</td>
															            <td>Edinburgh</td>
															            <td>61</td>
														                <td>2011/04/25</td>
														                <td>
							                                                <button type='button' data-toggle='modal' data-target='#mediumModal' data-toggle='tooltip' title='Edit' style='color:#666;'>
							                                                    <i class='zmdi zmdi-edit'></i>
							          	 	                                </button>
							                                                <button class='item p-l-10' data-toggle='modal' data-target='#staticModal' data-toggle='tooltip' title='Delete' style='color:#666;'>
							                                                    <i class='zmdi zmdi-delete'></i>
							                                                </button>
							                                            </td>
															        </tr>";
												        		}
												        	?>
												        </tbody>
												    </table>
												</div>
												<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
													<br>
													<div class="row">
														<div class="col-md-6"><h4>DNA</h4></div>
														<div class="col-md-6 text-right"><button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#mediumModaldna"> Add </button></div>
													</div>
													<br>
													<table id="example_dna" class="table table-responsive table-striped table-bordered" style="width:100%">
												        <thead>
												            <tr>
												                <th width="1%" style="white-space: nowrap; text-align: left; min-width: 300px;">Name</th>
												                <th width="1%">QTY</th>
												                <th width="1%">Floor</th>
												                <th width="1%">Rack</th>
												                <th width="1%">date</th>
												                <th width="1%" style="white-space: nowrap; text-align: left;">Modify</th>
												            </tr>
												        </thead>
												        <tbody>
												        	<?php 
												        		for($i=0;$i<=20;$i++){
												        			echo "<tr>
															            <td>Tiger Nixon</td>
															            <td>System Architect</td>
															            <td>Edinburgh</td>
															            <td>61</td>
															            <td>2011/04/25</td>
															            <td>
							                                                <button type='button' data-toggle='modal' data-target='#mediumModal' data-toggle='tooltip' title='Edit' style='color:#666;'>
							                                                    <i class='zmdi zmdi-edit'></i>
							                                                </button>
							                                                <button class='item p-l-10' data-toggle='modal' data-target='#staticModal' data-toggle='tooltip' title='Delete' style='color:#666;'>
							                                                    <i class='zmdi zmdi-delete'></i>
							                                                </button>
							                                            </td>
															        </tr>";
												        		}
												        	?>
												        </tbody>
												    </table>
												</div>
												<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
													<br>
													<div class="row">
														<div class="col-md-6"><h4>RNA Sample</h4></div>
														<div class="col-md-6 text-right"><button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#mediumModal"> Add </button></div>
													</div>
													<br>
													<table id="example_rna" class="table table-responsive table-striped table-bordered" style="width:100%">
												        <thead>
												            <tr>
												                <th width="1%" style="white-space: nowrap; text-align: left; min-width: 300px;">Name</th>
												                <th width="1%">QTY</th>
												                <th width="1%">Floor</th>
												                <th width="1%">Rack</th>
												                <th width="1%">date</th>
												                <th width="1%" style="white-space: nowrap; text-align: left;">Modify</th>
												            </tr>
												        </thead>
												        <tbody>
												        	<?php 
												        		for($i=0;$i<=20;$i++){
												        			echo "<tr>
															            <td>Tiger Nixon</td>
															            <td>System Architect</td>
															            <td>Edinburgh</td>
															            <td>61</td>
															            <td>2011/04/25</td>
															            <td>
							                                                <button type='button' data-toggle='modal' data-target='#mediumModal' data-toggle='tooltip' title='Edit' style='color:#666;'>
							                                                    <i class='zmdi zmdi-edit'></i>
							                                                </button>
							                                                <button class='item p-l-10' data-toggle='modal' data-target='#staticModal' data-toggle='tooltip' title='Delete' style='color:#666;'>
							                                                    <i class='zmdi zmdi-delete'></i>
							                                                </button>
							                                            </td>
															        </tr>";
												        		}
												        	?>
												        </tbody>
												    </table>
												</div>
												<div class="tab-pane fade" id="nav-serum" role="tabpanel" aria-labelledby="nav-serum-tab">
													<br>
													<div class="row">
														<div class="col-md-6"><h4>Serum / Plasma sample</h4></div>
														<div class="col-md-6 text-right"><button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#mediumModal"> Add </button></div>
													</div>
													<br>
													<table id="example_serum" class="table table-responsive table-striped table-bordered" style="width:100%">
												        <thead>
												            <tr>
												                <th width="1%" style="white-space: nowrap; text-align: left; min-width: 300px;">Name</th>
												                <th width="1%">QTY</th>
												                <th width="1%">Floor</th>
												                <th width="1%">Rack</th>
												                <th width="1%">date</th>
												                <th width="1%" style="white-space: nowrap; text-align: left;">Modify</th>
												            </tr>
												        </thead>
												        <tbody>
												        	<?php 
												        		for($i=0;$i<=20;$i++){
												        			echo "<tr>
															            <td>Tiger Nixon</td>
															            <td>System Architect</td>
															            <td>Edinburgh</td>
															            <td>61</td>
															            <td>2011/04/25</td>
															            <td>
							                                                <button type='button' data-toggle='modal' data-target='#mediumModal' data-toggle='tooltip' title='Edit' style='color:#666;'>
							                                                    <i class='zmdi zmdi-edit'></i>
							                                                </button>
							                                                <button class='item p-l-10' data-toggle='modal' data-target='#staticModal' data-toggle='tooltip' title='Delete' style='color:#666;'>
							                                                    <i class='zmdi zmdi-delete'></i>
							                                                </button>
							                                            </td>
															        </tr>";
												        		}
												        	?>
												        </tbody>
												    </table>
												</div>
												<div class="tab-pane fade" id="nav-tissue" role="tabpanel" aria-labelledby="nav-tissue-tab">
													<br>
													<div class="row">
														<div class="col-md-6"><h4>Tissue sample</h4></div>
														<div class="col-md-6 text-right"><button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#mediumModal"> Add </button></div>
													</div>
													<br>
													<table id="example_tissue" class="table table-responsive table-striped table-bordered" style="width:100%">
												        <thead>
												            <tr>
												                <th width="1%" style="white-space: nowrap; text-align: left; min-width: 300px;">Name</th>
												                <th width="1%">QTY</th>
												                <th width="1%">Floor</th>
												                <th width="1%">Rack</th>
												                <th width="1%">date</th>
												                <th width="1%" style="white-space: nowrap; text-align: left;">Modify</th>
												            </tr>
												        </thead>
												        <tbody>
												        	<?php 
												        		for($i=0;$i<=40;$i++){
												        			echo "<tr>
															            <td>Tiger Nixon</td>
															            <td>System Architect</td>
															            <td>Edinburgh</td>
															            <td>61</td>
															            <td>2011/04/25</td>
															            <td>
							                                                <button type='button' data-toggle='modal' data-target='#mediumModal' data-toggle='tooltip' title='Edit' style='color:#666;'>
							                                                    <i class='zmdi zmdi-edit'></i>
							                                                </button>
							                                                <button class='item p-l-10' data-toggle='modal' data-target='#staticModal' data-toggle='tooltip' title='Delete' style='color:#666;'>
							                                                    <i class='zmdi zmdi-delete'></i>
							                                                </button>
							                                            </td>
															        </tr>";
												        		}
												        	?>
												        </tbody> 
												    </table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /# column -->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		
		<!-- Add modal -->
        <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Add Bio</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Name</label>
                                        <input type="text" class="form-control" name="name" value="">
                                    </div>
                                </div>               
                                <div class="col-md-2">
                                    <div class="form-group">
                                    	<label class="control-label">QTY</label>
                                        <input type="text" class="form-control" name="qty" value="">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                    	<label class="control-label">Floor</label>
                                        <input type="text" class="form-control" name="floor" value="" >
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                    	<label class="control-label">Rack</label>
                                        <input type="text" class="form-control" name="rack" value="" >
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group"> 
                                    	<label class="control-label">Date</label>
                                        <input type="text" class="form-control" name="date" id="form_date" value="">
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" name="save_bio" value="save_bio">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- modal medium -->
        <div class="modal fade" id="mediumModaldna" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="margin-top: 20%;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Barcode</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Name</label>
                                        <input name="cc-exp" type="text" class="form-control" value="" data-val="true" data-val-required="Please enter the card expiration" data-val-cc-exp="Please enter a valid month and year" placeholder="Name..." autocomplete="cc-exp">
                                    </div>
                                </div>               
                                <div class="col-md-2">
                                    <div class="form-group">
                                    	<label class="control-label">QTY</label>
                                        <input name="x_card_code" type="text" class="form-control" value="" data-val="true" data-val-required="Please enter the security code" data-val-cc-cvc="Please enter a valid security code" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                    	<label class="control-label">Floor</label>
                                        <input name="x_card_code" type="text" class="form-control" value="" data-val="true" data-val-required="Please enter the security code" data-val-cc-cvc="Please enter a valid security code" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                    	<label class="control-label">Rack</label>
                                        <input name="x_card_code" type="text" class="form-control" value="" data-val="true" data-val-required="Please enter the security code" data-val-cc-cvc="Please enter a valid security code" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group"> 
                                    	<label class="control-label">Date</label>
                                        <input name="x_card_code" type="text" class="form-control" value="" data-val="true" data-val-required="Please enter the security code" data-val-cc-cvc="Please enter a valid security code" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal medium -->

        <!-- modal static -->
		<div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
			 data-backdrop="static">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="staticModalLabel">Delect Bio</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="" method="post">
                            <div class="row">
                            	<div class="col-md-12">
									<p>Delect ?</p>	
								</div>
                            </div>
                        </form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary">Confirm</button>
					</div>
				</div>
			</div>
		</div>
		<!-- end modal static -->
    </form>  
		<?php include("_footer.php"); ?>
		<?php include("_js.php"); ?>
		<?php include("./vendor/datatables/_js_datatable.php"); ?>

		<!-- <script src="vendor/datepicker/jquery/jquery-1.8.3.min.js"></script>
		<script src="vendor/datepicker/bootstrap/js/bootstrap.min.js"></script>
		 -->
		 
		<!-- <script src="vendor/datepicker/js/bootstrap-datetimepicker.js"></script>
		<script src="vendor/datepicker/js/locales/bootstrap-datetimepicker.fr.js"></script> -->

		<!-- Date Picker Plugin JavaScript -->
	    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
	    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>
	    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.th.min.js"></script>

		<script type="text/javascript">
			$(document).ready(function() {
			    $('#example').DataTable();
			});

			$(document).ready(function() {
			    $('#example_dna').DataTable();
			});

			$(document).ready(function() {
			    $('#example_rna').DataTable();
			});

			$(document).ready(function() {
			    $('#example_serum').DataTable();
			});

			$(document).ready(function() {
			    $('#example_tissue').DataTable();
			});

			function showResult(result,id){
	            if(result==1){
	                location.reload();
	            }
	        }
	
			$(document).ready(function(){
		        $("#form_date").each(function() {    
		            $(this).datepicker({
		                format: 'dd/mm/yyyy',
		                todayBtn: false,
		                language: 'eng',             
		                thaiyear: false,
		                autoclose:true,   
		            }).datepicker('setDate', '');
		        });
		    });
		</script>
</body>

</html>
<!-- end document-->