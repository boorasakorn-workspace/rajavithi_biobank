<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Forget Password</title>

    <?php include("_css.php"); ?>

</head>
<body class="animsition">
    <div class="page-wrapper">
            <div class="container">
                    <div class="login-content">
                        <div class="login-logo" style="margin-top: 3%;">
                            <a href="#">
                                <img src="images/icon/logo.png" alt="CoolAdmin">
                            </a>
                        </div>
                        <div class="login-form" style="margin-top: 5%;">
                            <form action="" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#mediumModal" style="width: 100%; font-size: 70px;"> เข้า </button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger mb-1" data-toggle="modal" data-target="#mediumModal" style="width: 100%; font-size: 70px;"> ออก </button>
                                    </div>
                                </div>
                                
                                <div class="row" style="margin-top: 5%;">
                                    <div class="col-md-12">
                                        <div class="table-responsive table--no-card m-b-30">
                                            <table class="table table-borderless table-striped table-earning">
                                                <thead>
                                                    <tr>
                                                        <th>ลำดับ</th>
                                                        <th>Barcode</th>
                                                        <th>Name</th>
                                                        <th class="text-right">Rack</th>
                                                        <th class="text-right">จำนวน</th>
                                                        <th class="text-right">ตู้</th>
                                                        <th class="text-right">ชั้น</th>
                                                        <th class="text-right">Patient</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>100398</td>
                                                        <td>iPhone X 64Gb Grey</td>
                                                        <td class="text-right">$999.00</td>
                                                        <td class="text-right">1</td>
                                                        <td class="text-right">$999.00</td>
                                                         <td class="text-right">1</td>
                                                        <td class="text-right">$999.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>100397</td>
                                                        <td>Samsung S8 Black</td>
                                                        <td class="text-right">$756.00</td>
                                                        <td class="text-right">1</td>
                                                        <td class="text-right">$756.00</td>
                                                         <td class="text-right">1</td>
                                                        <td class="text-right">$999.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>100396</td>
                                                        <td>Game Console Controller</td>
                                                        <td class="text-right">$22.00</td>
                                                        <td class="text-right">2</td>
                                                        <td class="text-right">$44.00</td>
                                                         <td class="text-right">1</td>
                                                        <td class="text-right">$999.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>100395</td>
                                                        <td>iPhone X 256Gb Black</td>
                                                        <td class="text-right">$1199.00</td>
                                                        <td class="text-right">1</td>
                                                        <td class="text-right">$1199.00</td>
                                                         <td class="text-right">1</td>
                                                        <td class="text-right">$999.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td>100393</td>
                                                        <td>USB 3.0 Cable</td>
                                                        <td class="text-right">$10.00</td>
                                                        <td class="text-right">3</td>
                                                        <td class="text-right">$30.00</td>
                                                         <td class="text-right">1</td>
                                                        <td class="text-right">$999.00</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
        </div>
        <!-- modal medium -->
        <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="margin-top: 20%;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Barcode</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="input-group">
                                        <input type="email" id="input2-group2" name="input2-group2" placeholder="Barcode" class="form-control">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal medium -->
    </div>

    <?php include("_js.php"); ?>

</body>
</html>
<!-- end document-->