<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Forms</title>

    <?php include("_css.php"); ?>

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <h1>Biobank</h1>
                    <!-- <img src="images/icon/logo.png" alt="Cool Admin" /> -->
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li>
                            <a href="index.php"><i class="fas fa-chart-bar"></i>Dashboard</a> 
                        </li>
                        <li class="active">
                            <a href="check_bio.php"><i class="fas fa-chart-bar"></i>Check Bio</a> 
                        </li>
                        <li>
                            <a href="inventory.php"><i class="fas fa-chart-bar"></i>Inventory</a> 
                        </li>
                        <li>
                            <a href="patient.php"><i class="fas fa-chart-bar"></i>Patient</a> 
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">Check Bio</div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">Check Bio</h3>
                                        </div>
                                        <hr>
                                        <form action="" method="post" novalidate="novalidate">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#mediumModalinput" style="width: 100%; font-size: 70px;"> < Input </button>
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="button" class="btn btn-danger mb-1" data-toggle="modal" data-target="#mediumModaloutput" style="width: 100%; font-size: 70px;"> Output > </button>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive table--no-card m-b-30">
                                                        <table class="table table-borderless table-striped table-earning">
                                                            <thead>
                                                                <tr>
                                                                    <th>Order</th>
                                                                    <th>Barcode</th>
                                                                    <th>Name</th>
                                                                    <th class="text-right">Rack</th>
                                                                    <th class="text-right">Num</th>
                                                                    <th class="text-right">storage</th>
                                                                    <th class="text-right">Floor</th>
                                                                    <th class="text-right">Patient</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>1</td>
                                                                    <td>100398</td>
                                                                    <td>iPhone X 64Gb Grey</td>
                                                                    <td class="text-right">$999.00</td>
                                                                    <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                     <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2</td>
                                                                    <td>100397</td>
                                                                    <td>Samsung S8 Black</td>
                                                                    <td class="text-right">$756.00</td>
                                                                    <td class="text-right">1</td>
                                                                    <td class="text-right">$756.00</td>
                                                                     <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>3</td>
                                                                    <td>100396</td>
                                                                    <td>Game Console Controller</td>
                                                                    <td class="text-right">$22.00</td>
                                                                    <td class="text-right">2</td>
                                                                    <td class="text-right">$44.00</td>
                                                                     <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>4</td>
                                                                    <td>100395</td>
                                                                    <td>iPhone X 256Gb Black</td>
                                                                    <td class="text-right">$1199.00</td>
                                                                    <td class="text-right">1</td>
                                                                    <td class="text-right">$1199.00</td>
                                                                     <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>5</td>
                                                                    <td>100393</td>
                                                                    <td>USB 3.0 Cable</td>
                                                                    <td class="text-right">$10.00</td>
                                                                    <td class="text-right">3</td>
                                                                    <td class="text-right">$30.00</td>
                                                                     <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>6</td>
                                                                    <td>100398</td>
                                                                    <td>iPhone X 64Gb Grey</td>
                                                                    <td class="text-right">$999.00</td>
                                                                    <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                     <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>7</td>
                                                                    <td>100397</td>
                                                                    <td>Samsung S8 Black</td>
                                                                    <td class="text-right">$756.00</td>
                                                                    <td class="text-right">1</td>
                                                                    <td class="text-right">$756.00</td>
                                                                     <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>8</td>
                                                                    <td>100396</td>
                                                                    <td>Game Console Controller</td>
                                                                    <td class="text-right">$22.00</td>
                                                                    <td class="text-right">2</td>
                                                                    <td class="text-right">$44.00</td>
                                                                    <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>9</td>
                                                                    <td>100395</td>
                                                                    <td>iPhone X 256Gb Black</td>
                                                                    <td class="text-right">$1199.00</td>
                                                                    <td class="text-right">1</td>
                                                                    <td class="text-right">$1199.00</td>
                                                                    <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>10</td>
                                                                    <td>100393</td>
                                                                    <td>USB 3.0 Cable</td>
                                                                    <td class="text-right">$10.00</td>
                                                                    <td class="text-right">3</td>
                                                                    <td class="text-right">$30.00</td>
                                                                    <td class="text-right">1</td>
                                                                    <td class="text-right">$999.00</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal medium -->
        <div class="modal fade" id="mediumModalinput" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="margin-top: 20%;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Input</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="input-group">
                                        <input type="email" id="input2-group2" name="input2-group2" placeholder="Barcode" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal medium -->

        <!-- modal medium -->
        <div class="modal fade" id="mediumModaloutput" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="margin-top: 20%;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Output</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="input-group">
                                        <input type="email" id="input2-group2" name="input2-group2" placeholder="Barcode" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal medium -->
        <?php include("_footer.php"); ?>
    </div>

   <?php include("_js.php"); ?>

   <script type="text/javascript">
       $(document).ready(function(){
            $("#check_bio2").click(function(){
                $("#li_check_bio2").addClass("active");
            });
        });
   </script>

</body>

</html>
<!-- end document-->
