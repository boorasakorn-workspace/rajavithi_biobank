<!-- HEADER DESKTOP-->
<header class="header-desktop">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="header-wrap">
                <div class="form-header" action="" method="POST">
                             
                </div>
                <div class="header-button">
                    <div class="noti-wrap">
                                    
                    </div>
                    <div class="header-wrap">
                        <div class="account-item clearfix js-item-menu">
                            <div class="image">
                                <img src="images/user/<?php echo $_SESSION["user_image"]; ?>" alt="" />
                            </div>
                            <div class="content">
                                <a class="js-acc-btn" href="#"><?php echo $_SESSION["user_name"]; ?></a>
                            </div>
                            <div class="account-dropdown js-dropdown">
                                <div class="info clearfix">
                                    <div class="image">
                                        <a href="">
                                            <img src="images/user/<?php echo $_SESSION["user_image"]; ?>" alt="" />
                                        </a>
                                    </div>
                                    <div class="content">
                                        <h5 class="name">
                                            <a href=""><?php echo $_SESSION["user_name"]; ?></a>
                                        </h5>
                                        <span class="email"><?php echo $_SESSION["user_email"]; ?></span>
                                    </div>
                                </div>
                                <div class="account-dropdown__body">
                                    <div class="account-dropdown__item">
                                        <a href="account.php"><i class="fa fa-user"></i>Account</a>
                                    </div>
                                    <div class="account-dropdown__item">
                                        <a href="change_password.php"><i class="fa fa-lock"></i>Change Password</a>
                                    </div>
                                </div>
                                <div class="account-dropdown__footer">
                                    <a href="logout.php"><i class="fa fa-power-off"></i>Logout</a> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- HEADER DESKTOP-->