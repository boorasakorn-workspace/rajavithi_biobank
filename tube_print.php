<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>
	<style type="text/css">   
		.printable { display: block; }  
		@media print{    
		     .non-printable { display: none; }   
		     .printable { display: block; }   
		}

		.button {
		    text-align: center;
		    text-decoration: none;
		    display: inline-block;
		    font-size: 16px;
		    margin: 2px 2px;
		    cursor: pointer;
		    font-weight: 400;
		    white-space: nowrap;
		    vertical-align: middle;
		    color: #fff;
		    border-color: #007bff;
		    user-select: none;
		    border: 1px solid transparent;
		    padding: .375rem .75rem;
		    line-height: 1.5;
		    border-radius: .25rem;
		    transition: color .15s 
		}
	</style>  
</head>
<body>
	<center>
	<div class="non-printable">
		<br><br>
		<button class="button" style="background-color: #007bff;" onclick="window.print()">Print</button>
		<button class="button" style="background-color: #6c757d;" onclick="window.history.back();"> Back </button>
	</div>
	<div class="non-printable">
		<br><br>
	</div>
	<?php
		foreach ($_POST['tube_check'] as $key => $value){
			echo "<img alt='Coding Sips' src='barcode.php?size=15&text=".$value."&print=true' style='padding:10px;' />";
		}
	?>
	</center>
</body>
</html>
