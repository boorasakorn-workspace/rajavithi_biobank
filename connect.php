<?php
	date_default_timezone_set("Asia/Bangkok");
	$db_host = "";
	$db_user = "";
	$db_pass = "";
	$db_prefix = "";
	$db_name = "";
	$db_connection = new mysqli($db_host,$db_user,$db_pass,$db_name);
	if ($db_connection->connect_errno) {
   		printf("Connect failed: %s\n", $db_connection->connect_error);
    	exit();
	}
	$db_connection->set_charset("utf8");
?>