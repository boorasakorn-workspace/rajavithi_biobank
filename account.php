<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: login.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }
    include("connect.php"); 
?> 
<?php
	$sqlacc = "SELECT * FROM `user` WHERE user_id = ".$_SESSION["user_id"]."";
    $objacc = $db_connection->query($sqlacc);
    while(($row = $objacc->fetch_assoc()) != null){
        $acc_idcard = $row['user_idcard'];
        $acc_prefix = $row['user_prefix'];
        $acc_gender = $row['user_gender'];
        $acc_hbd = $row['user_hbd'];
        $acc_age = $row['user_age'];
        $acc_fname = $row['user_fname'];
        $acc_lname = $row['user_lname'];
        $acc_email = $row['user_email'];
        $acc_phone = $row['user_phone'];
        $acc_image = $row['user_image'];
        $acc_imagename = $row['user_imagename'];
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

	<?php include("_css.php"); ?>
	<?php include("./vendor/datatables/_css_datatable.php"); ?>

	<!-- <link href="vendor/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="all"> -->
	<link href="vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
        h2 {
            color: #555;
            margin-top: 15px;
            text-align: center;
        }
        .btn-lg{
            margin-top: 15px;
            padding: .3rem 2rem;
        }
        .lead {
             margin-top: 15px;
        }
        #red{
            color: red;
        }
	</style>
</head>
<body class="animsition">
	<div class="page-wrapper">
		<!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
		<!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
		<!-- PAGE CONTAINER-->
		<div class="page-container">
			<!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
			<!-- MAIN CONTENT-->
			<div class="main-content">
				<iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
				<form action="account_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
				<div class="section__content section__content--p30">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-header">
										<h4>Account</h4>
									</div>
									<div class="card-body">
										<div class="card-title">
                                            <h4 class="text-center title-2">Account</h4>
                                        </div>
                                        <hr>
                                        <div class="row">
		                                	<div class="col-md-2">
		                                        <div class="form-group">
		                                            <label class="control-label">Prefix</label>
		                                            <select class="form-control" name="user_prefix">
		                                                <option value="">Please select</option>
		                                                <option value="Mr." <?php if($acc_prefix=='Mr.'){ echo "selected"; } ?>>Mr.</option>
		                                                <option value="Mrs." <?php if($acc_prefix=='Mrs.'){ echo "selected"; } ?>>Mrs.</option>
		                                                <option value="Miss." <?php if($acc_prefix=='Miss.'){ echo "selected"; } ?>>Miss.</option>
		                                            </select>
		                                        </div>
		                                    </div>  
		                                    <div class="col-md-5">
		                                        <div class="form-group">
		                                            <label class="control-label">Firstname <a id="red">*</a></label>
		                                            <input type="text" class="form-control" name="user_fname" value="<?php echo $acc_fname; ?>">
		                                        </div>
		                                    </div>  
		                                    <div class="col-md-5">
		                                        <div class="form-group">
		                                            <label class="control-label">Lastname <a id="red">*</a></label>
		                                            <input type="text" class="form-control" name="user_lname" value="<?php echo $acc_lname; ?>">
		                                        </div>
		                                    </div>     
		                                </div> 
		                                <div class="row">
		                                    <div class="col-md-4">
		                                        <div class="form-group">
		                                            <label class="control-label">Birthday</label>
		                                            <input type="text" class="form-control" id="user_hbd" name="user_hbd" value="" maxlength="10"> 
		                                        </div>
		                                    </div>  
		                                    <div class="col-md-4">
		                                        <div class="form-group">
		                                            <label class="control-label">Age</label>
		                                            <input type="number" class="form-control" name="user_age" value="<?php echo $acc_age; ?>">
		                                        </div>
		                                    </div>
		                                    <div class="col-md-4">
		                                    	<label class="control-label">Gender</label>
		                                        <div class="form-group">
		                                            <div class="form-check-inline form-check">
		                                                <label class="form-check-label">
		                                                    <input class="form-check-input" type="radio" name="user_gender"  value="Male" <?php if($acc_gender=='Male'){ echo "checked"; } ?>> Male
		                                                </label>
		                                                <label class="form-check-label p-l-30">
		                                                    <input class="form-check-input" type="radio" name="user_gender" value="Female" <?php if($acc_gender=='Female'){ echo "checked"; } ?>> Female
		                                                </label>
		                                            </div>
		                                        </div>
		                                    </div>        
		                                </div>
		                                <div class="row">
		                                	<div class="col-md-4">
		                                        <div class="form-group">
		                                            <label class="control-label">Staff ID <a id="red">*</a></label>
		                                            <input type="text" class="form-control" name="user_idcard" value="<?php echo $acc_idcard; ?>">
		                                            <input type="hidden" class="form-control" name="user_idcardold" value="<?php echo $acc_idcard; ?>">
		                                            <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>"> 
		                                        </div>
		                                    </div>
		                                    <div class="col-md-5">
		                                        <div class="form-group">
		                                            <label class="control-label">Email <a id="red">*</a></label>
		                                            <input type="email" class="form-control" name="user_email" value="<?php echo $acc_email; ?>">
		                                        </div>
		                                    </div>  
		                                    <div class="col-md-3">
		                                        <div class="form-group">
		                                            <label class="control-label">Phone</label>
		                                            <input type="text" class="form-control" name="user_phone" value="<?php echo $acc_phone; ?>" maxlength="10" onKeyPress="CheckNum()">
		                                        </div>
		                                    </div>     
		                                </div>
		                                <div class="row form-group">
		                                    <div class="col col-md-2">
		                                        <label class="control-label">Profile Image</label>
		                                    </div>
		                                    <div class="col-12 col-md-3">
		                                        <input type="file" class="form-control-file" id="user_image" name="acc_image">
		                                        <input type="hidden" class="form-control" id="user_image" name="user_image" value="<?php echo $acc_image; ?>">
		                                        <input type="hidden" class="form-control" id="user_image" name="user_imagename" value="<?php echo $acc_imagename; ?>">
		                                    </div>
		                                    <div class="col-12 col-md-3">
		                                        <?php echo $acc_imagename; ?>
		                                    </div>
		                                </div>
		                                <hr>
		                                <button type="submit" class="btn btn-primary" name="save_acc" value="save_acc">Confirm</button>
		                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
									</div>
								</div>
							</div>  
						</form>  
						<!-- /# column -->
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->
	</div>

	<?php include("_footer.php"); ?>
	<?php include("_js.php"); ?>
	<?php include("./vendor/datatables/_js_datatable.php"); ?>
	<!-- <script src="vendor/datepicker/jquery/jquery-1.8.3.min.js"></script>
	<script src="vendor/datepicker/bootstrap/js/bootstrap.min.js"></script> -->
	<!-- <script src="vendor/datepicker/js/bootstrap-datetimepicker.js"></script>
	<script src="vendor/datepicker/js/locales/bootstrap-datetimepicker.fr.js"></script> -->
	<!-- Date Picker Plugin JavaScript -->
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
	<script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>
	<script src="vendor/bootstrap-datepicker/bootstrap-datepicker.th.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#example').DataTable();
		});

		$(document).ready(function() {
		    $('#example_dna').DataTable();
		});

		$(document).ready(function() {
			$('#example_rna').DataTable();
		});

		$(document).ready(function() {
		    $('#example_serum').DataTable();
		});

		$(document).ready(function() {
		    $('#example_tissue').DataTable();
		});

		function showResult(result,id){
	        if(result==1){
	            location.reload();
	        }
	        else if(result==0){
		        swal({
		            title: "Fail",
		        });
		    }
	        else if(result==3){
		        swal({
		            title: "Fail",
		            text: "Upload File Images",
		        });
		    }
		    else if(result==4){
		        swal({
		            title: "Fail",
		            text: "Duplicate Staff ID",
		        });
		    }
	    }
	
		$(document).ready(function(){
	        $("#user_hbd").each(function() {    
	            $(this).datepicker({
		            format: 'dd-mm-yyyy',
		            todayBtn: false,
		            language: 'eng',             
		            thaiyear: false,
		            autoclose:true,   
		        }).datepicker('setDate', '<?php echo $acc_hbd; ?>');
		    });
	    });

		function CheckNum(){
		    if(event.keyCode < 48 || event.keyCode > 57){
		        event.returnValue = false;
	        }
	    }
	</script>
</body>
</html>
<!-- end document-->