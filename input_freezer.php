<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: login.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }
    include("connect.php"); 
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

    <?php include("_css.php"); ?>
    <?php include("connect.php"); ?> 
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <?php include("_header_mobile.php"); ?>
        <!-- MENU SIDEBAR-->
        <?php include("_menu.php"); ?>
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?php include("_header_desktop.php"); ?>
            <!-- MAIN CONTENT--> 
            <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
            <form action="freezer_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Input</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6"></div>
                                            <!-- <div class="col-md-6 text-right"><button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#add_modal"> New Freezer </button></div> -->
                                        </div>
                                        <div class="row">
                                            <?php 
                                                $sql = "SELECT * FROM `ms_freezer` WHERE freezer_status !=1";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    echo "<div class='col-md-4 text-center'>
                                                        <a href='input_freezer_floor.php?fz=".$row['freezer_id']."'>
                                                        <img src='images/freezer_n.png' alt='Biobank' width='70%' />
                                                        </a>
                                                        <h3 class='text-center p-t-10 p-b-20'>".$row['freezer_name']." </h3>
                                                    </div>";
                                                }
                                            ?> <!-- <button class='btn btn-sm btn-warning' data-toggle='modal' data-target='#edit_modal' onclick='editname(\"".$row['freezer_id']."\",\"".$row['freezer_name']."\")'>Edit</button> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTAINER-->
        </div>

        <!-- Add modal -->
        <div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">New Freezer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" class="form-control" name="name_add">
                                    <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="save_freezer" value="save_freezer">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- Add modal -->
        <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Edit Name Freezer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" class="form-control" name="name_edit" id="name_edit">
                                    <input type="hidden" class="form-control" name="id_edit" id="id_edit">
                                    <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="edit_freezer" value="edit_freezer">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->
    </form>

    <?php include("_footer.php"); ?>
    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>

    <script type="text/javascript">
        function showResult(result,id){
            if(result==1){      
                location.reload();
            }
            else{
                location.reload();
            }
        }

        function editname(id,name){
            document.getElementById("id_edit").value = id;
            document.getElementById("name_edit").value = name;
        }
    </script>
</body>
</html>
<!-- end document-->