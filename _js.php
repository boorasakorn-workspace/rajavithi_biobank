<!-- Jquery JS-->
<script src="vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="vendor/bootstrap-4.1/popper.min.js"></script>
<script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS -->
<script src="vendor/slick/slick.min.js"></script>
<script src="vendor/wow/wow.min.js"></script>
<script src="vendor/animsition/animsition.min.js"></script>
<script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<script src="vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="vendor/counter-up/jquery.counterup.min.js"></script>
<script src="vendor/circle-progress/circle-progress.min.js"></script>
<script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="vendor/chartjs/Chart.bundle.min.js"></script>
<script src="vendor/select2/select2.min.js"></script>
<script src="vendor/sweetalert/sweetalert.js"></script>
<script src="vendor/sweetalert/sweetalert.min.js"></script>
<!-- Main JS-->
<script src="js/main.js"></script>
<script>
	$(function(){
	    var url = window.location.pathname, 
	    urlRegExp = new RegExp(url.replace(/\/$/,'') + "$"); // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
	        // now grab every link from the navigation
	    $('.menu li a').each(function(){
	        // and test its normalized href against the url pathname regexp
	        if(urlRegExp.test(this.href.replace(/\/$/,''))){
	           	if(url == "/biobank/index.php"){
					$("#index").addClass('active');
	            }
	            else if(url == "/biobank/account.php"){
	            	$("#account").addClass('active');
	            	$("#manage").css('display','block');
	            }
	            else if(url == "/biobank/rack.php"){
	            	$("#rack").addClass('active');
	            	$("#manage").css('display','block');
	            }
	            else if(url == "/biobank/freezer.php"){
	            	$("#freezer").addClass('active');
	            	$("#manage1").css('display','block');
	            } 
	            else if(url == "/biobank/box.php"){
	            	$("#box").addClass('active');
	            	$("#manage1").css('display','block');
	            } 
	            else if(url == "/biobank/sample.php"){
	            	$("#sample").addClass('active');
	            	/*$("#manage").css('display','block');
	            	$("#manage1").css('display','block');*/
	            }
	            else if(url == "/biobank/search.php"){
	            	$("#search").addClass('active');
	            	/*$("#manage").css('display','block');*/
	            } 
	            else if(url == "/biobank/report.php"){
	            	$("#report").addClass('active');
	            	/*$("#manage").css('display','block');*/
	            } 
	            else if(url == "/biobank/change_password.php"){
	            	$("#change_password").addClass('active');
	            	$("#manage").css('display','block');
	            } 
	            else if(url == "/biobank/tube.php"){
	            	$("#tube").addClass('active');
	            	$("#manage1").css('display','block');
	            }
	            else if(url == "/biobank/input_freezer.php"){
	            	$("#input").addClass('active');
	            	/*$("#manage").css('display','block');*/
	            }
	            else if(url == "/biobank/user.php"){
	            	$("#user").addClass('active');
	            	$("#manage").css('display','block');
	            }   
	        }
	    });
	});
</script>
