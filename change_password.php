<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: login.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }
    include("connect.php"); 
?> 
<?php
	$sqlacc = "SELECT * FROM `user` WHERE user_id = ".$_SESSION["user_id"]."";
    $objacc = $db_connection->query($sqlacc);
    while(($row = $objacc->fetch_assoc()) != null){
        $user_password = $row['user_password'];
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>
	<?php include("_css.php"); ?>
	<?php include("./vendor/datatables/_css_datatable.php"); ?>
	<!-- <link href="vendor/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="all"> -->
	<link href="vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
        #example1 {
            background: url(images/3.jpg) no-repeat left top; 
            background-size: cover;
        }
        h2 {
            font-weight: 200;
            color: #555;
            margin-top: 32px;
            margin-bottom: 32px;
            text-align: center;
        }
        .btn-lg{
            padding: .4rem 2rem;
        }
    </style>
</head>
<body class="animsition">
	<div class="page-wrapper">
		<!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
		<!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
		<!-- PAGE CONTAINER-->
		<div class="page-container">
			<!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
			<!-- MAIN CONTENT-->
			<div class="main-content">
				<div class="section__content section__content--p30">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-header">
										<h4>Change Password</h4>
									</div>
									<div class="card-body">
										<div class="card-title">
                                            <h4 class="text-center title-2">Change Password</h4>
                                        </div>
                                        <hr>
                                        <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
										<form action="change_password_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                                        <div class="row">
		                                    <div class="col-md-12">
		                                        <div class="form-group">
		                                            <label class="control-label">Old Password</label>
		                                            <input type="password" class="form-control" name="password_old" value=""> 
		                                            <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>"> 
		                                            <input type="hidden" class="form-control" name="user_password" value="<?php echo $user_password; ?>"> 
		                                        </div>
		                                    </div>   
		                                </div>
		                                <div class="row">
		                                    <div class="col-md-12">
		                                        <div class="form-group">
		                                            <label class="control-label">New Password</label>
		                                            <input type="password" class="form-control" name="password_old1" value=""> 
		                                        </div>
		                                    </div>   
		                                </div>
		                                <div class="row">
		                                    <div class="col-md-12">
		                                        <div class="form-group">
		                                            <label class="control-label">New Password again</label>
		                                            <input type="password" class="form-control" name="password_old2" value="">  
		                                        </div>
		                                    </div>   
		                                </div>
		                                <hr>
		                                <button type="submit" class="btn btn-primary" name="save_pass" value="save_pass">Confirm</button>
		                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
									</div>
								</div>
							</div>
							<!-- /# column -->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
    </form>  
	<?php include("_footer.php"); ?>
	<?php include("_js.php"); ?>
	<?php include("./vendor/datatables/_js_datatable.php"); ?>
	<!-- <script src="vendor/datepicker/jquery/jquery-1.8.3.min.js"></script>
	<script src="vendor/datepicker/bootstrap/js/bootstrap.min.js"></script>-->
	<!-- <script src="vendor/datepicker/js/bootstrap-datetimepicker.js"></script>
	<script src="vendor/datepicker/js/locales/bootstrap-datetimepicker.fr.js"></script> -->
	<!-- Date Picker Plugin JavaScript -->
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
	<script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>
	<script src="vendor/bootstrap-datepicker/bootstrap-datepicker.th.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#example_tissue').DataTable();
		});

		function showResult(result,id){
	        if(result==1){
	            location.reload();
	        }
	        else if(result==2){
		        swal({
		            title: "Incorrect Old Password",
		        },
	            function(){
		                
		        });
		    }
	        else if(result==3){
		        swal({
		            title: "Incorrect New Password",
		        },
	            function(){
		                
		        });
		    }
	    }
	
		$(document).ready(function(){
		    $("#form_date").each(function() {    
		        $(this).datepicker({
		            format: 'dd/mm/yyyy',
		            todayBtn: false,
		            language: 'eng',             
		            thaiyear: false,
	                autoclose:true,   
		        }).datepicker('setDate', '');
		    });
		});
	</script>
</body>
</html>
<!-- end document-->