<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: tablet_404.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }
    include("connect.php"); 
?> 
<?php 
    $sqlfz = "SELECT * FROM `ms_freezer` where freezer_id = ".$_GET['fzm']."";
    $objQueryfz = $db_connection->query($sqlfz);
    while(($row = $objQueryfz->fetch_assoc()) != null){
        $fz_id = $row['freezer_id'];
        $fz_name = $row['freezer_name'];
    }
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

    <?php include("_css.php"); ?>
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
    <link href="vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        #iconimg{
            width: 15%;
        }

        @media (max-width: 991px) {
            #iconimg{
                width: 35%;
            }

            div.dataTables_wrapper div.dataTables_filter input {
                margin-left: 0.5em;
                display: inline-block;
                width: 65%;
            }
        }
    </style>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <div class="container">
            <div class="login-content">
                <form action="<?php echo $_SERVER['SCRIPT_NAME'];?>" method="post" name="frmSearch" >
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Move <?php if($_GET['s']=='t'){ echo "Tube"; }elseif($_GET['s']=='b'){ echo "Box"; } ?> | Freezer floor</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6"><img src="images/icon/logo-bio.png" alt="CoolAdmin" id="iconimg">
                                            <h3 class="m-t-10"><?php echo @$fz_name; ?></h3>
                                        </div>
                                        <div class="col-md-6 text-right p-t-10">
                                            <button type="button" class="btn btn-secondary mb-1" onclick="window.location.href='tablet_menu.php?fz=<?php echo $_GET['fz']; ?>'" > Back </button>
                                        </div>
                                    </div>   
                                    <br>
                                    <div class="row form-group">
                                    <?php 
                                        $sql = "SELECT * FROM `ms_freezer_floor` WHERE freezer_id = ".$_GET['fzm']." AND freezer_floor_status !=1";
                                        $objQuery = $db_connection->query($sql);
                                        $i=0;
                                        while(($row = $objQuery->fetch_assoc()) != null){
                                            $sqlrack = "SELECT * FROM `ms_rack_floor` LEFT JOIN ms_rack ON ms_rack.rack_id = ms_rack_floor.rack_id LEFT JOIN ms_freezer_floor ON ms_freezer_floor.freezer_floor_id = ms_rack.freezer_floor_id WHERE ms_rack_floor.freezer_floor_id=".$row['freezer_floor_id']." AND ms_rack.rack_status != 1 ";
                                            $objQueryrack = $db_connection->query($sqlrack);
                                            $max_rack = $objQueryrack->num_rows;
                                            if($max_rack==0){
                                                $max_rack = 10;
                                            }
                                            else{
                                                $max_rack = $max_rack*4;
                                            }
                                            $sqlbox = "SELECT * FROM `tr_freezer_add` LEFT JOIN ms_rack_floor ON ms_rack_floor.rack_floor_id = tr_freezer_add.rack_floor_id LEFT JOIN ms_rack ON ms_rack.rack_id = ms_rack_floor.rack_id where freezeradd_status=0 AND ms_rack_floor.freezer_floor_id=".$row['freezer_floor_id']."";
                                            $objQuerybox = $db_connection->query($sqlbox);
                                            $sum_num = $objQuerybox->num_rows;
                                            $i++;
                                            echo "<div class='col-md-12 text-center'>
                                                <a href='tablet_move_rack.php?id=".$_GET['id']."&s=".$_GET['s']."&fz=".$_GET['fz']."&fzm=".$_GET['fzm']."&fz_f=".$row['freezer_floor_id']."'>
                                                    <img src='images/freezer_floor_n.png' alt='Biobank'/>
                                                </a>
                                                <h3 class='text-center p-t-10 p-b-20'>".$row['freezer_floor_edit']." ";
                                                    if($sum_num<$max_rack){
                                                        echo " <span class='badge badge-success' style='color:#28a745;' >0</span>";
                                                    }
                                                    else{
                                                        echo " <span class='badge badge-danger' style='color:#dc3545;' >0</span>";
                                                    }
                                            echo "</h3>
                                            </div>";
                                        }
                                    ?> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <input type="button" class="btn btn-success m-t-10" value="Menu" onclick="window.location.href='tablet_menu.php?fz=<?php echo $_GET['fz']; ?>'" />
                            <input type="button" class="btn btn-info m-t-10 m-l-10" value="Logout" onclick="window.location.href='tablet_logout.php?fz=<?php echo $_GET['fz']; ?>'" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>
    <!--  <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.th.min.js"></script> -->
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        }
    </script>
</body>
</html>
<!-- end document-->