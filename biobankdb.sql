-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 04, 2018 at 10:36 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `biobankdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `ms_bio`
--

CREATE TABLE `ms_bio` (
  `bio_id` int(10) NOT NULL,
  `bio_barcode` char(20) NOT NULL,
  `bio_name` varchar(20) NOT NULL,
  `bio_detail` text NOT NULL,
  `bio_cuser` varchar(10) NOT NULL,
  `bio_cwhen` datetime NOT NULL,
  `bio_muser` varchar(10) NOT NULL,
  `bio_mwhen` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ms_box`
--

CREATE TABLE `ms_box` (
  `box_id` int(10) NOT NULL,
  `box_barcode` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT '2(box)0000(num)',
  `box_name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `box_date` varchar(10) CHARACTER SET utf8 NOT NULL,
  `box_time` varchar(10) CHARACTER SET utf8 NOT NULL,
  `box_row` int(10) NOT NULL,
  `box_colum` int(10) NOT NULL,
  `box_cuser` int(10) NOT NULL,
  `box_cwhen` datetime NOT NULL,
  `box_muser` int(10) NOT NULL,
  `box_mwhen` datetime NOT NULL,
  `box_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_freezer`
--

CREATE TABLE `ms_freezer` (
  `freezer_id` int(10) NOT NULL,
  `freezer_name` varchar(100) NOT NULL,
  `freezer_cuser` int(10) NOT NULL,
  `freezer_cwhen` datetime NOT NULL,
  `freezer_muser` int(10) NOT NULL,
  `freezer_mwhen` datetime NOT NULL,
  `freezer_status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ms_freezer_floor`
--

CREATE TABLE `ms_freezer_floor` (
  `freezer_floor_id` int(10) NOT NULL,
  `freezer_floor_name` varchar(30) NOT NULL,
  `freezer_floor_edit` varchar(100) NOT NULL,
  `freezer_id` int(10) NOT NULL,
  `freezer_floor_cuser` int(10) NOT NULL,
  `freezer_floor_cwhen` datetime NOT NULL,
  `freezer_floor_muser` int(10) NOT NULL,
  `freezer_floor_mwhen` datetime NOT NULL,
  `freezer_floor_status` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ms_log`
--

CREATE TABLE `ms_log` (
  `log_id` int(11) NOT NULL,
  `log_item` int(11) NOT NULL COMMENT '1=tube,2=box',
  `log_status` varchar(11) CHARACTER SET utf8 NOT NULL COMMENT '1=in,2=out,3=move',
  `log_user` int(11) NOT NULL,
  `log_datetime` datetime NOT NULL,
  `log_cuser` int(11) NOT NULL,
  `log_cwhen` datetime NOT NULL,
  `log_muser` int(11) NOT NULL,
  `log_mwhen` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_rack`
--

CREATE TABLE `ms_rack` (
  `rack_id` int(10) NOT NULL,
  `rack_name` varchar(20) NOT NULL,
  `rack_edit` varchar(100) NOT NULL,
  `freezer_floor_id` int(10) NOT NULL,
  `freezer_id` int(10) NOT NULL,
  `rack_cuser` int(10) NOT NULL,
  `rack_cwhen` datetime NOT NULL,
  `rack_muser` int(10) NOT NULL,
  `rack_mwhen` datetime NOT NULL,
  `rack_status` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ms_rack_floor`
--

CREATE TABLE `ms_rack_floor` (
  `rack_floor_id` int(10) NOT NULL,
  `rack_floor_name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `rack_floor_edit` varchar(100) CHARACTER SET utf8 NOT NULL,
  `rack_id` int(10) NOT NULL,
  `freezer_floor_id` int(10) NOT NULL,
  `freezer_id` int(10) NOT NULL,
  `rack_floor_cuser` int(10) NOT NULL,
  `rack_floor_cwhen` datetime NOT NULL,
  `rack_floor_muser` int(10) NOT NULL,
  `rack_floor_mwhen` datetime NOT NULL,
  `rack_floor_status` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_biod`
--

CREATE TABLE `tr_biod` (
  `biod_id` int(10) NOT NULL,
  `biod_name` varchar(10) NOT NULL,
  `rack_id` int(10) NOT NULL,
  `floor_id` int(10) NOT NULL,
  `cs_id` int(10) NOT NULL,
  `biod_cuser` varchar(10) NOT NULL,
  `biod_cwhen` datetime NOT NULL,
  `biod_muser` varchar(10) NOT NULL,
  `biod_mwhen` datetime NOT NULL,
  `biod_status` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tr_box_add`
--

CREATE TABLE `tr_box_add` (
  `boxadd_id` int(11) NOT NULL,
  `sample_sid` varchar(20) CHARACTER SET utf8 NOT NULL,
  `boxadd_well` varchar(11) CHARACTER SET utf8 NOT NULL,
  `box_id` int(11) NOT NULL,
  `boxadd_cuser` int(11) NOT NULL,
  `boxadd_cwhen` datetime NOT NULL,
  `boxadd_muser` int(11) NOT NULL,
  `boxadd_mwhen` datetime NOT NULL,
  `boxadd_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_freezer_add`
--

CREATE TABLE `tr_freezer_add` (
  `freezeradd_id` int(10) NOT NULL,
  `box_barcode` varchar(100) CHARACTER SET utf8 NOT NULL,
  `freezer_id` int(100) NOT NULL,
  `freezer_floor_id` int(100) NOT NULL,
  `rack_id` int(100) NOT NULL,
  `rack_floor_id` int(10) NOT NULL,
  `freezeradd_cuser` int(10) NOT NULL,
  `freezeradd_cwhen` datetime NOT NULL,
  `freezeradd_muser` int(10) NOT NULL,
  `freezeradd_mwhen` datetime NOT NULL,
  `freezeradd_status` int(10) NOT NULL COMMENT '0=in,1=out'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_report`
--

CREATE TABLE `tr_report` (
  `report_id` int(11) NOT NULL,
  `sample_sid` varchar(11) CHARACTER SET utf8 NOT NULL,
  `report_file` varchar(200) CHARACTER SET utf8 NOT NULL,
  `report_filename` varchar(200) CHARACTER SET utf8 NOT NULL,
  `report_cuser` int(11) NOT NULL,
  `report_cwhen` datetime NOT NULL,
  `report_muser` int(11) NOT NULL,
  `report_mwhen` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_sample`
--

CREATE TABLE `tr_sample` (
  `sample_id` int(10) NOT NULL,
  `sample_sid` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sample_date` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sample_time` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sample_prefix` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sample_fname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sample_lname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sample_hn` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sample_hbd` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sample_age` int(100) NOT NULL,
  `sample_gender` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sample_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sample_type_id` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sample_pjowner` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sample_consent` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sample_pjname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sample_receive` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sample_cuser` int(5) NOT NULL,
  `sample_cwhen` datetime NOT NULL,
  `sample_muser` int(5) NOT NULL,
  `sample_mwhen` datetime NOT NULL,
  `sample_status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_sample_dokeep`
--

CREATE TABLE `tr_sample_dokeep` (
  `dokeep_id` int(11) NOT NULL,
  `dokeep_status` int(11) NOT NULL COMMENT '1=do,2=keep,3=doandkeep',
  `sample_id` int(11) NOT NULL,
  `dokeep_cuser` int(11) NOT NULL,
  `dokeep_cwhen` datetime NOT NULL,
  `dokeep_muser` int(11) NOT NULL,
  `dokeep_mwhen` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_sample_tube`
--

CREATE TABLE `tr_sample_tube` (
  `tube_id` int(11) NOT NULL,
  `sample_sid` int(11) NOT NULL,
  `sample_sid_tube` varchar(20) CHARACTER SET utf8 NOT NULL,
  `tube_barcode` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '1(tube)0000(num)',
  `tube_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `tube_cuser` int(11) NOT NULL,
  `tube_cwhen` datetime NOT NULL,
  `tube_muser` int(11) NOT NULL,
  `tube_mwhen` datetime NOT NULL,
  `tube_status` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_stock`
--

CREATE TABLE `tr_stock` (
  `stock_id` int(11) NOT NULL,
  `rack_floor_id` int(11) NOT NULL,
  `box_barcode` int(11) NOT NULL,
  `stock_cuser` int(11) NOT NULL,
  `stock_cwhen` datetime NOT NULL,
  `stock_muser` int(11) NOT NULL,
  `stock_mwhen` datetime NOT NULL,
  `stock_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(10) NOT NULL,
  `user_idcard` varchar(13) NOT NULL,
  `user_prefix` varchar(100) NOT NULL,
  `user_gender` varchar(100) NOT NULL,
  `user_hbd` varchar(20) NOT NULL,
  `user_age` varchar(100) NOT NULL,
  `user_fname` varchar(100) NOT NULL,
  `user_lname` varchar(100) NOT NULL,
  `user_username` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_phone` varchar(100) NOT NULL,
  `user_image` varchar(100) NOT NULL,
  `user_imagename` varchar(100) NOT NULL,
  `user_type` int(20) NOT NULL COMMENT '0=admin,1=พนักงาน,2=หัวหน้าพนักงาน',
  `user_cuser` int(10) NOT NULL,
  `user_cwhen` datetime NOT NULL,
  `user_muser` int(10) NOT NULL,
  `user_mwhen` datetime NOT NULL,
  `user_status` int(20) NOT NULL COMMENT '1=add,2=delete'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_idcard`, `user_prefix`, `user_gender`, `user_hbd`, `user_age`, `user_fname`, `user_lname`, `user_username`, `user_password`, `user_email`, `user_phone`, `user_image`, `user_imagename`, `user_type`, `user_cuser`, `user_cwhen`, `user_muser`, `user_mwhen`, `user_status`) VALUES
(1, '12345', 'Mr.', 'Male', '01-01-2018', '0', 'admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@gmail.com', '0000000000', 'BnmhBb20180827181546.png', 'avatar.png', 0, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1),
(2, '54321', '', '', '27-08-2018', '22', 'sa', 'ra', 'user', '01cfcd4f6b8770febfb40cb906715822', 'sd@gmail.cpm', '0899887421', 'zWnbFv20180830152401.png', 'freezer.png', 1, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1),
(3, '123321', 'Miss.', 'Male', '27-10-2018', '27', 'ลองแก้', 'ดูแก้', 'usersd', '827ccb0eea8a706c4c34a16891f84e7b', 'testtest@gmail.com', '00112233', 'TvJugv20181024150347.jpg', 'logo-biobank.jpg', 2, 0, '0000-00-00 00:00:00', 1, '2018-10-24 15:27:34', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ms_bio`
--
ALTER TABLE `ms_bio`
  ADD PRIMARY KEY (`bio_id`);

--
-- Indexes for table `ms_box`
--
ALTER TABLE `ms_box`
  ADD PRIMARY KEY (`box_id`);

--
-- Indexes for table `ms_freezer`
--
ALTER TABLE `ms_freezer`
  ADD PRIMARY KEY (`freezer_id`);

--
-- Indexes for table `ms_freezer_floor`
--
ALTER TABLE `ms_freezer_floor`
  ADD PRIMARY KEY (`freezer_floor_id`);

--
-- Indexes for table `ms_log`
--
ALTER TABLE `ms_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `ms_rack`
--
ALTER TABLE `ms_rack`
  ADD PRIMARY KEY (`rack_id`);

--
-- Indexes for table `ms_rack_floor`
--
ALTER TABLE `ms_rack_floor`
  ADD PRIMARY KEY (`rack_floor_id`);

--
-- Indexes for table `tr_biod`
--
ALTER TABLE `tr_biod`
  ADD PRIMARY KEY (`biod_id`);

--
-- Indexes for table `tr_box_add`
--
ALTER TABLE `tr_box_add`
  ADD PRIMARY KEY (`boxadd_id`);

--
-- Indexes for table `tr_freezer_add`
--
ALTER TABLE `tr_freezer_add`
  ADD PRIMARY KEY (`freezeradd_id`);

--
-- Indexes for table `tr_report`
--
ALTER TABLE `tr_report`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `tr_sample`
--
ALTER TABLE `tr_sample`
  ADD PRIMARY KEY (`sample_id`);

--
-- Indexes for table `tr_sample_dokeep`
--
ALTER TABLE `tr_sample_dokeep`
  ADD PRIMARY KEY (`dokeep_id`);

--
-- Indexes for table `tr_sample_tube`
--
ALTER TABLE `tr_sample_tube`
  ADD PRIMARY KEY (`tube_id`);

--
-- Indexes for table `tr_stock`
--
ALTER TABLE `tr_stock`
  ADD PRIMARY KEY (`stock_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ms_bio`
--
ALTER TABLE `ms_bio`
  MODIFY `bio_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_box`
--
ALTER TABLE `ms_box`
  MODIFY `box_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_freezer`
--
ALTER TABLE `ms_freezer`
  MODIFY `freezer_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_freezer_floor`
--
ALTER TABLE `ms_freezer_floor`
  MODIFY `freezer_floor_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_log`
--
ALTER TABLE `ms_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_rack`
--
ALTER TABLE `ms_rack`
  MODIFY `rack_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_rack_floor`
--
ALTER TABLE `ms_rack_floor`
  MODIFY `rack_floor_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_biod`
--
ALTER TABLE `tr_biod`
  MODIFY `biod_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_box_add`
--
ALTER TABLE `tr_box_add`
  MODIFY `boxadd_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_freezer_add`
--
ALTER TABLE `tr_freezer_add`
  MODIFY `freezeradd_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_report`
--
ALTER TABLE `tr_report`
  MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_sample`
--
ALTER TABLE `tr_sample`
  MODIFY `sample_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_sample_dokeep`
--
ALTER TABLE `tr_sample_dokeep`
  MODIFY `dokeep_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_sample_tube`
--
ALTER TABLE `tr_sample_tube`
  MODIFY `tube_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_stock`
--
ALTER TABLE `tr_stock`
  MODIFY `stock_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
