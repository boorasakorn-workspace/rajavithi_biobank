<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: login.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }
    include("connect.php"); 
?> 
<?php 
    $sqldna = "SELECT * FROM `tr_sample` WHERE sample_type = 'DNA' AND sample_status !=1";
    $objdna = $db_connection->query($sqldna);
    $row_dna = $objdna->num_rows;
    /////////////
    $sqlrna = "SELECT * FROM `tr_sample` WHERE sample_type = 'RNA' AND sample_status !=1";
    $objrna = $db_connection->query($sqlrna);
    $row_rna = $objrna->num_rows;
    ////////////
    $sqlserum = "SELECT * FROM `tr_sample` WHERE sample_type = 'Serum' AND sample_status !=1";
    $objserum = $db_connection->query($sqlserum);
    $row_serum = $objserum->num_rows;
    //////////////
    $sqltissue = "SELECT * FROM `tr_sample` WHERE sample_type = 'Tissue' AND sample_status !=1";
    $objtissue = $db_connection->query($sqltissue);
    $row_tissue = $objtissue->num_rows;
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

    <?php include("_css.php"); ?>
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="au-card m-b-30">
                                    <div class="au-card-inner">
                                        <h3 class="title-2 m-b-40">Bio Chart </h3>
                                        <canvas id="myChart"></canvas>
                                    </div>
                                </div>
                            </div>                            
                            <div class="col-lg-6">
                                <div class="au-card m-b-30">
                                    <div class="au-card-inner">
                                        <h3 class="title-2 m-b-40">Bio Chart</h3>
                                        <canvas id="pieaChart"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Sample</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6"><h4>Sample</h4></div>
                                        </div>
                                        <br>
                                        <table id="example" class="table table-responsive table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th width="1%" style="white-space: nowrap;">N</th>
                                                    <th width="1%" style="white-space: nowrap; min-width: 100px;">LN</th>  
                                                    <th width="1%" style="white-space: nowrap; min-width: 100px;">LN Date</th>
                                                    <th width="1%" style="white-space: nowrap;">LN Time</th>
                                                    <th width="1%" style="white-space: nowrap; min-width: 100px;">Name</th>
                                                    <th width="1%" style="white-space: nowrap; min-width: 100px;">HN</th>
                                                    <th width="1%" style="white-space: nowrap;">Birthday</th>
                                                    <th width="1%" style="white-space: nowrap;">Age</th>
                                                    <th width="1%" style="white-space: nowrap;">Gender</th>
                                                    <th width="1%" style="white-space: nowrap;">Type of speciman</th>
                                                    <th width="1%" style="white-space: nowrap;">Project owner</th>
                                                    <th width="1%" style="white-space: nowrap;">Consent form</th>
                                                    <th width="1%" style="white-space: nowrap;">Project name</th>
                                                    <th width="1%" style="white-space: nowrap;">Receive</th>
                                                    <th width="1%" style="white-space: nowrap;">Status</th>
                                                    <th width="1%" style="white-space: nowrap; text-align: left; min-width: 230px;">Manage</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div> 
        </div>
        <?php include("_footer.php"); ?>
    </div>
    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>
</body>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });

    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["DNA", "RNA", "Serum/plasma", "Tissue"],
            datasets: [{
                label: '# of Votes',
                data: [<?php echo $row_dna; ?>,<?php echo $row_rna; ?>,<?php echo $row_serum; ?>,<?php echo $row_tissue; ?>],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var ctxP = document.getElementById("pieaChart").getContext('2d');
    var myPieChart = new Chart(ctxP, {
        type: 'pie',
        data: {
            labels: ["DNA", "RNA", "Serum/plasma", "Tissue"],
            datasets: [
                {
                    data: [<?php echo $row_dna; ?>,<?php echo $row_rna; ?>,<?php echo $row_serum; ?>,<?php echo $row_tissue; ?>],
                    backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", ],
                    hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5"]
                }
            ]
        },
        options: {
            responsive: true
        }
    });
</script>
</html>
<!-- end document-->