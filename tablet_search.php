<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: tablet_404.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }
    include("connect.php"); 
?> 
<?php
    ini_set('display_errors', 1);
    error_reporting(~0);
    $sample_id = null; 
    $box_barcode = null; 
    $box_name = null;
    $sample_prefix = null;
    $sample_fname = null;
    $sample_lname = null;
    $sample_hn = null;
    $sample_age = null;
    $sample_gender = null;
    $sample_pjowner = null;
    $sample_consent = null;
    $sample_pjname = null;
    $sample_type = null;

    if(isset($_POST["sample_id"],$_POST["box_barcode"],$_POST["box_name"],$_POST["sample_prefix"],$_POST["sample_fname"],$_POST["sample_lname"],$_POST["sample_hn"],$_POST["sample_age"],$_POST["sample_gender"],$_POST["sample_pjowner"],$_POST["sample_consent"],$_POST["sample_pjname"],$_POST["sample_type"])){
        $sample_id = $_POST["sample_id"];
        $box_barcode = $_POST["box_barcode"];
        $box_name = $_POST["box_name"];
        $sample_prefix = $_POST["sample_prefix"];
        $sample_fname = $_POST["sample_fname"];
        $sample_lname = $_POST["sample_lname"];
        $sample_hn = $_POST["sample_hn"];
        $sample_age = $_POST["sample_age"];
        $sample_gender = $_POST["sample_gender"];
        $sample_pjowner = $_POST["sample_pjowner"];
        $sample_consent = $_POST["sample_consent"];
        $sample_pjname = $_POST["sample_pjname"];
        $sample_type = $_POST["sample_type"];
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

    <?php include("_css.php"); ?>
    <?php include("connect.php"); ?> 
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
    <link href="vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        #iconimg{
            width: 15%;
        }

        @media (max-width: 991px) {
            #iconimg{
                width: 35%;
            }

            div.dataTables_wrapper div.dataTables_filter input {
                margin-left: 0.5em;
                display: inline-block;
                width: 65%;
            }
        }
    </style>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <div class="container">
            <div class="login-content">
                <form action="<?php echo $_SERVER['SCRIPT_NAME'];?>?fz=<?php echo $_GET['fz']; ?>" method="post" name="frmSearch">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Search</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6"><img src="images/icon/logo-bio.png" alt="CoolAdmin" id="iconimg"></div>
                                        <div class="col-md-6 text-right"><button type="button" class="btn btn-warning mb-1" data-toggle="modal" data-target="#addmodal"> Search </button></div>
                                    </div>
                                    <br>
                                    <table id="example" class="table table-responsive table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="1%" style="white-space: nowrap;">N</th>
                                                <th width="1%" style="white-space: nowrap;">LN</th>
                                                <th width="1%" style="white-space: nowrap;">Prefix</th>
                                                <th width="1%" style="white-space: nowrap;">Fristname</th>
                                                <th width="1%" style="white-space: nowrap;">Lastname</th>
                                                <th width="1%" style="white-space: nowrap;">HN</th>
                                                <th width="1%" style="white-space: nowrap;">Age</th>
                                                <th width="1%" style="white-space: nowrap;">Gender</th>
                                                <th width="1%" style="white-space: nowrap;">Type of speciman</th>
                                                <th width="1%" style="white-space: nowrap;">Project owner</th>
                                                <th width="1%" style="white-space: nowrap;">Consent form</th>
                                                <th width="1%" style="white-space: nowrap;">Project name</th>
                                                <th width="1%" style="white-space: nowrap;">Tube Barcode</th>
                                                <th width="1%" style="white-space: nowrap; min-width: 200px;">Freezer</th>
                                                <th width="1%" style="white-space: nowrap;">Freezer floor</th>
                                                <th width="1%" style="white-space: nowrap;">Rack</th>
                                                <th width="1%" style="white-space: nowrap;">Rack floor</th>
                                                <th width="1%" style="white-space: nowrap;">Box Barcode</th>
                                                <th width="1%" style="white-space: nowrap;">Box Name</th>
                                                <th width="1%" style="white-space: nowrap;">Well</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                            $sql = "SELECT * , tr_sample.sample_sid as s FROM `tr_sample` LEFT JOIN `tr_sample_tube` on tr_sample_tube.sample_sid = tr_sample.sample_id LEFT JOIN `tr_box_add` on tr_box_add.sample_sid = tr_sample_tube.tube_barcode LEFT JOIN `ms_box` on ms_box.box_id = tr_box_add.box_id LEFT JOIN `tr_freezer_add` on tr_freezer_add.box_barcode = ms_box.box_barcode LEFT JOIN `ms_rack_floor` on ms_rack_floor.rack_floor_id = tr_freezer_add.rack_floor_id LEFT JOIN `ms_rack` on ms_rack.rack_id = tr_freezer_add.rack_id LEFT JOIN `ms_freezer_floor` on ms_freezer_floor.freezer_floor_id = tr_freezer_add.freezer_floor_id LEFT JOIN `ms_freezer` on ms_freezer.freezer_id = tr_freezer_add.freezer_id WHERE tr_freezer_add.freezeradd_status = 0 AND tr_box_add.boxadd_status = 0 AND tr_sample.sample_type LIKE '%".$sample_type."%' AND tr_sample.sample_sid LIKE '%".$sample_id."%' AND ms_box.box_barcode LIKE '%".$box_barcode."%' AND ms_box.box_name LIKE '%".$box_name."%' AND tr_sample.sample_prefix LIKE '%".$sample_prefix."%' AND tr_sample.sample_fname LIKE '%".$sample_fname."%' AND tr_sample.sample_lname LIKE '%".$sample_lname."%' AND tr_sample.sample_hn LIKE '%".$sample_hn."%' AND tr_sample.sample_age LIKE '%".$sample_age."%' AND tr_sample.sample_gender LIKE '%".$sample_gender."%' AND tr_sample.sample_pjowner LIKE '%".$sample_pjowner."%' AND tr_sample.sample_consent LIKE '%".$sample_consent."%' AND tr_sample.sample_pjname LIKE '%".$sample_pjname."%'";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                            while(($row = $objQuery->fetch_assoc()) != null){
                                                $i++;
                                                echo "<tr>
                                                    <td>".$i."</td>
                                                    <td>".$row['s']."</td>
                                                    <td>".$row['sample_prefix']."</td>
                                                    <td>".$row['sample_fname']."</td>
                                                    <td>".$row['sample_lname']."</td>
                                                    <td>".$row['sample_hn']."</td>
                                                    <td>".$row['sample_age']."</td>
                                                    <td>".$row['sample_gender']."</td>
                                                    <td>".$row['sample_type']."</td>
                                                    <td>".$row['sample_pjowner']."</td>
                                                    <td>".$row['sample_consent']."</td>
                                                    <td>".$row['sample_pjname']."</td>
                                                    <td>".$row['sample_sid']."</td>
                                                    <td>".$row['freezer_name']."</td>
                                                    <td>".$row['freezer_floor_edit']."</td>
                                                    <td>".$row['rack_edit']."</td>
                                                    <td>".$row['rack_floor_edit']."</td>
                                                    <td>".$row['box_barcode']."</td>
                                                    <td>".$row['box_name']."</td>
                                                    <td>".$row['boxadd_well']."</td>
                                                </tr>";
                                            }
                                            /*<button class='item p-l-10' data-toggle='modal' data-target='#staticModal' data-toggle='tooltip' title='Delete'  style='color:#666;'>
                                                                <i class='zmdi zmdi-delete'></i>
                                                            </button>*/
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- <div class="row">
                            <div class="col-md-6">
                                <button type="button" class="btn btn-success mb-1 m-t-10" style="width: 100%; font-size: 50px;" onclick="window.location.href='tablet_search.php?fz=<?php // echo $_GET['fz']; ?>'"> <img src="images/search.png" alt="CoolAdmin" id="iconimg" > Search</button>
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-danger mb-1 m-t-10" style="width: 100%; font-size: 50px;" onclick="window.location.href='tablet_freezer_floor.php?fz=<?php // echo $_GET['fz']; ?>'"> <img src="images/in_out.png" alt="CoolAdmin" id="iconimg" > Input/Output</button>
                            </div>
                        </div> 
                        <br>-->
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <input type="button" class="btn btn-success m-t-10" value="Menu" onclick="window.location.href='tablet_menu.php?fz=<?php echo $_GET['fz']; ?>'" />
                            <input type="button" class="btn btn-info m-t-10 m-l-10" value="Logout" onclick="window.location.href='tablet_logout.php?fz=<?php echo $_GET['fz']; ?>'" />
                        </div>
                    </div>
                </div>
            </div>
        <!-- modal scroll -->
        <div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Search</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>                                
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">LN</label>
                                    <input type="text" class="form-control" name="sample_id" id="sample_id" value="<?php echo @$sample_id; ?>">
                                </div>
                            </div>               
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">ID Box</label>
                                    <input type="text" class="form-control" id="box_barcode" name="box_barcode" value="<?php echo @$box_barcode; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Box Name</label>
                                    <input type="text" class="form-control" id="box_name" name="box_name" value="<?php echo @$box_name; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Prefix</label>
                                    <select class="form-control" id="sample_prefix" name="sample_prefix" >
                                        <option value="">Please select</option>
                                        <option value="Mr." <?php if(@$sample_prefix=='Mr.'){ echo "selected"; } ?>>Mr.</option>
                                        <option value="Mrs." <?php if(@$sample_prefix=='Mrs.'){ echo "selected"; } ?>>Mrs.</option>
                                        <option value="Miss." <?php if(@$sample_prefix=='Miss.'){ echo "selected"; } ?>>Miss.</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group"> 
                                    <label class="control-label">Fristname</label>
                                    <input type="text" class="form-control" id="sample_fname" name="sample_fname" value="<?php echo @$sample_fname; ?>">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group"> 
                                    <label class="control-label">Lastname</label>
                                    <input type="text" class="form-control" id="sample_lname" name="sample_lname" value="<?php echo @$sample_lname; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"> 
                                    <label class="control-label">HN</label>
                                    <input type="text" class="form-control" id="sample_hn" name="sample_hn" value="<?php echo @$sample_hn; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group"> 
                                    <label class="control-label">Age</label>
                                    <input type="number" class="form-control" id="sample_age" name="sample_age" value="<?php echo @$sample_age; ?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Gender</label>
                                <div class="form-group">
                                    <div class="form-check-inline form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" id="sample_gender1" name="sample_gender"  value="Male" checked <?php if(@$sample_gender=='Male'){ echo "checked"; } ?>> Male
                                        </label>
                                        <label class="form-check-label p-l-10">
                                            <input class="form-check-input" type="radio" id="sample_gender2" name="sample_gender" value="Female" <?php if(@$sample_gender=='Female'){ echo "checked"; } ?>> Female
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Type of specimen</label>
                                    <select class="form-control" name="sample_type" id="sample_type">
                                        <option value="">Please select</option>
                                        <option value="DNA" <?php if(@$sample_type=='DNA'){ echo "selected"; } ?>>DNA</option>
                                        <option value="RNA" <?php if(@$sample_type=='RNA'){ echo "selected"; } ?>>RNA</option>
                                        <option value="Serum" <?php if(@$sample_type=='Serum'){ echo "selected"; } ?>>Serum</option>
                                        <option value="Tissue" <?php if(@$sample_type=='Tissue'){ echo "selected"; } ?>>Tissue</option>
                                    </select>
                                </div>
                            </div>               
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Project owner</label>
                                    <input type="text" class="form-control" id="sample_pjowner" name="sample_pjowner" value="<?php echo @$sample_pjowner; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Consent form</label>
                                    <input type="text" class="form-control" id="sample_consent" name="sample_consent" value="<?php echo @$sample_consent; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Project name</label>
                                    <input type="text" class="form-control" id="sample_pjname" name="sample_pjname" value="<?php echo @$sample_pjname; ?>">
                                </div>
                            </div>               
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="submit" value="Search">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->
    </div>
    
    </form>

    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>
    <!--  <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.th.min.js"></script> -->

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        }

        var sample_age = document.getElementById('sample_age');
        sample_age.onkeydown = function(e) {
            if(!((e.keyCode > 95 && e.keyCode < 106)
              || (e.keyCode > 47 && e.keyCode < 58) 
              || e.keyCode == 8)) {
                return false;
            }
        }
    </script>
</body>
</html>
<!-- end document-->