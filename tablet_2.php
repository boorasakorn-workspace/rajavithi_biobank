<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>Login</title>

    <?php include("_css.php"); ?>
    
    <style type="text/css">
        #example1 {
            background: url(images/3.jpg) no-repeat left top; 
            background-size: cover;
        }
        h2 {
            font-weight: 200;
            color: #555;
            margin-top: 32px;
            margin-bottom: 32px;
            text-align: center;
        }
        .btn-lg{
            padding: .4rem 2rem;
        }
    </style>
</head>

<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge4" id="example1">
           <!-- <img src="images/1.jpg" alt="CoolAdmin"><img src="images/icon/logo.png" alt="CoolAdmin"><img src="images/icon/logo.png" alt="CoolAdmin"> -->
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">
                                <img src="images/icon/logo-bio.png" alt="Biobank" style="width: 150px; height: 150px;" />
                                <!-- <img src="images/icon/logo.png" alt="CoolAdmin"> -->
                            </a>
                        </div>
                        <div class="login-form">
                            <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
                            <form action="tablet_check.php" method="post" enctype="multipart/form-data" target="iframe_target">
                                <div class="row form-group">
                                    <div class="col col-md-12">
                                        <div class="input-group">
                                            <input type="text" id="tablet_id" name="tablet_id" placeholder="Scan" class="form-control">
                                            <input class="form-control" type="hidden" id="fz" name="fz" value="1">
                                            <div class="input-group-btn">
                                                <button type="submit" class="btn btn-primary" name="login" value="login">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include("_js.php"); ?>
</body>
<script>
    $( document ).ready(function() {
        $('#tablet_id').focus()
    });

    function showResult(result,id){
        if(result==1){
            window.location.href = "tablet_menu.php?fz=2";
        }
        else if(result==0){
            swal({
                title: "Incorrect Scan",
            },
            function(){
                
            });
        }
    }
</script>
</html>
<!-- end document-->