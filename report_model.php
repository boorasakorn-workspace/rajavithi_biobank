<?php 
	include("connect.php");
	date_default_timezone_set('Asia/Bangkok');
	$ndate = date("Ymd");
	$ntime = date("His");
	///////////////////////
	if($_POST['save_report']=='save_report'){
		$date = date("Y-m-d H:i:s");
		$ndate = date("Ymd");
		$ntime = date("His");
	    if($_POST['report_ln']!=''&&$_FILES["report_file"]["name"]!=''){
			if($_FILES["report_file"]["name"]!=''){
		        $rdname = makeRandomString();
		        $extension = strrchr($_FILES["report_file"]["name"] , '.' );
			    $report_file = $rdname.$ndate.$ntime.$extension;
			    $report_filename = $_FILES["report_file"]["name"];
			   	move_uploaded_file($_FILES["report_file"]["tmp_name"],"./file/".$report_file);
		    }
		    $str = "INSERT INTO `tr_report`(`sample_sid`, `report_file`, `report_filename`, `report_cuser`, `report_cwhen`, `report_muser`, `report_mwhen`) VALUES ('".$_POST['report_ln']."','".$report_file."','".$report_filename."',".$_POST['user_id'].",'".$date."',0,'0000-00-00 00:00:00')";
	    	$obj = $db_connection->query($str);
	    }
	   	else{
	   		echo "<script>window.top.window.showResult('0');</script>";
	   		exit();
	   	}
	    //////////////////////
        if($obj){
        	echo "<script>window.top.window.showResult('1');</script>";
		}
		else{
			echo "<script>window.top.window.showResult('0');</script>";
		}	
	}
	elseif($_POST['edit_sample']=='edit_sample'){
		$date = date("Y-m-d H:i:s");
	    $str = "UPDATE `tr_sample` SET `sample_sid`='".$_POST['sample_sid_edit']."',`sample_date`='".$_POST['sample_date_edit']."',`sample_time`='".$_POST['sample_time_edit']."',`sample_prefix`='".$_POST['sample_prefix_edit']."',`sample_fname`='".$_POST['sample_fname_edit']."',`sample_lname`='".$_POST['sample_lname_edit']."',`sample_hn`='".$_POST['sample_hn_edit']."',`sample_age`='".$_POST['sample_age_edit']."',`sample_gender`='".$_POST['sample_gender_edit']."',`sample_type`='".$_POST['sample_type_edit']."',`sample_pjowner`='".$_POST['sample_pjowner_edit']."',`sample_consent`='".$_POST['sample_consent_edit']."',`sample_pjname`='".$_POST['sample_pjname_edit']."',`sample_receive`='".$_POST['sample_receive_edit']."',`sample_muser`=".$_POST['user_id'].",`sample_mwhen`='".$date."',`sample_status`=0 WHERE `sample_id` = ".$_POST['sample_id_edit']."";
	    $obj = $db_connection->query($str);
	    //////////////////////
        if($obj){
        	echo "<script>window.top.window.showResult('1');</script>";
		}
		else{
			echo "<script>window.top.window.showResult('0');</script>";
		}	
	}
	elseif($_GET['dow']=='report'){
        $file = "./file/".$_GET['file'];
        $filename = $_GET['filename'];
        $file_extension = strtolower(substr(strrchr($filename,"."),1));
        switch ($file_extension) {
            case "pdf": $ctype="application/pdf"; break;
            case "doc": $ctype="application/msword"; break;
            case "docx": $ctype="application/msword"; break;
            case "xls": $ctype="application/vnd.ms-excel"; break;
            case "xlsx": $ctype="application/vnd.ms-excel"; break;
            case "jpg": $ctype="image/jpg"; break;
            case "png": $ctype="image/png"; break;
            case "jpe": case "jpeg":
            default: $ctype="application/force-download";
        }

		if ($file) {
		    header('Content-Description: File Transfer');
		   	header("Content-Type: $ctype");
		    header('Content-Disposition: attachment; filename='.$filename);
		    header('Content-Transfer-Encoding: binary');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    ob_clean();
		    flush();
		    readfile($file);
		    exit;
		}
    }
	else{
		echo "error";
	}
?>

<?php
	function makeRandomString($max=6) {
	    $i = 0; //Reset the counter.
	    $possible_keys = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    $keys_length = strlen($possible_keys);
	    $str = ""; //Let's declare the string, to add later.
	    while($i<$max) {
	        $rand = mt_rand(1,$keys_length-1);
	        $str.= $possible_keys[$rand];
	        $i++;
	    }
	    return $str;
	}
?>