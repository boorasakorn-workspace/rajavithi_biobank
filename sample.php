<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: login.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }

    include("connect.php"); 
?> 
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

	<?php include("_css.php"); ?>
	<?php include("./vendor/datatables/_css_datatable.php"); ?>
    <link href="vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        h2{
            color: #555;
            margin-top: 15px;
            text-align: center;
        }
        .btn-lg{
            margin-top: 15px;
            padding: .3rem 2rem;
        }
        .lead{
             margin-top: 15px;
        }
        .sweet-alert{
            background-color: #f2f2f2;
        }
        #red{
            color: red;
        }
    </style>
</head>
<body class="animsition">
	<div class="page-wrapper">
		<!-- HEADER MOBILE-->
        <?php  include("_header_mobile.php"); ?>
        <!-- END HEADER MOBILE-->

		<!-- MENU SIDEBAR-->
        <?php  include("_menu.php"); ?>
        <!-- END MENU SIDEBAR-->

    <!-- <form action="sample_model.php" method="post" enctype="multipart/form-data" target="iframe_target"> -->
		<!-- PAGE CONTAINER-->
		<div class="page-container">
			<!-- HEADER DESKTOP-->
            <?php  include("_header_desktop.php"); ?>
            <!-- HEADER DESKTOP-->

			<!-- MAIN CONTENT-->
			<div class="main-content">
				<div class="section__content section__content--p30">
					<div class="container-fluid">
						<div class="row">
                            <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff; display: none;"></iframe>
							<div class="col-md-12">
								<div class="card">
									<div class="card-header">
										<h4>Sample</h4>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-md-6"><h4>Sample</h4></div>
											<div class="col-md-6 text-right"><button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#addmodal"> New Sample </button></div>
										</div>
										<br>
										<table id="example" class="table table-responsive table-striped table-bordered" style="width:100%">
											<thead>
												<tr>
												    <th width="1%" style="white-space: nowrap;">N</th>
                                                    <th width="1%" style="white-space: nowrap; text-align: left; min-width: 180px;">Manage</th>
												    <th width="1%" style="white-space: nowrap; min-width: 100px;">LN</th>  
                                                    <th width="1%" style="white-space: nowrap; min-width: 100px;">IN Date</th>
                                                    <th width="1%" style="white-space: nowrap;">IN Time</th>
												    <th width="1%" style="white-space: nowrap; min-width: 100px;">Name</th>
												    <th width="1%" style="white-space: nowrap; min-width: 100px;">HN</th>
                                                    <th width="1%" style="white-space: nowrap;">Birthday</th>
                                                    <th width="1%" style="white-space: nowrap;">Age</th>
												    <th width="1%" style="white-space: nowrap;">Gender</th>
                                                    <th width="1%" style="white-space: nowrap;">Type of speciman</th>
                                                    <th width="1%" style="white-space: nowrap;">Project owner</th>
                                                    <th width="1%" style="white-space: nowrap;">Consent form</th>
                                                    <th width="1%" style="white-space: nowrap;">Project name</th>
                                                    <th width="1%" style="white-space: nowrap;">Receive</th>
                                                    <th width="1%" style="white-space: nowrap;">Status</th>
												    
												</tr>
											</thead>
											<tbody>
											<?php 
                                                $sql = "SELECT * , tr_sample.sample_id as sam_id FROM `tr_sample` LEFT JOIN tr_sample_dokeep on tr_sample_dokeep.sample_id=tr_sample.sample_id where sample_status != 1 ORDER BY tr_sample.sample_id DESC";
                                                $objQuery = $db_connection->query($sql);
                                                $i=0;
                                                while(($row = $objQuery->fetch_assoc()) != null){
                                                    $i++;
                                                    echo "<tr>
                                                        <td>".$i."</td>";
                                                    echo "<td>
                                                            <button type='button' class='btn btn-warning btn-sm' data-toggle='modal' data-target='#edit_modal' data-toggle='tooltip' title='Edit' onclick='editpatient(\"".$row['sam_id']."\",\"".$row['sample_sid']."\",\"".$row['sample_date']."\",\"".$row['sample_time']."\",\"".$row['sample_prefix']."\",\"".$row['sample_fname']."\",\"".$row['sample_lname']."\",\"".$row['sample_hn']."\",\"".$row['sample_hbd']."\",\"".$row['sample_age']."\",\"".$row['sample_gender']."\",\"".$row['sample_type']."\",\"".$row['sample_pjowner']."\",\"".$row['sample_consent']."\",\"".$row['sample_pjname']."\",\"".$row['sample_receive']."\")'>Edit
                                                            </button> ";
                                                            if($row['sample_receive']=='Yes'){
                                                                echo " <button type='button' class='btn btn-primary btn-sm' data-toggle='modal' data-target='#doorkeepModal' data-toggle='tooltip' title='Status' onclick='dokeep(\"".$row['sam_id']."\",\"".$row['dokeep_id']."\",\"".$row['dokeep_status']."\",\"".$row['sample_sid']."\")'>Status
                                                                </button>
                                                                <button type='button' class='btn btn-info btn-sm' data-toggle='modal' data-target='#tubeModal' data-toggle='tooltip' title='Tube' onclick='tube(\"".$row['sam_id']."\",\"".$row['sample_sid']."\")'>Tube
                                                                </button>"; 
                                                              /*  <button type='button' class='btn btn-info btn-sm' data-toggle='modal' data-target='#printModal' data-toggle='tooltip' title='Tube' onclick='print(\"".$row['sam_id']."\",\"".$row['sample_sid']."\")'>Print
                                                                </button> */
                                                            }
                                                            echo " <button type='button' class='btn btn-danger btn-sm' data-toggle='modal' data-target='#delModal' data-toggle='tooltip' title='Delete' onclick='del(\"".$row['sam_id']."\",\"".$row['sample_sid']."\")'>x
                                                            </button>
                                                        </td>
                                                    ";
                                                    echo "<td>".$row['sample_sid']."</td>
                                                        <td>".$row['sample_date']."</td>
                                                        <td>".$row['sample_time']."</td>
                                                        <td>".$row['sample_prefix']."".$row['sample_fname']." ".$row['sample_lname']."</td>
                                                        <td>".$row['sample_hn']."</td>
                                                        <td>".$row['sample_hbd']."</td>
                                                        <td>".$row['sample_age']."</td>
                                                        <td>".$row['sample_gender']."</td>
                                                        <td>".$row['sample_type']."</td>
                                                        <td>".$row['sample_pjowner']."</td>
                                                        <td>".$row['sample_consent']."</td>
                                                        <td>".$row['sample_pjname']."</td>
                                                        <td>".$row['sample_receive']."</td>";
                                            ?>
                                            <?php
                                                        if($row['dokeep_status']==1){
                                                            echo "<td>Do</td>";
                                                        }
                                                        elseif($row['dokeep_status']==2){
                                                            echo "<td>Keep</td>";
                                                        }
                                                        elseif($row['dokeep_status']==3){
                                                            echo "<td>Do and Keep</td>";
                                                        }
                                                        else{
                                                            echo "<td></td>";
                                                        }
                                                    echo "</tr>";
                                                }
                                            ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!-- /# column -->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->
		</div>

        <!-- modal scroll -->
        <div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">New Sample</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="cancel_sam()">
                            <span aria-hidden="true">&times;</span>
                        </button>                                
                    </div>
                <form action="sample_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Scan LN <a id="red">*</a></label>
                                    <input type="text" class="form-control" id="sample_id" name="sample_id" value="">
                                    <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                </div>
                            </div>               
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">IN Date</label>
                                    <input type="text" class="form-control" id="sample_date" name="sample_date" value="<?php echo date("d-m-Y"); ?>" maxlength="10">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">IN Time</label>
                                    <input type="time" class="form-control" name="sample_time" value="<?php echo date("H:i"); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Prefix</label>
                                    <select class="form-control" id="sample_prefix" name="sample_prefix" >
                                        <option value="">Please select</option>
                                        <option value="Mr.">Mr.</option>
                                        <option value="Mrs.">Mrs.</option>
                                        <option value="Miss.">Miss.</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group"> 
                                    <label class="control-label">Firstname</label>
                                    <input type="text" class="form-control" id="sample_fname" name="sample_fname" value="">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group"> 
                                    <label class="control-label">Lastname</label>
                                    <input type="text" class="form-control" id="sample_lname" name="sample_lname" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"> 
                                    <label class="control-label">HN</label>
                                    <input type="text" class="form-control" id="sample_hn" name="sample_hn" value="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group"> 
                                    <label class="control-label">Birthday</label>
                                    <input type="text" class="form-control" id="sample_hbd" name="sample_hbd" value="" maxlength="10">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group"> 
                                    <label class="control-label">Age</label>
                                    <input type="number" class="form-control" id="sample_age" name="sample_age" value="0" min='0'>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Gender</label>
                                <div class="form-group">
                                    <div class="form-check-inline form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" id="sample_gender1" name="sample_gender"  value="Male" checked> Male
                                        </label>
                                        <label class="form-check-label p-l-10">
                                            <input class="form-check-input" type="radio" id="sample_gender2" name="sample_gender" value="Female"> Female
                                        </label>
                                    </div>
                                </div>
                            </div>   
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Type of specimen</label>
                                    <select class="form-control" id="sample_type" name="sample_type">
                                        <option value="">Please select</option>
                                        <option value="Plasma">Plasma</option>
                                        <option value="Urine">Urine</option>
                                        <option value="Cervical swab">Cervical swab</option>
                                        <option value="Blood">Blood</option>
                                        <option value="Tissue">Tissue</option>
                                    </select>
                                </div>
                            </div>               
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Project owner</label>
                                    <input type="text" class="form-control" id="sample_pjowner" name="sample_pjowner" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Consent form</label>
                                    <input type="text" class="form-control" id="sample_consent" name="sample_consent" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label">Project name</label>
                                    <input type="text" class="form-control" id="sample_pjname" name="sample_pjname" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Receive sample</label>
                                <div class="form-group">
                                    <div class="form-check-inline form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" id="sample_receive1" name="sample_receive"  value="Yes" checked> Yes
                                        </label>
                                        <label class="form-check-label p-l-10">
                                            <input class="form-check-input" type="radio" id="sample_receive" name="sample_receive" value="No"> No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="save_sample" value="save_sample">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button><!-- onclick="cancel_sam()" -->
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

		<!-- modal scroll -->
        <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Edit Sample</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>                                
                    </div>
                <form action="sample_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Scan LN <a id="red">*</a></label>
                                    <input type="text" class="form-control" name="sample_sid_edit" id="sample_sid_edit" value="" readonly>
                                    <input type="hidden" class="form-control" name="sample_id_edit" id="sample_id_edit" value="">
                                    <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                </div>
                            </div>               
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">IN Date</label>
                                    <input type="text" class="form-control" name="sample_date_edit" id="sample_date_edit" value="" maxlength="10">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">IN Time</label>
                                    <input type="time" class="form-control" name="sample_time_edit" id="sample_time_edit" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Prefix</label>
                                    <select class="form-control" name="sample_prefix_edit" id="sample_prefix_edit">
                                        <option value="" >Please select</option>
                                        <option value="Mr.">Mr.</option>
                                        <option value="Mrs.">Mrs.</option>
                                        <option value="Miss.">Miss.</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group"> 
                                    <label class="control-label">Firstname</label>
                                    <input type="text" class="form-control" name="sample_fname_edit" id="sample_fname_edit" value="">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group"> 
                                    <label class="control-label">Lastname</label>
                                    <input type="text" class="form-control" name="sample_lname_edit" id="sample_lname_edit" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"> 
                                    <label class="control-label">HN</label>
                                    <input type="text" class="form-control" name="sample_hn_edit" id="sample_hn_edit" value="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group"> 
                                    <label class="control-label">Birthday</label>
                                    <input type="text" class="form-control" name="sample_hbd_edit" id="sample_hbd_edit" value="" maxlength="10">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group"> 
                                    <label class="control-label">Age</label>
                                    <input type="number" class="form-control" name="sample_age_edit" id="sample_age_edit" value="" min='0'>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Gender</label>
                                <div class="form-group">
                                    <div class="form-check-inline form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" id="sample_gender_edit1" name="sample_gender_edit"  value="Male" checked> Male
                                        </label>
                                        <label class="form-check-label p-l-10">
                                            <input class="form-check-input" type="radio" id="sample_gender_edit2" name="sample_gender_edit" value="Female"> Female
                                        </label>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Type of specimen</label>
                                    <select class="form-control" name="sample_type_edit" id="sample_type_edit">
                                        <option value="0">Please select</option>
                                         <option value="">Please select</option>
                                        <option value="Plasma">Plasma</option>
                                        <option value="Urine">Urine</option>
                                        <option value="Cervical swab">Cervical swab</option>
                                        <option value="Blood">Blood</option>
                                        <option value="Tissue">Tissue</option>
                                    </select>
                                </div>
                            </div>               
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Project owner</label>
                                    <input type="text" class="form-control" name="sample_pjowner_edit" id="sample_pjowner_edit" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Consent form</label>
                                    <input type="text" class="form-control" name="sample_consent_edit" id="sample_consent_edit" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label">Project name</label>
                                    <input type="text" class="form-control" name="sample_pjname_edit" id="sample_pjname_edit" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Receive sample</label>
                                <div class="form-group">
                                    <div class="form-check-inline form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" id="sample_receive_edit1" name="sample_receive_edit"  value="Yes" checked> Yes
                                        </label>
                                        <label class="form-check-label p-l-10">
                                            <input class="form-check-input" type="radio" id="sample_receive_edit2" name="sample_receive_edit" value="No"> No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="edit_sample" value="edit_sample">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- end modal scroll -->

        <!-- modal static -->
		<div class="modal fade" id="doorkeepModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
			 data-backdrop="static">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="staticModalLabel">Do or Keep</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
                <form action="sample_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
					<div class="modal-body">
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="form-control-label" id="dokeep_ln_edit"></label><br>
                                <label class="form-control-label">Do or Keep</label>
                                <input type="hidden" class="form-control" name="dokeep_id_edit" id="dokeep_id_edit" value="">
                                <input type="hidden" class="form-control" name="dokeep_status_edit" id="dokeep_status_edit" value="">
                                <input type="hidden" class="form-control" name="dokeep_sample_id" id="dokeep_sample_id" value="">
                                <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                <div class="form-check">
                                    <div class="radio m-l-30">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" id="doorkeep1" name="doorkeep" value="1" checked> Do
                                        </label>
                                    </div>
                                    <div class="radio m-l-30">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" id="doorkeep2" name="doorkeep" value="2"> Keep
                                        </label>
                                    </div>
                                    <div class="radio m-l-30">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" id="doorkeep3" name="doorkeep" value="3"> Do and Keep
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary" name="dokeep_save" value="dokeep_save">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					</div>
                </form>
				</div>
			</div>
		</div>
		<!-- end modal static -->
        
        <!-- modal static -->
        <div class="modal fade" id="tubeModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
             data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticModalLabel">Add Tube</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <form action="sample_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                    <div class="modal-body">
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="form-control-label" id="tube_sample_ln"></label><br>
                                <label class="form-control-label">Add Tube</label>
                                <input type="hidden" class="form-control" name="tube_sample_id" id="tube_sample_id" value="">
                                <input type="hidden" class="form-control" name="tube_sample_sid" id="tube_sample_sid" value="">
                                <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                <div class="form-check">
                                    <div class="checkbox m-l-30">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="tube_check1" id="tube_check1" value="1" onclick="check_1()">
                                            <input type="text" class="form-control" name="tube_barcode1" id="tube_barcode1" value="" placeholder='Tube Name' readonly>
                                        </label>
                                    </div>
                                    <div class="checkbox m-l-30 m-t-10">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="tube_check2" id="tube_check2" value="2" onclick="check_2()"> 
                                            <input type="text" class="form-control" name="tube_barcode2" id="tube_barcode2" value="" placeholder='Tube Name' readonly>
                                        </label>
                                    </div>
                                    <div class="checkbox m-l-30 m-t-10">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="tube_check3" id="tube_check3" value="3" onclick="check_3()"> 
                                            <input type="text" class="form-control" name="tube_barcode3" id="tube_barcode3" value="" placeholder='Tube Name' readonly>
                                        </label>
                                    </div>
                                    <div class="checkbox m-l-30 m-t-10">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="tube_check4" id="tube_check4" value="4" onclick="check_4()"> 
                                            <input type="text" class="form-control" name="tube_barcode4" id="tube_barcode4" value="" placeholder='Tube Name' readonly>
                                        </label>
                                    </div>
                                    <div class="checkbox m-l-30 m-t-10">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="tube_check5" id="tube_check5" value="5" onclick="check_5()"> 
                                            <input type="text" class="form-control" name="tube_barcode5" id="tube_barcode5" value="" placeholder='Tube Name' readonly>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="tube_save" value="tube_save">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- end modal static -->

        <!-- modal static -->
        <div class="modal fade" id="printModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
             data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticModalLabel">Add Tube</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <form action="sample_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                    <div class="modal-body">
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="form-control-label" id="print_sample_ln"></label><br>
                                <label class="form-control-label">Add Tube</label>
                                <input type="hidden" class="form-control" name="print_sample_id" id="print_sample_id" value="">
                                <input type="hidden" class="form-control" name="print_sample_sid" id="print_sample_sid" value="">
                                <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                
                                <div class="form-check">
                                    <div class="checkbox m-l-30">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="tube_check1" id="checkall" value="1" onclick="check_1()"> All
                                            <!-- <input type="text" class="form-control" name="tube_barcode1" id="tube_barcode1" value="" readonly> -->
                                        </label>
                                    </div>
                                    <div class="checkbox m-l-30 m-t-10">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input checkcheck" name="tube_check1" id="tube_check1" value="1" onclick="check_1()"> Barcode_1
                                            <!-- <input type="text" class="form-control" name="tube_barcode1" id="tube_barcode1" value="" readonly> -->
                                        </label>
                                    </div>
                                    <div class="checkbox m-l-30 m-t-10">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input checkcheck" name="tube_check2" id="tube_check2" value="2" onclick="check_2()"> Barcode_2
                                            <!-- <input type="text" class="form-control" name="tube_barcode2" id="tube_barcode2" value="" readonly> -->
                                        </label>
                                    </div>
                                    <div class="checkbox m-l-30 m-t-10">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input checkcheck" name="tube_check3" id="tube_check3" value="3" onclick="check_3()"> Barcode_3
                                            <!-- <input type="text" class="form-control" name="tube_barcode3" id="tube_barcode3" value="" readonly> -->
                                        </label>
                                    </div>
                                    <div class="checkbox m-l-30 m-t-10">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input checkcheck" name="tube_check4" id="tube_check4" value="4" onclick="check_4()"> Barcode_4
                                            <!-- <input type="text" class="form-control" name="tube_barcode4" id="tube_barcode4" value="" readonly> -->
                                        </label>
                                    </div>
                                    <div class="checkbox m-l-30 m-t-10">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input checkcheck" name="tube_check5" id="tube_check5" value="5" onclick="check_5()"> Barcode_5
                                            <!-- <input type="text" class="form-control" name="tube_barcode5" id="tube_barcode5" value="" readonly> -->
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="tube_save" value="tube_save">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- end modal static -->

        <!-- modal static -->
        <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
             data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticModalLabel">Delete</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <form action="sample_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                    <div class="modal-body">
                        <label>Want to delete </label>
                        <label class="form-control-label" id="del_ln"></label>
                        <input type="hidden" class="form-control" name="del_id" id="del_id">
                        <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="del_save" value="del_save">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>    
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- end modal static -->
  <!--   </form> -->

	<?php include("_footer.php"); ?>
	<?php include("_js.php"); ?>
	<?php include("./vendor/datatables/_js_datatable.php"); ?>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>
    <!--  <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.th.min.js"></script> -->

	<script type="text/javascript">
		$(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
            else if(result==0){
                swal({
                    title: "Error",
                });
            }
            else if(result==2){
                swal({
                    title: "Fail",
                    text: "Input LN",
                });
            }
            else if(result==4){
                swal({
                    title: "Fail",
                    text: "Duplicate LN",
                });
            }
            else if(result==5){
                swal({
                    title: "Fail",
                    text: "Duplicate Tube Barcode",
                });
            }
            else if(result==3){
                swal({
                    title: "Fail",
                    text: "Check input barcode tube",
                });
            }
        }

        function editpatient(id,sam_id,sam_date,sam_time,prefix,fname,lname,hn,hbd,age,gender,type,pjowner,consent,pjname,receive) {
            document.getElementById("sample_id_edit").value = id;
            document.getElementById("sample_sid_edit").value = sam_id;
            document.getElementById("sample_date_edit").value = sam_date;
            document.getElementById("sample_time_edit").value = sam_time;
            document.getElementById("sample_prefix_edit").value = prefix;
            document.getElementById("sample_fname_edit").value = fname;
            document.getElementById("sample_lname_edit").value = lname;  
            document.getElementById("sample_hn_edit").value = hn;
            document.getElementById("sample_hbd_edit").value = hbd;
            document.getElementById("sample_age_edit").value = age; 
            var gender1 = document.getElementById("sample_gender_edit1").value;
            var gender2 = document.getElementById("sample_gender_edit2").value;
            if(gender1==gender){
                document.getElementById("sample_gender_edit1").checked = gender;
            }
            else if(gender2==gender){
                document.getElementById("sample_gender_edit2").checked = gender;
            }
            document.getElementById("sample_type_edit").value = type;
            document.getElementById("sample_pjowner_edit").value = pjowner;
            document.getElementById("sample_consent_edit").value = consent;
            document.getElementById("sample_pjname_edit").value = pjname;
            var receive1 = document.getElementById("sample_receive_edit1").value;
            var receive2 = document.getElementById("sample_receive_edit2").value;
            if(receive1==receive){
                document.getElementById("sample_receive_edit1").checked = receive;
            }
            else if(receive2==receive){
                document.getElementById("sample_receive_edit2").checked = receive;
            }
        }

        function dokeep(id,dokepp,sta,ln) {
            document.getElementById("dokeep_ln_edit").innerHTML = ln;
            document.getElementById("dokeep_sample_id").value = id;
            document.getElementById("dokeep_id_edit").value = dokepp;
            document.getElementById("dokeep_status_edit").value = sta;
            var doorkeep1 = document.getElementById("doorkeep1").value;
            var doorkeep2 = document.getElementById("doorkeep2").value;
            var doorkeep3 = document.getElementById("doorkeep3").value;
            if(doorkeep1==sta){
                document.getElementById("doorkeep1").checked = sta;
            }
            else if(doorkeep2==sta){
                document.getElementById("doorkeep2").checked = sta;
            }
            else if(doorkeep3==sta){
                document.getElementById("doorkeep3").checked = sta;
            }
        }

        function tube(id,sid) {
            document.getElementById("tube_sample_ln").innerHTML = sid;
            document.getElementById("tube_sample_id").value = id;
            document.getElementById("tube_sample_sid").value = sid;
        }

        function print(id,sid) {
            document.getElementById("print_sample_ln").innerHTML = sid;s
            document.getElementById("print_sample_id").value = id;
            document.getElementById("print_sample_sid").value = sid;
        }

        function del(id,ln) {
            document.getElementById("del_ln").innerHTML = ln+"?";
            document.getElementById("del_id").value = id;
        }

        $(document).ready(function(){
            $("#sample_date").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', <?php echo date("d-m-Y"); ?>);
            });
        });

        $(document).ready(function(){
            $("#sample_hbd").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', '');
            });
        });

        $(document).ready(function(){
            $("#sample_date_edit").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', '');
            });
        });

        $(document).ready(function(){
            $("#sample_hbd_edit").each(function() {    
                $(this).datepicker({
                    format: 'dd-mm-yyyy',
                    todayBtn: false,
                    language: 'eng',             
                    thaiyear: false,
                    autoclose:true,   
                }).datepicker('setDate', '');
            });
        });

        $('#addmodal').on('shown.bs.modal', function () {
            $('#sample_id').focus()
        });

        function cancel_sam() {
            document.getElementById("sample_id").value = '';
            document.getElementById("sample_prefix").value = '';
            document.getElementById("sample_fname").value = '';
            document.getElementById("sample_lname").value = '';
            document.getElementById("sample_hn").value = '';
            document.getElementById("ssaample_hbd").value = '';
            document.getElementById("sample_age").value = '';
            document.getElementById("sample_gender1").checked = 'Male';
            document.getElementById("sample_type").value = '';
            document.getElementById("sample_pjowner").value = '';
            document.getElementById("sample_consent").value = '';
            document.getElementById("sample_pjname").value = '';
            document.getElementById("sample_receive1").checked = 'Yes';
        }

        var sample_age = document.getElementById('sample_age');
        sample_age.onkeydown = function(e) {
            if(!((e.keyCode > 95 && e.keyCode < 106)
              || (e.keyCode > 47 && e.keyCode < 58) 
              || e.keyCode == 8)) {
                return false;
            }
        }

        var sample_age_edit = document.getElementById('sample_age_edit');
        sample_age_edit.onkeydown = function(e) {
            if(!((e.keyCode > 95 && e.keyCode < 106)
              || (e.keyCode > 47 && e.keyCode < 58) 
              || e.keyCode == 8)) {
                return false;
            }
        }

        function check_1() {
            var checkBox = document.getElementById("tube_check1");
            var text = document.getElementById("tube_barcode1");
            if(checkBox.checked == true){
                text.readOnly = false;
            }
            else{
               text.readOnly = true;
               text.value = '';
            }
        }

        function check_2() {
            var checkBox = document.getElementById("tube_check2");
            var text = document.getElementById("tube_barcode2");
            if(checkBox.checked == true){
                text.readOnly = false;
            }
            else{
               text.readOnly = true;
               text.value = '';
            }
        }

        function check_3() {
            var checkBox = document.getElementById("tube_check3");
            var text = document.getElementById("tube_barcode3");
            if(checkBox.checked == true){
                text.readOnly = false;
            }
            else{
               text.readOnly = true;
               text.value = '';
            }
        }

        function check_4() {
            var checkBox = document.getElementById("tube_check4");
            var text = document.getElementById("tube_barcode4");
            if(checkBox.checked == true){
                text.readOnly = false;
            }
            else{
               text.readOnly = true;
               text.value = '';
            }
        }

        function check_5() {
            var checkBox = document.getElementById("tube_check5");
            var text = document.getElementById("tube_barcode5");
            if(checkBox.checked == true){
                text.readOnly = false;
            }
            else{
               text.readOnly = true;
               text.value = '';
            }
        }

        $('#checkall').change(function () {
            $('.checkcheck').prop('checked',this.checked);
        });

        $('.checkcheck').change(function () {
            if($('.checkcheck:checked').length == $('.checkcheck').length){
                $('#checkall').prop('checked',true);
            }
            else{
                $('#checkall').prop('checked',false);
            }
        });
	</script>
</body>
</html>
<!-- end document-->