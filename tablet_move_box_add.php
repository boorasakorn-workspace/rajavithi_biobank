<?php 
    session_start();
    if (!$_SESSION["user_id"]){  //check session
        Header("Location: tablet_404.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 
    }
    include("connect.php"); 
?> 
<?php 
    if($_POST['input_barcode_tube']!=''&&$_GET['box_bar']==''){
        $sqlfz = "SELECT * FROM `ms_box` WHERE box_barcode = '".$_POST['input_barcode_tube']."'";
    }
    elseif($_POST['input_barcode_tube']==''&&$_GET['box_bar']!=''){
        $sqlfz = "SELECT * FROM `ms_box` WHERE box_barcode = '".$_GET['box_bar']."'";
    }
    $objQueryfz = $db_connection->query($sqlfz);
    while(($row = $objQueryfz->fetch_assoc()) != null){
        $box_id = $row['box_id'];
        $box_name = $row['box_name'];
        $r = $row['box_row'];
        $c = $row['box_colum'];
    }
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/icon/logo_bioo.ico">
    <!-- Title Page-->
    <title>RJBiobank</title>

    <?php include("_css.php"); ?>
    <?php include("./vendor/datatables/_css_datatable.php"); ?>
    <link href="vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        #iconimg{
            width: 15%;
        }

        @media (max-width: 991px) {
            #iconimg{
                width: 35%;
            }

            div.dataTables_wrapper div.dataTables_filter input {
                margin-left: 0.5em;
                display: inline-block;
                width: 65%;
            }
        }
    </style>
</head>
<body class="animsition">
    <div class="page-wrapper">
            <div class="container">
                    <div class="login-content">
                <iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff;display: none;"></iframe>
                <form action="box_add_model.php" method="post" enctype="multipart/form-data" target="iframe_target">
                            <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4> Move <?php if($_GET['s']=='t'){ echo "Tube"; }elseif($_GET['s']=='b'){ echo "Box"; } ?> | Box</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6"> 
                                                <img src="images/icon/logo-bio.png" alt="CoolAdmin" id="iconimg"> <h3 class="m-t-10"><?php echo $box_name; ?> </h3>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <button type="button" class="btn btn-secondary mb-1" onclick="window.history.back();"> Back </button>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-5 text-right">
                                                <table id="example" class="table table-responsive table-striped table-bordered" style="width:100%">
                                                    <tr>
                                                        <th width="1%" style="white-space: nowrap; text-align: left;">N</th> 
                                                        <th width="1%" style="white-space: nowrap; text-align: left;">Barcode</th>
                                                        <th width="1%" style="white-space: nowrap; text-align: left;">Well</th>
                                                    </tr>
                                                <?php
                                                    $sql = "SELECT * FROM `tr_box_add` where box_id = '".$box_id."'";
                                                    $objQuery = $db_connection->query($sql);
                                                    $i=0;
                                                    while(($row = $objQuery->fetch_assoc()) != null){
                                                        if($row['boxadd_status']!=1){
                                                            $i++;
                                                            echo "<tr>
                                                                <td style='text-align: left;'>".$i."</td>
                                                                <td style='text-align: left;'>".$row['sample_sid']."</td>
                                                                <td style='text-align: left;'>".$row['boxadd_well']."</td>
                                                            </tr>";
                                                        }
                                                    }
                                                ?>
                                                </table>
                                            </div>
                                            <div class="col-md-7">
                                                <table id="example" class="table table-responsive table-striped table-bordered text-center" style="width:100%">
                                                <?php
                                                    $r = $r;
                                                    $r = $r+65;
                                                    $c = $c;
                                                    echo "<tr>
                                                        <td></td>";
                                                        for($i=1;$i<=$c;$i++){
                                                            echo "<th width='1%'>".$i."</th>";
                                                        }
                                                      echo"</tr>";
                                                    for($i=65;$i<$r;$i++){                                                    
                                                        echo "<tr>";
                                                            echo "<th width='1%'>".chr($i)."</th>";

                                                            for($j=1;$j<=$c;$j++){  
                                                                $well = chr($i).$j;
                                                                $sql = "SELECT * FROM `tr_box_add` where box_id = '".$box_id."' AND boxadd_well = '".$well."' AND boxadd_status != 1";
                                                                $objQuery = $db_connection->query($sql);
                                                                $row = $objQuery->fetch_assoc();
                                                               
                                                                if($row['boxadd_well']!=''){
                                                                    echo "<td style='background-color: #ffc107; color: #212529'>";
                                                                    echo $row['boxadd_well'];
                                                                    echo "</td>";
                                                                }
                                                                else{
                                                                    echo "<td>
                                                                        <button type='button' data-toggle='modal' data-target='#add_model'  style='' onclick='addtube(\"".$box_id."\",\"".$well."\")'> ".$well." </button>";
                                                                    echo "</td>";
                                                                }                                       
                                                            }
                                                        echo "</tr>";
                                                    }
                                                ?>
                                                </table>
                                                <center>Open</center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <input type="button" class="btn btn-success m-t-10" value="Menu" onclick="window.location.href='tablet_menu.php?fz=<?php echo $_GET['fz']; ?>'" />
                                <input type="button" class="btn btn-info m-t-10 m-l-10" value="Logout" onclick="window.location.href='tablet_logout.php?fz=<?php echo $_GET['fz']; ?>'" />
                            </div>
                        </div>
            </div>
        </div>
    </div>

        <!-- Add modal -->
        <div class="modal fade" id="add_model" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
             data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticModalLabel">Move Tube in box</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Want to move?</label>
                                    <input type="hidden" class="form-control" name="well" id="well">
                                    <input type="hidden" class="form-control" name="box_id" id="box_id">
                                    <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $_SESSION["user_id"]; ?>">
                                    <input type="hidden" class="form-control" name="id" value="<?php echo $_GET['id']; ?>">
                                </div>
                            </div>               
                        </div>
                    </div>
                    <div class="modal-footer"> 
                        <button type="submit" class="btn btn-primary" name="save_movetube" value="save_movetube">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div> 
                </div>
            </div>
        </div>
        <!-- End modal -->

        <!-- modal scroll -->
            <div class="modal fade" id="move_fz_model" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="scrollmodalLabel">Move Tube to Freezer</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h5 id='bar_fz'></h5>
                            <br>
                            <div class="row">   
                            <?php 
                                $sql = "SELECT * FROM `ms_freezer`";
                                $objQuery = $db_connection->query($sql);
                                $i=0;
                                while(($row = $objQuery->fetch_assoc()) != null){
                                    $i++;
                                    echo "<div class='col-md-4 text-center'>
                                        <a class='move_link' id='move_link".$i."' onclick='move_fz_l(\"".$i."\",\"".$row['freezer_id']."\")'> <img src='images/freezer_n.png' alt='Biobank' width='70%' /> </a>
                                        <h4 class='text-center p-t-10 p-b-20'>".$row['freezer_name']." </h4>
                                    </div>";
                                }
                            ?>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <!-- <button type="button" class="btn btn-primary">Confirm</button> -->
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end modal scroll -->

    </form>

    <?php include("_js.php"); ?>
    <?php include("./vendor/datatables/_js_datatable.php"); ?>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="vendor/bootstrap-datepicker/bootstrap-datepicker-custom.js"></script>
    <!--  <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.th.min.js"></script> -->

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });

        function showResult(result,id){
            if(result==1){
                location.reload();
            }
        }

        $('#add_model').on('shown.bs.modal', function () {
            $('#sample_id').focus()
        });

        function addtube(id,num) {
            document.getElementById("box_id").value = id;
            document.getElementById("well").value = num;
        }

        var link = '';

        function move_fz(id,bar) {
            link = '';
            var n = <?php echo $i; ?>;
            var i;
            document.getElementById("bar_fz").innerHTML = bar;
            link = 'tablet_move_freezer_floor.php?id='+id+'&s=t';
            for(i=1;i<=n;i++){
                document.getElementById('move_link'+i).href = link;
            }
        }

        function move_fz_l(i,fz) {
            document.getElementById('move_link'+i).href = link+'&fz='+fz;
        }
    </script>
</body>
</html>
<!-- end document-->